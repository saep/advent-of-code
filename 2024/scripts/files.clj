(ns files
  (:require
   [babashka.fs :as fs]
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]))

(with-test
  (defn strip-extensions
    [filename]
    (if-let [first-dot (str/index-of filename ".")]
      (subs filename 0 first-dot)
      filename))
  (is (= "foo" (strip-extensions "foo")))
  (is (= "bar" (strip-extensions "bar.txt")))
  (is (= "quz" (strip-extensions "quz.txt.age"))))

(with-test
  (defn add-extensions
    "Adds multiple extensions after the file name separated with a dot."
    [filename extensions]
    (str/join "." (reduce
                   conj
                   [filename]
                   (if (string? extensions)
                     [extensions]
                     extensions))))
  (is (= "foo" (add-extensions "foo" [])))
  (is (= "bar.txt" (add-extensions "bar" ["txt"])))
  (is (= "bar.txt" (add-extensions "bar" "txt")))
  (is (= "quz.txt.age" (add-extensions "quz" ["txt" "age"]))))

(with-test
  (defn replace-extension
    "Replaces the extension with the given extension completely.
  
    You can remove an extension by calling this function with ext nil"
    [file extensions]
    (let [f (if (string? file)
              (.toAbsolutePath (fs/path file))
              (.toAbsolutePath file))
          stripped (strip-extensions (fs/file-name f))]
      (if (empty? extensions)
        (.resolve (.getParent f) stripped)
        (.resolve (.getParent f)
                  (add-extensions stripped extensions)))))
  (is (= (.toAbsolutePath (fs/path "foo"))
         (replace-extension "foo" nil)))
  (is (= (.toAbsolutePath (fs/path "bar.txt.age"))
         (replace-extension "bar.md" ["txt" "age"])))
  (is (= (.toAbsolutePath (fs/path "bar.txt"))
         (replace-extension "bar.md" "txt"))))



