(ns decrypt.age
  (:require
   [babashka.fs :as fs]
   [babashka.process :as proc]
   [files :as fu]
   [ssh :as ssh]))

(defn read-password-from-stdin
  [purpose]
  (let [console (System/console)
        pw (.readPassword console "Type the password for %s:" (into-array [purpose]))]
    (apply str pw)))

(defn -main
  [& _args]
  (let [unencrypted-files (->> (fs/glob "inputs/" "*.txt.age")
                               (map (fn [f] {:original f :missing (fu/replace-extension f "txt")}))
                               (filter #(not (fs/exists? (:missing %)))))]

    (if (empty? unencrypted-files)
      (println "All files are already decrypted.")
      (doseq [{:keys [original missing]} unencrypted-files]
        (println "Decrypting " (fs/file-name original))
        (try
          (proc/shell
           {:out (.toString missing)}
           "age --decrypt -i" ssh/IDENTITY original)
          (catch Exception e (do (println (.getMessage e))
                                 (fs/delete-if-exists missing))))))))

