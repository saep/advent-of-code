(ns encrypt.age
  (:require
   [babashka.fs :as fs]
   [babashka.process :as proc]
   [files :as fu]
   [ssh :as ssh]))

(defn -main
  [& _args]
  (let [inputs (fs/glob "inputs/" "*.txt")
        unencrypted-files (->> inputs
                               (map (fn [f] {:original f,
                                             :missing (fu/replace-extension f ["txt" "age"])}))
                               (filter #(not (fs/exists? (:missing %)))))]
    (if (empty? unencrypted-files)
      (println "No files found that aren't already encrypted.")
      (doseq [{:keys [original missing]} unencrypted-files]
        (println "Encrypting" (fs/file-name original) "to" (fs/file-name missing))
        (proc/shell {:out (.toString missing)} "age --encrypt -a -i" ssh/IDENTITY original)))))
