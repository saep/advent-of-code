(ns aoc.utils
  (:require
   [clojure.test :refer [is with-test]]))

(defn transpose
  [m]
  (when (not-empty m)
    (apply mapv vector m)))

(with-test
  (defn chr->int
    "Converts the character 1 to 1 as an int"
    [c]
    (let [i (- (int c) (int \0))]
      (when (and (< i 10) (>= i 0)) i)))
  (is (= 1 (chr->int \1)))
  (is (nil? (chr->int \a))))
