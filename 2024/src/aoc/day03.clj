(ns aoc.day03
  (:require
   [clojure.test :as test :refer [with-test is]]))

(def example-input "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))")

(with-test
  (defn part-1
    [input]
    (->> input
         (re-seq #"mul[(]\d+,\d+[)]")
         (map #(->> % (re-seq #"\d+") (map parse-long) (apply *)))
         (apply +)))
  (is (= 161 (part-1 example-input)))
  (is (= 168539636 (part-1 (slurp "inputs/day03.txt")))))

(def example-input-part-2 "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))")

(defrecord instruction [mul do dont])

(with-test
  (defn- parse
    [input]
    (let [mul (fn [m] (when m (->> m (re-seq #"\d+") (map parse-long))))
          mkInstruction (fn [[_ m do dont]] (->instruction (mul m) (some? do) (some? dont)))]
      (->> input
           (re-seq #"(mul[(]\d+,\d+[)])|(do\(\))|(don't\(\))")
           (map #(mkInstruction %)))))
  (is (= (list (->instruction '(2 4) false false)) (parse "mul(2,4)")))
  (is (= (list (->instruction nil true false)) (parse "do()")))
  (is (= (list (->instruction nil false true)) (parse "don't()"))))

(with-test
  (defn part-2
    [input]
    (->> input
         parse
         (reduce
          (fn [state el]
            (cond
              (:do el) (assoc state :on true)
              (:dont el) (assoc state :on false)
              (:on state) (update state :result #(+ % (apply * (:mul el))))
              :else state))
          {:on true :result 0})
         :result))
  (is (= 48 (part-2 example-input-part-2)))
  (is (= 97529391 (part-2 (slurp "inputs/day03.txt")))))


