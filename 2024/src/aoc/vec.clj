(ns aoc.vec
  (:require
   [clojure.test :refer [is with-test]]))

(defn s*v
  "Multiplies vector `v` with scalar `s`"
  [s v]
  (mapv #(* s %) v))

(defn s+v
  "Adds scalar `s` to every element of vector `v`"
  [s v]
  (mapv #(+ s %) v))

(with-test
  (defn v*
    "Multiplies vector `v` with `vs`"
    ([v] v)
    ([v1 v2] (mapv * v1 v2))
    ([v1 v2 v3] (mapv * v1 v2 v3))
    ([v1 v2 v3 & vs] (apply mapv * v1 v2 v3 vs)))
  (is (= [1 7] (v* [1 7])))
  (is (= [15 21] (v* [3 7] [5 3])))
  (is (= [64 27 8] (v* [4 3 2] [4 3 2] [4 3 2])))
  (is (= [1 2 3 4] (v* [1 1 1 1] [1 2 3 4]))))

(defn v+
  "Sums vector `v1` with `vs`"
  ([v] v)
  ([v1 v2] (mapv + v1 v2))
  ([v1 v2 v3] (mapv + v1 v2 v3))
  ([v1 v2 v3 & vs] (apply mapv + v1 v2 v3 vs)))

(defn v-
  ([v] (mapv - v))
  ([v1 v2] (mapv - v1 v2)))

