(ns aoc.day07
  (:require
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]))

(def example-input "190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20")

(defrecord calibration-equation [result numbers])

(defn- parse-calibration-equation
  [line]
  (let [[result & numbers] (map parse-long (str/split line #":?\s+"))]
    (->calibration-equation result (vec numbers))))

(defn- parse
  [input]
  (->> input
       str/split-lines
       (mapv parse-calibration-equation)))

(with-test
  (defn- con
    [a b]
    (parse-long (str/join "" [(str a) (str b)])))
  (is (= 178 (con 17 8))))

(with-test
  (defn solvable?
    [ops {:keys [result numbers]}]
    (let [result result
          [i & is] numbers]
      (letfn [(go [acc remaining]
                  (if (empty? remaining)
                    (= result acc)
                    (let [[x & xs] remaining]
                      (some true? (map #(go (% acc x) xs) ops)))))]
        (go i is))))
  (is (solvable? [+ *] (->calibration-equation 190 [10 19])))
  (is (solvable? [+ *] (->calibration-equation 3267 [81 40 27])))
  (is (solvable? [+ * con] (->calibration-equation 77 [7 7]))))

(with-test
  (defn part-1
    [input]
    (->> input
         parse
         (filter #(solvable? [+ *] %))
         (map :result)
         (apply +)))
  (is (= 3749 (part-1 example-input)))
  (is (= 5512534574980 (part-1 (slurp "inputs/day07.txt")))))

(with-test
  (defn part-2
    [input]
    (->> input
         parse
         (filter #(solvable? [+ * con] %))
         (map :result)
         (apply +)))
  (is (= 11387 (part-2 example-input)))
  (is (= 328790210468594 (part-2 (slurp "inputs/day07.txt")))))

