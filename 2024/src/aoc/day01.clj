(ns aoc.day01
  (:require
   [aoc.utils :refer [transpose]]
   [clojure.string :as str]
   [clojure.test :as test :refer [is with-test]]))

(def example-input
  "3   4
4   3
2   5
1   3
3   9
3   3")

(defn- parse-line
  [line]
  (->>
   (str/split line #"\s+")
   (map parse-long)
   (apply vector)))

(with-test
  (defn- parse
    [input]
    (some->> input
             str/split-lines
             (filterv #(not (str/blank? %)))
             (mapv parse-line)
             transpose
             (mapv sort)))
  (is (nil? (parse "")))
  (is (= ['(1) '(2)] (parse "1 2")))
  (is (= ['(2 4 7) '(1 3 7)] (parse "2 1\n4 3\n7 7")))
  (is (= ['(1 2 3 3 3 4) '(3 3 3 4 5 9)] (parse example-input))))

(with-test
  (defn part-1
    [input]
    (let [[left right] (parse input)]
      (apply + (map abs (map - right left)))))
  (is (= 11 (part-1 example-input)))
  (is (= 2000468 (part-1 (slurp "inputs/day01.txt")))))

(with-test
  (defn part-2
    [input]
    (let [[left right] (parse input)
          freq (frequencies right)]
      (apply + (map #(* % (get freq % 0)) left))))
  (is (= 31 (part-2 example-input)))
  (is (= 18567089 (part-2 (slurp "inputs/day01.txt")))))

