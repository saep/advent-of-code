(ns aoc.day14
  (:require
   [aoc.vec :as v]
   [clojure.core.reducers :as r]
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]
   [medley.core :as medley]))

(def example-input "p=0,4 v=3,-3
p=6,3 v=-1,-3
p=10,3 v=-1,2
p=2,0 v=2,-1
p=0,0 v=1,3
p=3,0 v=-2,-2
p=7,6 v=-1,-3
p=3,0 v=-1,-2
p=9,3 v=2,3
p=7,3 v=-1,2
p=2,4 v=2,-3
p=9,5 v=-3,-3")

(with-test
  (defn- parse-robot
    [line]
    (let [m (re-matcher #"^p=(-?\d+),(-?\d+)\s+v=(-?\d+),(-?\d+)$" line)
          l #(parse-long (.group m %))]
      (when (.matches m)
        {:position [(l 1) (l 2)]
         :velocity [(l 3) (l 4)]})))
  (is (= {:position [0 4] :velocity [3 -3]} (parse-robot "p=0,4 v=3,-3"))))

(defn- parse-robots
  [input]
  (map parse-robot (str/split-lines input)))

(with-test
  (defn- pos-after-n-seconds
    [area seconds {:keys [position velocity]}]
    (->> (v/v+ position (v/s*v seconds velocity))
         (mapv #(mod %2 %1) area)))
  (is (= [1 3] (pos-after-n-seconds [11 7] 5 {:position [2 4] :velocity [2 -3]}))))

(with-test
  (defn- quadrant
    [area pos]
    (letfn [(f [w x]
              (cond
                (< x (bit-shift-right w 1)) 0
                (> x (bit-shift-right w 1)) 1
                :else nil))]
      (let [res (mapv f area pos)]
        (when (not-any? nil? res) res))))
  (is (nil? (quadrant [11 7] [5 1])))
  (is (nil? (quadrant [11 7] [4 3])))
  (is (= [0 0] (quadrant [11 7] [0 2])))
  (is (= [1 0] (quadrant [11 7] [6 0])))
  (is (= [0 1] (quadrant [11 7] [4 5])))
  (is (= [1 1] (quadrant [11 7] [6 6]))))

(defn- safety-factor
  [area robots]
  (->> robots
       (medley/collate-by #(quadrant area %) (fn [a _] (inc a)) (fn [_] 1))
       (filter (fn [[k _]] (not (nil? k))))
       vals
       (apply *)))

(with-test
  (defn part-1
    [area input]
    (->> input
         parse-robots
         (map #(pos-after-n-seconds area 100 %))
         (safety-factor area)))
  (is (= 12 (part-1 [11 7] example-input)))
  (is (= 223020000 (part-1 [101 103] (slurp "inputs/day14.txt")))))

#_{:clj-kondo/ignore [:unused-private-var]}
(defn- print-robots
  [[cols rows] initial-robots seconds]
  (let [robots (into #{} (map #(pos-after-n-seconds [cols rows] seconds %) initial-robots))
        lines (str/join "\n" (map (fn [y]
                                    (str/join "" (map (fn [x]
                                                        (if (robots [x y]) \# \.)) (range cols))))
                                  (range rows)))]
    (print lines)
    seconds))

; After printing out the grid for the first 6 lowest safety-factors, which other people were also doing,
; I have found the tree manually. Now that I know how it looks like, I came up with this check.
(defn- likely-tree
  [robots]
  (< 1 (->> robots
            (into #{})
            (map second)
            frequencies
            vals
            (filter #(> % 30))
            count)))

(with-test
  (defn part-2
    [input]
    (let [area [101 103]
          robots (parse-robots input)]
      (->> (range (apply * area))
           (into [])
           (r/fold
            (r/monoid min (constantly Long/MAX_VALUE))
            (fn [acc s]
              (if
               (likely-tree (map (partial pos-after-n-seconds area s) robots))
                s
                acc))))))

  (is (= 7338 (time (part-2 (slurp "inputs/day14.txt"))))))




