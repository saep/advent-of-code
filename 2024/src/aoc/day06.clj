(ns aoc.day06
  (:require
   [aoc.vec :as vec]
   [clojure.core.reducers :as r]
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]))

(def example-input "....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...")

(defn- rotate-right
  [[row col]]
  [col (- row)])

(defrecord patrol-route [grid start dir])
(with-test
  (defn parse
    [input]
    (let [grid (str/split-lines input)
          start (for [row (range (count grid))
                      col (range (count (first grid)))
                      :when (= \^ (get-in grid [row col]))]
                  [row col])]
      (->patrol-route grid (first start) [-1 0])))
  (is (= [[6 4] [-1 0]] (->> example-input parse ((juxt :start :dir))))))

(with-test
  (defn patrol
    [{:keys [grid start dir]}]
    (loop [guard start
           dir dir
           visited (transient #{})
           path (transient [])]
      (if (nil? (get-in grid guard))
        (persistent! path)
        (let [next-pos (vec/v+ guard dir)]
          (cond (= \# (get-in grid next-pos)) (recur guard (rotate-right dir) (conj! visited [guard dir]) path)
                (visited [next-pos dir]) :loop
                :else (recur next-pos dir (conj! visited [guard dir]) (conj! path guard)))))))
  (is (= 1 ((comp count distinct #(map first %) patrol parse) "^.")))
  (is (= 4 ((comp count distinct patrol parse str/join) "\n" [".#.."
                                                              "...#"
                                                              ".^.."])))
  (is (= :loop ((comp patrol parse str/join) "\n" [".#.."
                                                   "...#"
                                                   "#..."
                                                   ".^#."]))))

(with-test
  (defn part-1
    [input]
    (->> input parse patrol distinct count))
  (is (= 41 (part-1 example-input)))
  (is (= 5564 (part-1 (slurp "inputs/day06.txt")))))

(with-test
  (defn- replace-char-at [s col c]
    (str/join "" [(.substring s 0 col) c (.substring s (inc col) (.length s))]))
  (is (= "abc" (replace-char-at "bbc" 0 \a)))
  (is (= "123" (replace-char-at "103" 1 \2)))
  (is (= "(-)" (replace-char-at "(-:" 2 \)))))

(with-test
  (defn part-2 [input]
    (let [patrol-route (parse input)
          path (->> patrol-route patrol set vec)]
      (r/fold
       +
       (fn [acc [row col]]
         (let [blocked-patrol-route (update-in patrol-route [:grid row] #(replace-char-at % col \#))]
           (if (= :loop (patrol blocked-patrol-route))
             (inc acc)
             acc)))
       path)))
  (is (= 6 (part-2 example-input)))
  (is (= 1976 (part-2 (slurp "inputs/day06.txt")))))

