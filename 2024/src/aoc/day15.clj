(ns aoc.day15
  (:require
   [aoc.vec :as vec]
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]))

(def ^:private example-input "##########
#..O..O.O#
#......O.#
#.OO..O.O#
#..O@..O.#
#O#..O...#
#O..O..O.#
#.OO.O.OO#
#....O...#
##########

<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^")

(def ^:private small-example "########
#..O.O.#
##@.O..#
#...O..#
#.#.O..#
#...O..#
#......#
########

<^^>>>vv<v>>v<<")

(defn- find-start-position
  [grid]
  (first (for [row (range (count grid))
               col (range (count (first grid)))
               :when (= \@ (get-in grid [row col]))]
           [row col])))

(defn- parse
  [input]
  (let [[grid-str moves] (str/split input #"\n\n" 2)
        grid (str/split-lines grid-str)
        pos (find-start-position grid)]
    {:grid grid, :moves moves :robot pos}))

(def ^:private dirs
  {\^ [-1 0], \> [0 1], \v [1 0], \< [0 -1]})

(defn- free? [grid pos]
  (= \. (get-in grid pos)))

(defn- box? [grid pos]
  (case (get-in grid pos)
    \O [pos]
    \[ [pos (vec/v+ pos [0 1])]
    \] [(vec/v- pos [0 1]) pos]
    nil))

(defn- assoc-grid
  [grid [row col] v]
  (let [old (get grid row)]
    (assoc grid row
           (str (subs old 0 col)
                v
                (subs old (inc col))))))

(with-test
  (defn- move-box
    [grid [l-pos r-pos] dir]
    (reduce
     (fn [g [p c]]
       (assoc-grid g p c))
     grid
     (filter some? [[l-pos \.]
                    (when r-pos [r-pos \.])
                    [(vec/v+ dir l-pos) (if r-pos \[ \O)]
                    (when r-pos [(vec/v+ dir r-pos) \]])])))
  (is (= ["O."] (move-box [".O"] [[0 1]] [0 -1])))
  (is (= ["." "O"] (move-box ["O" "."] [[0 0]] [1 0]))))

(defn- can-move-box?
  [grid dir box]
  (every? #(or (free? grid %) (some #{%} box))
          (map #(vec/v+ dir %) box)))

(with-test
  (defn- affected-boxes
    [grid initial-pos dir]
    (if-let [box (box? grid initial-pos)]
      (loop [seen (into #{} box)
             [box-positions & bs] (list (into #{} box))]
        (let [new-boxes (->> box-positions
                             (map (comp #(box? grid %) #(vec/v+ dir %)))
                             (filter some?)
                             (mapcat identity)
                             (filter #(not (seen %)))
                             (into #{}))]
          (if (seq new-boxes)
            (recur (into seen new-boxes) (conj bs box-positions new-boxes))
            (->> box-positions
                 (conj bs)
                 (map (comp #(into #{} %) #(map (partial box? grid) %)))))))
      nil))
  (is (empty? (affected-boxes ["."] [0 0] [0 1])))
  (is (= '(#{[[0 0] [0 1]]})
         (affected-boxes ["[]"] [0 0] [0 1])))
  (is (= '(#{[[0 2] [0 3]]} #{[[0 0] [0 1]]})
         (affected-boxes ["[][]"] [0 0] [0 1])))
  (is (= '(#{[[0 0] [0 1]]} #{[[0 2] [0 3]]})
         (affected-boxes ["[][]"] [0 3] [0 -1])))
  (is (= '(#{[[0 0] [0 1]] [[0 2] [0 3]]} #{[[1 1] [1 2]]})
         (affected-boxes ["[][]"
                          ".[]."] [1 1] [-1 0]))))
(defn- make-move
  [grid pos move]
  (let [dir (get dirs move)]
    (if (nil? dir)
      [pos grid]
      (let [next-pos (vec/v+ pos dir)]
        (letfn [(update-robot [grid] (-> grid (assoc-grid pos \.) (assoc-grid next-pos \@)))]
          (if (free? grid next-pos)
            [next-pos (update-robot grid)]
            (if-let [new-grid (some->>
                               (affected-boxes grid next-pos dir)
                               (reduce
                                (partial reduce
                                         (fn [g box]
                                           (if (can-move-box? g dir box)
                                             (move-box g box dir)
                                             (reduced nil))))
                                grid)
                               update-robot)]
              [next-pos new-grid]
              [pos grid])))))))

(defn- make-moves
  [{:keys [grid moves robot]}]
  (reduce
   (fn [[pos g] move]
     (make-move g pos move))
   [robot grid]
   moves))

(defn- gps-coordinates
  [grid]
  (for [row (range (count grid))
        col (range (count (first grid)))
        :let [c (get-in grid [row col])]
        :when (or (= \[ c) (= \O c))]
    (+ col (* 100 row))))

(with-test
  (defn part-1
    [input]
    (let [[_ grid] (make-moves (parse input))]
      (apply + (gps-coordinates grid))))
  (is (= 2028 (part-1 small-example)))
  (is (= 10092 (part-1 example-input)))
  (is (= 1426855 (part-1 (slurp "inputs/day15.txt")))))

(defn- widen-grid
  [{:keys [grid moves]}]
  (let [new-grid (mapv (comp #(str/replace % #"@" "@.")
                             #(str/replace % #"#" "##")
                             #(str/replace % #"O" "[]")
                             #(str/replace % #"\." "..")) grid)]
    {:grid new-grid :moves moves :robot (find-start-position new-grid)}))

(with-test
  (defn part-2
    [input]
    (apply + (-> input
                 parse
                 widen-grid
                 make-moves
                 second ; remove position
                 gps-coordinates)))
  (is (= 9021 (part-2 example-input)))
  (is (= 1404917 (part-2 (slurp "inputs/day15.txt")))))
