(ns aoc.day08
  (:require
   [aoc.vec :as vec]
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]))

(def example-input "............
........0...
.....0......
.......0....
....0.......
......A.....
............
............
........A...
.........A..
............
............")

(defrecord antenna [pos freq])
(with-test
  (defn- parse
    [input]
    (let [grid (str/split-lines input)
          nrows (count grid)
          ncols (count (first grid))
          antennas (for [row (range nrows)
                         col (range ncols)
                         :let [f (get-in grid [row col])]
                         :when (not= \. f)]
                     (->antenna [row col] f))]
      {:grid grid :antennas antennas}))
  (is (= [(->antenna [1 1] \0)] (->> "...\n.0." parse :antennas))))

(defn- snd
  [xs]
  (take 1 (drop 1 xs)))

(with-test
  (defn- antinodes
    [grid select p1 p2]
    (let [dist (mapv - p2 p1)]
      (concat (select (take-while #(get-in grid %) (iterate #(vec/v- % dist) p1)))
              (select (take-while #(get-in grid %) (iterate #(vec/v+ % dist) p2))))))
  (is (= [[1 3] [7 6]]
         (-> example-input parse :grid (antinodes snd [3 4] [5 5])))))

(with-test
  (defn- antinodes-of-all-pairs
    [grid select ps]
    (apply concat (for [i (range (count ps))
                        j (range (inc i) (count ps))]
                    (antinodes grid select (ps i) (ps j)))))
  (is (= [[2 0] [1 3] [7 6] [6 2]]
         (antinodes-of-all-pairs [".........."
                                  ".........."
                                  ".........."
                                  "....a....."
                                  "........a."
                                  ".....a...."
                                  ".........."
                                  ".........."
                                  ".........."
                                  ".........."] snd [[3 4] [4 8] [5 5]]))
      (= #{[0 0] [0 5] [1 3] [2 1] [2 6] [3 9] [4 2] [6 3] [8 4]}
         (set (antinodes-of-all-pairs ["T........."
                                       "...T......"
                                       ".T........"
                                       ".........."
                                       ".........."
                                       ".........."
                                       ".........."
                                       ".........."
                                       ".........."
                                       ".........."] identity [[0 0] [1 3] [2 1]])))))

(with-test
  (defn part-1
    [input]
    (let [{:keys [antennas grid]} (parse input)]
      (->> (group-by :freq antennas)
           vals
           (map #(mapv :pos %))
           (mapcat #(antinodes-of-all-pairs grid snd %))
           distinct
           count)))
  (is (= 14 (part-1 example-input)))
  (is (= 308 (part-1 (slurp "inputs/day08.txt")))))

(with-test
  (defn part-2
    [input]
    (let [{:keys [antennas grid]} (parse input)]
      (->> (group-by :freq antennas)
           vals
           (map #(mapv :pos %))
           (mapcat #(antinodes-of-all-pairs grid identity %))
           distinct
           count)))
  (is (= 34 (part-2 example-input)))
  (is (= 1147 (part-2 (slurp "inputs/day08.txt")))))
