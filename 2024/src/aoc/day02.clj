(ns aoc.day02
  (:require
   [clojure.string :as str]
   [clojure.test :as test :refer [with-test is]]))

(def example-input "7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9")

(defn- parse-row
  [row]
  (mapv parse-long (str/split row #"\s+")))

(with-test
  (defn- parse
    [input]
    (some->> input
             str/split-lines
             (mapv parse-row)
             (apply vector)))
  (is (= [[1 2 3 4]] (parse "1 2 3 4")))
  (is (= [[1 2] [3 4]] (parse "1 2\n3 4"))))

(with-test
  (defn- levels-are-safe
    [levels]
    (as-> levels l
      (map - l (drop 1 l))
      (or (every? #(and (> % 0) (<= % 3)) l)
          (every? #(and (< % 0) (>= % -3)) l))))
  (is (levels-are-safe [1 2 3])))

(with-test
  (defn part-1
    [input]
    (let [report (parse input)]
      (->> report
           (filter levels-are-safe)
           count)))
  (is (= 2 (part-1 example-input)))
  (is (= 591 (part-1 (slurp "inputs/day02.txt")))))

(with-test
  (defn- levels-are-safe-with-dampener [levels]
    (let [len (count levels)]
      (some
       levels-are-safe
       (for [i (range 0 (count levels))]
         (concat (subvec levels 0 i) (subvec levels (+ i 1) len))))))
  (is (levels-are-safe-with-dampener [7 6 4 2 1]))
  (is (not (levels-are-safe-with-dampener [1 2 7 8 9])))
  (is (not (levels-are-safe-with-dampener [9 7 6 2 1])))
  (is (levels-are-safe-with-dampener [1 3 2 4 5]))
  (is (levels-are-safe-with-dampener [8 6 4 4 1]))
  (is (levels-are-safe-with-dampener [1 3 6 7 9])))

(with-test
  (defn part-2 [input]
    (let [report (parse input)]
      (->> report
           (filter levels-are-safe-with-dampener)
           count)))
  (is (= 4 (part-2 example-input)))
  (is (= 621 (part-2 (slurp "inputs/day02.txt")))))

