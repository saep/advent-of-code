(ns aoc.day12
  (:require
   [aoc.vec :as vec]
   [clojure.set :as set]
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]))

(def example-inputs
  ["AAAA
BBCD
BBCC
EEEC"
   "OOOOO
OXOXO
OOOOO
OXOXO
OOOOO"
   "RRRRIICCFF
RRRRIICCCF
VVRRRCCFFF
VVRCCCJFFF
VVVVCJJCFE
VVIVCCJJEE
VVIIICJJEE
MIIIIIJJEE
MIIISIJEEE
MMMISSJEEE"
   "EEEEE
EXXXX
EEEEE
EXXXX
EEEEE"
   "AAAAAA
AAABBA
AAABBA
ABBAAA
ABBAAA
AAAAAA"])

(defn- positions
  [garden-plots]
  (for [row (range (count garden-plots))
        col (range (count (first garden-plots)))]
    [row col]))

(defn- neighbors
  [garden-plots pos]
  (->> [[-1 0] [1 0] [0 -1] [0 1]]
       (map (comp #(vector % (get-in garden-plots %)) #(vec/v+ pos %)))
       (filter (comp some? second))))

(with-test
  (defn- expand-region
    [garden-plots start]
    (let [c (get-in garden-plots start)]
      (loop [remaining #{start}
             region #{}]
        (if-let [pos (first remaining)]
          (recur (->> (neighbors garden-plots pos)
                      (filter #(= c (second %)))
                      (map first)
                      (filter #(not (contains? region %)))
                      (apply conj (set/difference remaining #{pos})))
                 (conj region pos))
          region))))
  (is (= #{[0 0]}
         (expand-region ["ab" "bb"] [0 0])))
  (is (= #{[0 1] [1 0] [1 1]}
         (expand-region ["ab" "bb"] [1 0])))
  (is (= #{[4 3] [2 2] [0 0] [1 0] [2 3] [3 4] [4 2] [3 0] [4 1] [1 4]
           [0 3] [2 4] [0 2] [2 0] [0 4] [2 1] [4 4] [1 2] [3 2] [0 1] [4 0]}
         (expand-region (str/split-lines (get example-inputs 1)) [4 3]))))

(with-test
  (defn- assign-regions
    [garden-plots]
    (loop [remaining (set (positions garden-plots))
           regions (transient {})
           id 0]
      (if-let [start (first remaining)]
        (let [region (expand-region garden-plots start)]
          (recur (set/difference remaining region)
                 (assoc! regions id region)
                 (inc id)))
        (persistent! regions))))
  (is (= 1 (count (assign-regions ["AAAA"]))))
  (is (= 4 (count (assign-regions ["AAAA" "BBCD"]))))
  (is (= 2 (count (assign-regions ["ab" "bb"])))))

(defn- fence-price
  [garden-plots regions]
  (letfn [(count-fences
            [pos]
            (->> [[-1 0] [1 0] [0 -1] [0 1]]
                 (map #(get-in garden-plots (vec/v+ pos %)))
                 (filter #(not= % (get-in garden-plots pos)))
                 count))]
    (->> regions
         vals
         (map #(* (count %) (apply + (map count-fences %))))
         (apply +))))

(with-test
  (defn part-1
    [input]
    (let [garden-plots (str/split-lines input)
          regions      (assign-regions garden-plots)]
      (fence-price garden-plots regions)))
  (is (= 140 (part-1 (get example-inputs 0))))
  (is (= 772 (part-1 (get example-inputs 1))))
  (is (= 1930 (part-1 (get example-inputs 2))))
  (is (= 1550156 (part-1 (slurp "inputs/day12.txt")))))

(def up [-1 0])
(def right [0 1])
(def down [1 0])
(def left [0 -1])

(def corners [[up left] [up right] [down left] [down right]])

(defn- fence-price-sides
  [garden-plots regions]
  (letfn [(count-corners
            [pos]
            (let [c (get-in garden-plots pos)]
              (->> corners
                   (filter (fn [corner]
                             (case (mapv (comp #(= % c) #(get-in garden-plots %) #(vec/v+ pos %)) corner)
                               [false, false] true
                               [true, true] (not= c (get-in garden-plots (apply vec/v+ pos corner)))
                               false)))
                   count)))]
    (->>  regions
          vals
          (map (comp #(* (apply + %) (count %)) #(map count-corners %)))
          (apply +))))

(with-test
  (defn part-2
    [input]
    (let [garden-plots (str/split-lines input)
          regions      (assign-regions garden-plots)]
      (fence-price-sides garden-plots regions)))
  (is (= 80 (part-2 (get example-inputs 0))))
  (is (= 436 (part-2 (get example-inputs 1))))
  (is (= 1206 (part-2 (get example-inputs 2))))
  (is (= 236 (part-2 (get example-inputs 3))))
  (is (= 368 (part-2 (get example-inputs 4))))
  (is (= 946084 (part-2 (slurp "inputs/day12.txt")))))
