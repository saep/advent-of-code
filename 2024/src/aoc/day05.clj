(ns aoc.day05
  (:require
   [clojure.string :as str]
   [clojure.set :as set]
   [clojure.test :refer [with-test is]]))

(def example-input "47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47")

(with-test
  (defn- parse-ordering
    [line]
    (mapv parse-long (str/split line #"\|")))
  (is (= [97 13] (parse-ordering "97|13"))))

(with-test
  (defn parse-updates
    [line]
    (mapv parse-long (str/split line #",")))
  (is (= [75 47 61 53 29] (parse-updates "75,47,61,53,29"))))

(with-test
  (defn- parse
    [input]
    (let [[orderings updates] (str/split input #"\n\n")]
      {:orderings (->> orderings
                       str/split-lines
                       (map parse-ordering)
                       (reduce
                        (fn [acc [k v]] (update acc k #(if %1 (conj %1 %2) #{%2}) v))
                        {}))
       :updates (mapv parse-updates (str/split-lines updates))}))
  (is (= {:orderings {47 #{127 61}} :updates [[61 13 29]]} (parse "47|61\n47|127\n\n61,13,29"))))

(with-test
  (defn- valid-updates?
    [orderings updates]
    (->> updates
         reverse
         (reduce
          (fn [cant-update page]
            (if (cant-update page)
              (reduced false)
              (if-let [after (orderings page)]
                (set/union cant-update after)
                cant-update)))
          #{})
         false?
         not))
  (is (valid-updates? {} [1 2 3]))
  (is (not (valid-updates? {61 #{75 76}} [75 61])))
  (is (not (valid-updates? {1 #{2}, 3 #{4}} [1 4 2 3]))))

(defn- middle
  [coll]
  (get coll (int (/ (count coll) 2))))

(with-test
  (defn part-1 [input]
    (let [{:keys [orderings updates]} (parse input)]
      (->> updates
           (filter #(valid-updates? orderings %))
           (map middle)
           (apply +))))
  (is (= 143 (part-1 example-input)))
  (is (= 4637 (part-1 (slurp "inputs/day05.txt")))))

(with-test
  (defn- fix-update-order
    [orderings updates]
    (loop [i (dec (count updates))
           fixed updates]
      (if (< i 0)
        fixed
        (let [must-be-before (->> (orderings (fixed i))
                                  (map #(.indexOf fixed %))
                                  (filter #(not= -1 %))
                                  (apply min i))]
          (if (= i must-be-before)
            (recur (dec i) fixed)
            (recur i (apply vector (concat (subvec fixed 0 must-be-before)
                                           [(fixed i)]
                                           (subvec fixed must-be-before i)
                                           (subvec fixed (inc i))))))))))
  (is (= [1 2 3] (fix-update-order {1 #{2}, 2 #{3}} [3 2 1]))))

(with-test
  (defn part-2
    [input]
    (let [{:keys [orderings updates]} (parse input)]
      (->> updates
           (filter #(not (valid-updates? orderings %)))
           (map (comp middle #(fix-update-order orderings %)))
           (apply +))))
  (is (= 123 (part-2 example-input)))
  (is (= 6370 (part-2 (slurp "inputs/day05.txt")))))


