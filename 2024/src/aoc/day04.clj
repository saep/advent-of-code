(ns aoc.day04
  (:require
   [aoc.vec :as vec]
   [clojure.string :as str]
   [clojure.test :as test :refer [is with-test]]))

(def example-input "MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX")

(defn- directions
  []
  (for [row [-1 0 1]
        col [-1 0 1]
        :when (not (and  (= 0 row) (= 0 col)))]
    [row col]))

(with-test
  (defn word
    [grid len pos dir]
    (->> (iterate #(vec/v+ dir %) pos)
         (take len)
         (map #(get-in grid %))
         str/join))
  (is (= "XMAS" (word ["Merry XMAS"] 4 [0 6] [0 1]))))

(with-test
  (defn part-1
    [input]
    (let [grid (str/split-lines input)
          all-words (for [row (range (count grid))
                          col (range (count (first grid)))
                          dir (directions)]
                      (word grid 4 [row col] dir))]
      (count (filter #(= "XMAS" %) all-words))))
  (is (= 18 (part-1 example-input)))
  (is (= 2644 (part-1 (slurp "inputs/day04.txt")))))

(with-test
  (defn part-2
    [input]
    (let [grid (str/split-lines input)
          get-word (fn [ps] (str/join (map #(get-in grid %) ps)))
          all-x-words (for [row (range 1 (dec (count grid)))
                            col (range 1 (dec (count (first grid))))
                            :let [pos [row col]]]
                        (mapv get-word [[(vec/v+ pos [-1 -1]) pos (vec/v+ pos [1 1])]
                                        [(vec/v+ pos [1 -1]) pos (vec/v+ pos [-1 1])]]))]
      (->> all-x-words
           (filter (fn [x-words] (every? #(or (= "MAS" %) (= "SAM" %)) x-words)))
           count)))
  (is (= 9 (part-2 example-input)))
  (is (= 1952 (part-2 (slurp "inputs/day04.txt")))))
