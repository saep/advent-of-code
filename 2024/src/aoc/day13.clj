(ns aoc.day13
  (:require
   [clojure.math :as math]
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]
   [aoc.vec :as vec]
   [uncomplicate.neanderthal.core :as n]
   [uncomplicate.neanderthal.linalg :as linalg]
   [uncomplicate.neanderthal.native :as native]))

(def example-input "Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400

Button A: X+26, Y+66
Button B: X+67, Y+21
Prize: X=12748, Y=12176

Button A: X+17, Y+86
Button B: X+84, Y+37
Prize: X=7870, Y=6450

Button A: X+69, Y+23
Button B: X+27, Y+71
Prize: X=18641, Y=10279")

(def ^:private ^:const REGEX_BUTTON #"Button (?<button>[AB]): X\+(?<x>\d+), Y\+(?<y>\d+)")
(def ^:private ^:const REGEX_PRIZE #"Prize: X=(?<x>\d+), Y=(?<y>\d+)")
(with-test
  (defn- parse-xy
    [regex line]
    (let [m (re-matcher regex line)]
      (when (.matches m)
        (mapv parse-long [(.group m "x") (.group m "y")]))))
  (is (= [94 34] (parse-xy REGEX_BUTTON (get (str/split-lines example-input) 0)))))

(with-test
  (defn- parse-claw-machine
    [[a b prize]]
    {:a (parse-xy REGEX_BUTTON a)
     :b (parse-xy REGEX_BUTTON b)
     :prize (parse-xy REGEX_PRIZE prize)})
  (is (= {:a [94 34] :b [22 67] :prize [8400 5400]}
         (parse-claw-machine (str/split-lines (first (str/split example-input #"\n\n")))))))

(defn- parse-claw-machines
  [input]
  (->> (str/split input #"\n\n")
       (map (comp parse-claw-machine str/split-lines))))

(with-test
  (defn- valid-solution
    [{:keys [a b prize]} sol]
    (let [[ta tb] (mapv math/round sol)
          prize' (->> (n/xpy (n/scal! ta (native/dv a)) (n/scal! tb (native/dv b)))
                      (into [])
                      (mapv long))]
      (when (= prize prize') [ta tb])))
  (is (valid-solution {:a [94 34] :b [22 67] :prize [8400 5400]} [80 40])))

(with-test
  (defn- solve
    [claw-machine]
    (let [{:keys [a b prize]} claw-machine
          ma (native/dge [a b])
          lu (linalg/trf ma)
          vb (native/dge [prize])]
      (->> (linalg/trs! lu vb)
           n/view-vctr
           (into [])
           (valid-solution claw-machine))))
  (is (= [[80 40] nil [38 86] nil] (mapv solve (parse-claw-machines example-input)))))

(defn- num-tokens
  [v]
  (apply + (vec/v* [3 1] v)))

(with-test
  (defn part-1
    [input]
    (->> input
         parse-claw-machines
         (map solve)
         (filter some?)
         (map num-tokens)
         (apply +)))
  (is (= 480 (part-1 example-input)))
  (is (= 40369 (part-1 (slurp "inputs/day13.txt")))))

(with-test
  (defn part-2
    [input]
    (->> input
         parse-claw-machines
         (map #(update % :prize (partial vec/s+v 10000000000000)))
         (map solve)
         (filter some?)
         (map num-tokens)
         (apply +)))
  (is (= 875318608908 (part-2 example-input)))
  (is (= 72587986598368 (part-2 (slurp "inputs/day13.txt")))))
