(ns aoc.day11
  (:require
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]))

(with-test
  (defn- split-even
    [n]
    (let [s (str n)
          len (count s)]
      (when (even? len)
        (mapv parse-long [(subs s 0 (bit-shift-right len 1))
                          (subs s (bit-shift-right len 1) len)]))))
  (is (= [10 0] (split-even 1000)))
  (is (nil? (split-even 123))))

(defn- blink
  [s]
  (if (= 0 s)
    [1]
    (if-let [halves (split-even s)]
      halves
      [(* 2024 s)])))

(with-test
  (defn- memoized-blink
    [memory times stone]
    (if (some? (get-in memory [times stone]))
      memory
      (if (<= times 0)
        (assoc-in memory [times stone] 1)
        (let [filled-memory
              (reduce (fn [m s]
                        (memoized-blink m (dec times) s))
                      memory
                      (blink stone))]
          (assoc-in filled-memory
                    [times stone]
                    (->> (blink stone)
                         (map #(get-in filled-memory [(dec times) %]))
                         (apply +)))))))
  (is (= 1 (get-in (memoized-blink {} 0 17) [0 17])))
  (is (= 2 (get-in (memoized-blink {} 1 17) [1 17])))
  (is (= 15 (get-in (memoized-blink {} 6 17) [6 17])))
  (is (= 7 (get-in (memoized-blink {} 6 125) [6 125]))))

(with-test
  (defn memoized-blinks
    [times stones]
    (let [memory (reduce (fn [mem stone] (memoized-blink mem times stone)) {} stones)]
      (->> stones
           (map #(get-in memory [times %]))
           (apply +))))
  (is (= 22 (memoized-blinks 6 [17 125])))
  (is (= 55312 (memoized-blinks 25 [17 125]))))

(with-test
  (defn part-1
    [input]
    (->> (str/split input #"\s+")
         (mapv parse-long)
         (memoized-blinks 25)))
  (is (= 55312 (part-1 "125 17")))
  (is (= 183435 (part-1 (slurp "inputs/day11.txt")))))

(with-test
  (defn part-2
    [input]
    (->> (str/split input #"\s+")
         (mapv parse-long)
         (memoized-blinks 75)))
  (is (= 218279375708592 (part-2 (slurp "inputs/day11.txt")))))



