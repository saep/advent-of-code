(ns aoc.day10
  (:require
   [aoc.utils :refer [chr->int]]
   [aoc.vec :as vec]
   [clojure.set :as set]
   [clojure.string :as str]
   [clojure.test :refer [is with-test]]))

(def example-input
  ["0123\n1234\n8765\n9876"
   "...0...\n...1...\n...2...\n6543456\n7.....7\n8.....8\n9.....9"
   "..90..9\n...1.98\n...2..7\n6543456\n765.987\n876....\n987...."
   "10..9..\n2...8..\n3...7..\n4567654\n...8..3\n...9..2\n.....01"
   "89010123\n78121874\n87430965\n96549874\n45678903\n32019012\n01329801\n10456732"])

(defn- parse
  [input]
  (->> (str/split input #"\n")
       (mapv #(mapv chr->int %))))

(defn- dfs-hiking-trail-to-9
  [topographic-map start]
  (letfn [(go [visited height pos]
              (if (= 9 height)
                #{pos}
                (->> [[-1 0] [0 -1] [0 1] [1 0]]
                     (map #(vec/v+ pos %))
                     (filter #(and (= (inc height) (get-in topographic-map %))
                                   (not (contains? visited %))))
                     (map #(go (into visited %) (inc height) %))
                     (apply set/union))))]
    (go #{start} 0 start)))

(with-test
  (defn- starting-positions
    [topographic-map]
    (for [row (range (count topographic-map))
          col (range (count (first topographic-map)))
          :when (= 0 (get-in topographic-map [row col]))]
      [row col]))
  (is (= [[0 0]] (starting-positions (parse (get example-input 0))))))

(with-test
  (defn part-1
    [input]
    (let [topographic-map (parse input)]
      (->> (starting-positions topographic-map)
           (map #(dfs-hiking-trail-to-9 topographic-map %))
           (map count)
           (apply +))))
  (is (= 1 (part-1 (example-input 0))))
  (is (= 2 (part-1 (example-input 1))))
  (is (= 4 (part-1 (example-input 2))))
  (is (= 3 (part-1 (example-input 3))))
  (is (= 36 (part-1 (example-input 4))))
  (is (= 652 (part-1 (slurp "inputs/day10.txt")))))

(defn- dfs-num-distinct-hiking-trails
  [topographic-map start]
  (letfn [(go [visited height pos]
              (if (= 9 height)
                1
                (->> [[-1 0] [0 -1] [0 1] [1 0]]
                     (map #(vec/v+ pos %))
                     (filter #(and (= (inc height) (get-in topographic-map %))
                                   (not (contains? visited %))))
                     (map #(go (into visited %) (inc height) %))
                     (apply +))))]
    (go #{start} 0 start)))

(with-test (defn part-2
             [input]
             (let [topographic-map (parse input)]
               (->> topographic-map
                    starting-positions
                    (map #(dfs-num-distinct-hiking-trails topographic-map %))
                    (apply +))))
  (is (= 81 (part-2 (get example-input 4))))
  (is (= 1432 (part-2 (slurp "inputs/day10.txt")))))
