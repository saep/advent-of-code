(ns aoc.day09
  (:require
   [clojure.test :refer [is with-test]]))

(def example-input "2333133121414131402")

(defn- chr->int
  [c]
  (- (int c) (int \0)))

(defn- parse
  [input]
  (mapv chr->int input))

(with-test
  (defn- sum-from-to
    "Same as (apply + (range from to)) "
    [from to]
    (- (bit-shift-right (* (dec to) to) 1)
       (bit-shift-right (* from (dec from)) 1)))
  (is (= 0 (sum-from-to 0 0)))
  (is (= 0 (sum-from-to 0 1)))
  (is (= 45 (sum-from-to 0 10)))
  (is (= 0 (sum-from-to 7 7)))
  (is (= 35 (sum-from-to 5 10))))

(with-test
  (defn- checksum
    [blocks]
    (loop [acc 0
           front 0
           rfront (get blocks front)
           rs 0 ; range start
           back (-> blocks count dec (bit-clear 0))
           rback (get blocks back)]
      (cond
        (> front back) acc
        (= front back) (let [re (+ rs rback)]
                         (+ acc (* (bit-shift-right front 1) (sum-from-to rs re))))
        (even? front) (let [re (+ rs rfront)]
                        (recur
                         (+ acc (* (bit-shift-right front 1) (sum-from-to rs re)))
                         (+ 1 front)
                         (get blocks (+ 1 front) 0)
                         re
                         back
                         rback))
        (>= rback rfront) (let [re (+ rs rfront)]
                            (recur
                             (+ acc (* (bit-shift-right back 1) (sum-from-to rs re)))
                             (+ 1 front)
                             (get blocks (+ 1 front) 0)
                             re
                             back
                             (- rback rfront)))
        :else (let [re (+ rs rback)]
                (recur
                 (+ acc (* (bit-shift-right back 1) (sum-from-to rs re)))
                 front
                 (- rfront rback)
                 re
                 (- back 2)
                 (blocks (- back 2)))))))
  (is (= 1
         (-> "002" parse checksum)))
  ;0..111....22222
  ;022111222......
  (is (= (apply + (map * (range) [0 2 2 1 1 1 2 2 2]))
         (-> "12345" parse checksum)))
  ;.111
  ;111.
  (is (= (apply + (map * (range) [1 1 1]))
         (-> "013" parse checksum))))

(with-test
  (defn part-1
    [input]
    (-> input parse checksum))
  (is (= 1928 (part-1 example-input)))
  (is (= 6330095022244 (part-1 (slurp "inputs/day09.txt")))))

(defn- parse-grouped-by-length
  [input]
  (->> input
       (map #(vector %1 %2) (range))
       (filter #(even? (% 0)))
       (reduce
        (fn [acc [i c]]
          (let [len (chr->int c)]
            (if (= 0 len)
              acc
              (update acc (chr->int c) #(conj % (bit-shift-right i 1))))))
        (mapv (constantly nil) (range 10)))))

(with-test
  (defn- max-ix-fitting-in-gap
    [gap-size grouped-by-length]
    (if (< gap-size 1) nil
        (let [ix (->> (range 1 (inc gap-size))
                      (apply max-key #(or (first (get grouped-by-length %)) -1)))]
          (if (empty? (grouped-by-length ix)) nil ix))))
  (is (nil? (max-ix-fitting-in-gap -38 [])))
  (is (nil? (max-ix-fitting-in-gap 7 (mapv (constantly nil) (range 8)))))
  (is (= 1 (max-ix-fitting-in-gap 2 [nil '(3) nil '(4 5)])))
  (is (= 2 (max-ix-fitting-in-gap 2 [nil '(1) '(2 4 5) '(3 8)]))))

(with-test
  (defn- defrag-checksum
    [input]
    (let [len (count input)
          at-pos #(chr->int (get input %))]
      (loop [remaining (parse-grouped-by-length input)
             rg nil ; remaining gap
             gaps (into #{} (range 1 (inc len) 2))
             pos 0
             sr 0
             checksum 0]
        (cond
          (>= pos len) checksum
          (= 0 rg) (recur remaining nil gaps (inc pos) sr checksum)
          (some? rg) (let [ix (max-ix-fitting-in-gap rg remaining)]
                       (if (nil? ix)
                         (recur remaining nil gaps (inc pos) (+ sr rg) checksum)
                         (let [[id & rs] (remaining ix)
                               pos-id (bit-shift-left id 1)
                               er (+ sr ix)]
                           (if (< pos-id pos)
                             (recur (assoc remaining ix rs) rg gaps pos sr checksum)
                             (recur (assoc remaining ix rs)
                                    (- rg ix)
                                    (conj gaps pos-id)
                                    pos
                                    er
                                    (+ checksum (* id (sum-from-to sr er))))))))
          (gaps pos) (recur remaining (at-pos pos) gaps pos sr checksum)
          :else (let [er (+ sr (at-pos pos))]
                  (recur remaining
                         nil
                         gaps
                         (inc pos)
                         er
                         (+ checksum (* (bit-shift-right pos 1) (sum-from-to sr er)))))))))
  (is (= (apply + (map * (range) [0 0 2 0 0 1 1 1])) ;00...111.2
         (defrag-checksum "23301")))
  (is (= (apply + (map * (range) [0 0 3 3 2 1 1 1])) ;00...111233
         (defrag-checksum "2330102"))))

(with-test
  (defn part-2
    [input]
    (defrag-checksum input))
  (is (= 2858 (part-2 example-input)))
  (is (= 6359491814941 (part-2 (slurp "inputs/day09.txt")))))
