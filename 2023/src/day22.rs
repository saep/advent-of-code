use std::{
    cmp::Reverse,
    collections::{BinaryHeap, HashMap, HashSet},
    ops::Index,
};

use bitvec::vec::BitVec;
use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::{i32, newline},
    combinator::map,
    multi::separated_list1,
    sequence::{separated_pair, terminated, tuple},
    IResult,
};

use crate::three_dimensional::Cuboid;

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day22_1.txt").expect("file to exist");
    FallingBricks::parse(&input)
        .expect("parsable input")
        .1
        .settle()
        .count_single_disintegratable()
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day22_1.txt").expect("file to exist");
    FallingBricks::parse(&input)
        .expect("parsable input")
        .1
        .settle()
        .sum_of_bricks_falling_by_disintegrating_a_single_brick()
}

#[derive(Debug)]
struct FallingBricks {
    bricks: Vec<Brick>,
}

impl FallingBricks {
    fn parse(input: &str) -> IResult<&str, FallingBricks> {
        map(
            terminated(separated_list1(newline, Brick::parse), newline),
            |bricks| FallingBricks { bricks },
        )(input)
    }

    fn settle(self) -> SettledBricks {
        let bricks = self
            .bricks
            .into_iter()
            .sorted_by_key(|brick| brick.bottom())
            .fold(Vec::<Brick>::new(), |mut result, mut brick| {
                let highest_level_of_intersecting_brick = result
                    .iter()
                    .filter_map(|b| {
                        if b.has_collision_xy(&brick) {
                            Some(b.top())
                        } else {
                            None
                        }
                    })
                    .max()
                    .unwrap_or(0);
                brick.raise(1 + highest_level_of_intersecting_brick - brick.bottom());
                result.push(brick);
                result
            });
        let mut by_bottom: HashMap<i32, Vec<usize>> = HashMap::new();
        let mut by_top: HashMap<i32, Vec<usize>> = HashMap::new();
        bricks.iter().enumerate().for_each(|(i, b)| {
            by_bottom.entry(b.bottom()).or_default().push(i);
            by_top.entry(b.top()).or_default().push(i)
        });
        SettledBricks {
            bricks,
            by_bottom,
            by_top,
        }
    }
}

#[derive(Debug)]
struct SettledBricks {
    bricks: Vec<Brick>,
    by_bottom: HashMap<i32, Vec<usize>>,
    by_top: HashMap<i32, Vec<usize>>,
}

impl Index<usize> for SettledBricks {
    type Output = Brick;

    fn index(&self, index: usize) -> &Self::Output {
        &self.bricks[index]
    }
}

impl SettledBricks {
    fn is_supported_by_multiple_bricks(&self, brick_ix: usize) -> bool {
        let brick = &self[brick_ix];
        let support_level = brick.bottom() - 1;
        match self.by_top.get(&support_level) {
            None => false,
            Some(supports) => {
                supports
                    .iter()
                    .filter(|i| self[**i].has_collision_xy(brick))
                    .take(2)
                    .count()
                    > 1
            }
        }
    }

    fn is_only_support_for_at_least_one_brick(&self, brick_ix: usize) -> bool {
        let brick = &self[brick_ix];
        let brick_level = brick.top();
        match self.by_bottom.get(&(brick_level + 1)) {
            None => false,
            Some(supports) => supports
                .iter()
                .copied()
                .filter(|i| brick.has_collision_xy(&self[*i]))
                .any(|i| !self.is_supported_by_multiple_bricks(i)),
        }
    }

    fn count_single_disintegratable(&self) -> usize {
        (0..self.bricks.len())
            .filter(|i| !self.is_only_support_for_at_least_one_brick(*i))
            .count()
    }

    fn sum_of_bricks_falling_by_disintegrating_a_single_brick(&self) -> usize {
        (0..self.bricks.len())
            .map(|i| self.count_falling_bricks(i))
            .sum()
    }

    fn count_falling_bricks(&self, brick_ix: usize) -> usize {
        let mut fallen: BitVec = BitVec::new();
        fallen.resize(self.bricks.len(), false);
        fallen.set(brick_ix, true);

        let mut visited_levels = HashSet::new();
        let mut remaining_levels = BinaryHeap::new();
        remaining_levels.push(Reverse(self[brick_ix].top()));
        while let Some(Reverse(level)) = remaining_levels.pop() {
            visited_levels.insert(level);
            let supports: Vec<_> = match self.by_top.get(&level) {
                None => continue,
                Some(supports) => supports
                    .iter()
                    .filter(|b| !fallen[**b])
                    .map(|b| &self[*b])
                    .collect(),
            };
            match self.by_bottom.get(&(level + 1)) {
                None => continue,
                Some(bricks) => bricks.iter().for_each(|b| {
                    let brick = &self[*b];
                    if supports
                        .iter()
                        .filter(|support| support.has_collision_xy(brick))
                        .take(1)
                        .count()
                        == 0
                    {
                        fallen.set(*b, true);
                        if visited_levels.insert(brick.top()) {
                            remaining_levels.push(Reverse(brick.top()));
                        }
                    }
                }),
            };
        }

        fallen.count_ones() - 1 // desintegrated
    }
}

type Brick = Cuboid<i32>;

impl Brick {
    fn parse(input: &str) -> IResult<&str, Brick> {
        let xyz = |input| {
            map(
                tuple((terminated(i32, tag(",")), terminated(i32, tag(",")), i32)),
                |xyz| xyz.into(),
            )(input)
        };
        map(separated_pair(xyz, tag("~"), xyz), |points| points.into())(input)
    }

    fn top(&self) -> i32 {
        self.top_right.z
    }

    fn bottom(&self) -> i32 {
        self.bottom_left.z
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(522, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(83519, solution2());
    }

    #[test]
    fn parse_brick() {
        let actual = Brick::parse("1,0,1~1,2,1").expect("valid parse");
        assert_eq!("", actual.0);
        assert_eq!(actual.1, ([1, 0, 1], [1, 2, 1]).into());
    }

    #[test]
    fn parse_falling_bricks() {
        let two_bricks = indoc! {"
            1,2,3~4,5,6
            7,8,9~0,0,0
            "};
        let actual = FallingBricks::parse(two_bricks).expect("valid parse");
        assert_eq!("", actual.0);
        let actual = actual.1.bricks;
        assert_eq!(
            actual,
            vec![([1, 2, 3], [4, 5, 6]).into(), ([7, 8, 9], [0, 0, 0]).into()]
        );
    }

    const EXAMPLE: &str = indoc! {"
        1,0,1~1,2,1
        0,0,2~2,0,2
        0,2,3~2,2,3
        0,0,4~0,2,4
        2,0,5~2,2,5
        0,1,6~2,1,6
        1,1,8~1,1,9
        "};

    #[test]
    fn disintegrate_single_bricks_example() {
        let actual = FallingBricks::parse(EXAMPLE).expect("parser must succeed");
        assert_eq!("", actual.0);
        let actual = actual.1.settle().count_single_disintegratable();
        assert_eq!(actual, 5);
    }

    #[test]
    fn sum_of_bricks_falling_by_disintegrating_a_single_brick_example() {
        let actual = FallingBricks::parse(EXAMPLE).expect("parser must succeed");
        assert_eq!("", actual.0);
        let actual = actual
            .1
            .settle()
            .sum_of_bricks_falling_by_disintegrating_a_single_brick();
        assert_eq!(actual, 7);
    }
}
