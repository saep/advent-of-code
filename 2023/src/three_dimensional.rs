use std::ops::{Add, AddAssign};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub struct Point3D<I = i32> {
    pub x: I,
    pub y: I,
    pub z: I,
}

impl<I: Copy> From<[I; 3]> for Point3D<I> {
    fn from(value: [I; 3]) -> Self {
        Point3D {
            x: value[0],
            y: value[1],
            z: value[2],
        }
    }
}

impl<I: Copy> From<(I, I, I)> for Point3D<I> {
    fn from(value: (I, I, I)) -> Self {
        Point3D {
            x: value.0,
            y: value.1,
            z: value.2,
        }
    }
}

impl<I: Copy> Point3D<I> {
    pub fn zip_map<F, J, R>(&self, other: &Point3D<J>, f: F) -> Point3D<R>
    where
        J: Copy,
        F: Fn(I, J) -> R,
    {
        Point3D {
            x: f(self.x, other.x),
            y: f(self.y, other.y),
            z: f(self.z, other.z),
        }
    }
}

impl<I: Copy> Point3D<I> {
    pub fn iter(&self) -> Point3DItemIterator<I> {
        Point3DItemIterator {
            pos: 0,
            point: *self,
        }
    }
}

pub struct Point3DItemIterator<I> {
    pos: usize,
    point: Point3D<I>,
}

impl<I: Copy> Iterator for Point3DItemIterator<I> {
    type Item = I;

    fn next(&mut self) -> Option<Self::Item> {
        let next = match self.pos {
            0 => Some(self.point.x),
            1 => Some(self.point.y),
            2 => Some(self.point.z),
            _ => None,
        };
        if next.is_some() {
            self.pos += 1;
        }
        next
    }
}

impl<I: Copy> IntoIterator for Point3D<I> {
    type Item = I;

    type IntoIter = Point3DItemIterator<I>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Cuboid<I = i32> {
    pub bottom_left: Point3D<I>,
    pub top_right: Point3D<I>,
}

impl<I: Copy + Ord> From<([I; 3], [I; 3])> for Cuboid<I> {
    fn from(value: ([I; 3], [I; 3])) -> Self {
        Cuboid::new(value.0.into(), value.1.into())
    }
}

impl<I: Copy + Ord> Cuboid<I> {
    pub fn new(a: Point3D<I>, b: Point3D<I>) -> Cuboid<I> {
        let top_left = a.zip_map(&b, I::min);
        let bottom_right = a.zip_map(&b, I::max);
        Cuboid {
            bottom_left: top_left,
            top_right: bottom_right,
        }
    }
}

impl<I: AddAssign + Copy> Cuboid<I> {
    pub fn raise(&mut self, by: I) {
        self.bottom_left.z += by;
        self.top_right.z += by;
    }
}

impl<I: Clone + Add<Output = I>> Cuboid<I> {
    pub fn raised(&self, by: I) -> Self {
        Cuboid {
            bottom_left: Point3D {
                z: self.bottom_left.z.clone() + by.clone(),
                ..self.bottom_left.clone()
            },
            top_right: Point3D {
                z: self.top_right.z.clone() + by.clone(),
                ..self.top_right.clone()
            },
        }
    }
}

impl<I: Ord + Copy> Cuboid<I> {
    pub fn has_collision(&self, other: &Self) -> bool {
        let a = self.bottom_left.zip_map(&self.top_right, |a, b| (a, b));
        let b = other.bottom_left.zip_map(&other.top_right, |a, b| (a, b));
        a.iter().zip(b.iter()).all(|(a, b)| intersects(a, b))
    }

    pub fn has_collision_xy(&self, other: &Self) -> bool {
        let a = self.bottom_left.zip_map(&self.top_right, |a, b| (a, b));
        let b = other.bottom_left.zip_map(&other.top_right, |a, b| (a, b));
        a.iter().zip(b.iter()).take(2).all(|(a, b)| intersects(a, b))
    }
}

fn intersects<I>(a: (I, I), b: (I, I)) -> bool
where
    I: Ord,
{
    let (a_min, a_max) = if a.0 < a.1 {
        (&a.0, &a.1)
    } else {
        (&a.1, &a.0)
    };
    let (b_min, b_max) = if b.0 < b.1 {
        (&b.0, &b.1)
    } else {
        (&b.1, &b.0)
    };
    a_min.max(b_min) <= a_max.min(b_max)
}

#[cfg(test)]
mod test {
    use rstest::rstest;

    use super::*;

    #[test]
    fn cuboid_new() {
        let actual = Cuboid::new([1, 0, 1].into(), [0, 1, 0].into());
        let expected = Cuboid {
            bottom_left: [0, 0, 0].into(),
            top_right: [1, 1, 1].into(),
        };
        assert_eq!(actual, expected);
    }

    #[rstest]
    #[case((0, 2), (1, 2), true)]
    #[case((2, 0), (2, 1), true)]
    #[case((0, 2), (2, 3), true)]
    #[case((10, 12), (2, 3), false)]
    #[case((10, 12), (9, 10), true)]
    fn intersects_examples(
        #[case] a: (i32, i32),
        #[case] b: (i32, i32),
        #[case] has_collision: bool,
    ) {
        assert_eq!(has_collision, intersects(a, b));
    }

    #[rstest]
    #[case(
        Cuboid::new([0,0,1].into(), [0,0,10].into()), 
        Cuboid::new([0,0,100].into(), [0,1,10].into()), 
    true)]
    fn has_collision_examples(#[case] a: Cuboid, #[case] b: Cuboid, #[case] has_collision: bool) {
        assert_eq!(a.has_collision(&b), has_collision);
    }
}
