use std::{cmp::min, collections::HashMap, rc::Rc, sync::RwLock};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{space1, u32},
    combinator::value,
    multi::{many1, separated_list1},
    sequence::separated_pair,
    IResult,
};

pub fn solution1() -> usize {
    std::fs::read_to_string("inputs/day12_1.txt")
        .expect("file to exist")
        .lines()
        .map(|line| BrokenRecord::parse(line).expect("successfull parse").1)
        .map(|broken_record| broken_record.num_possibilities())
        .sum()
}
pub fn solution2() -> usize {
    std::fs::read_to_string("inputs/day12_1.txt")
        .expect("file to exist")
        .lines()
        .map(|line| BrokenRecord::parse(line).expect("successfull parse").1)
        .map(BrokenRecord::unfold)
        .map(|broken_record| broken_record.num_possibilities())
        .sum()
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
enum Spring {
    Working,
    Broken,
    Unknown,
}

#[derive(Debug)]
struct BrokenRecord {
    springs: Vec<Spring>,
    broken_runs: Vec<u32>,
}

impl BrokenRecord {
    fn parse(line: &str) -> IResult<&str, BrokenRecord> {
        let (remaining, (springs, broken_runs)) = separated_pair(
            many1(alt((
                value(Spring::Working, tag(".")),
                value(Spring::Broken, tag("#")),
                value(Spring::Unknown, tag("?")),
            ))),
            space1,
            separated_list1(tag(","), u32),
        )(line)?;
        Ok((
            remaining,
            BrokenRecord {
                springs,
                broken_runs,
            },
        ))
    }

    fn num_possibilities(&self) -> usize {
        let state = NumPossibilitesState::new(self);
        self.num_possibilities_go(state).unwrap_or(0)
    }

    fn num_possibilities_go(&self, state: NumPossibilitesState) -> Option<usize> {
        let (popped_stack, run, spring_after_run) = match state.pop() {
            Some(popped) => popped,
            None => {
                match state.valid_configuration() {
                    Some(true) => return Some(1),
                    _ => return None,
                };
            }
        };
        let cache_key = (state.pos, state.pos_broken_run);
        let result_cache = Rc::clone(&state.cache);
        {
            if let Some(num_possibilities) = result_cache
                .read()
                .expect("No Poisoning")
                .get(&cache_key)
                .copied()
            {
                return Some(num_possibilities);
            }
        }
        let mut num_possibilities = 0;
        if self.springs[state.pos..state.pos + run]
            .iter()
            .all(|s| *s != Spring::Working)
            && spring_after_run != Some(Spring::Broken)
        {
            num_possibilities += self.num_possibilities_go(popped_stack).unwrap_or(0);
        } else if self.springs[state.pos] == Spring::Broken {
            return None;
        }
        if self.springs[state.pos] != Spring::Broken {
            num_possibilities += self
                .num_possibilities_go(NumPossibilitesState {
                    pos: state.pos + 1,
                    ..state
                })
                .unwrap_or(0);
        }
        let mut result_cache = result_cache.write().expect("No poisoning");
        result_cache.entry(cache_key).or_insert(num_possibilities);
        Some(num_possibilities)
    }

    fn unfold(self) -> Self {
        let mut springs = self.springs.clone();
        (0..4).for_each(|_| {
            springs.push(Spring::Unknown);
            springs.append(&mut self.springs.clone());
        });
        let mut broken_runs = self.broken_runs.clone();
        (0..4).for_each(|_| {
            broken_runs.append(&mut self.broken_runs.clone());
        });
        BrokenRecord {
            springs,
            broken_runs,
        }
    }
}

#[derive(Debug)]
struct NumPossibilitesState<'a> {
    broken_record: &'a BrokenRecord,
    pos: usize,
    pos_broken_run: usize,
    min_needed_positions: Rc<Vec<usize>>,
    cache: Rc<RwLock<HashMap<(usize, usize), usize>>>,
}

impl NumPossibilitesState<'_> {
    fn new(r: &BrokenRecord) -> NumPossibilitesState<'_> {
        let mut min_needed_positions = vec![];
        min_needed_positions.push(
            r.broken_runs.iter().map(|x| *x as usize).sum::<usize>() + r.broken_runs.len() - 1,
        );
        for i in 1..r.broken_runs.len() {
            min_needed_positions.push(min_needed_positions[i - 1] - r.broken_runs[i] as usize);
        }
        NumPossibilitesState {
            broken_record: r,
            pos: 0,
            pos_broken_run: 0,
            min_needed_positions: Rc::new(min_needed_positions),
            cache: Rc::new(RwLock::new(HashMap::new())),
        }
    }

    fn pop(&self) -> Option<(NumPossibilitesState, usize, Option<Spring>)> {
        let run_length = *self.broken_record.broken_runs.get(self.pos_broken_run)? as usize;
        if self.pos + run_length > self.broken_record.springs.len() {
            None
        } else {
            Some((
                NumPossibilitesState {
                    pos_broken_run: self.pos_broken_run + 1,
                    pos: self.pos + run_length + 1,
                    min_needed_positions: Rc::clone(&self.min_needed_positions),
                    cache: Rc::clone(&self.cache),
                    broken_record: self.broken_record,
                },
                run_length,
                self.broken_record
                    .springs
                    .get(self.pos + run_length)
                    .copied(),
            ))
        }
    }

    fn valid_configuration(&self) -> Option<bool> {
        if self.pos_broken_run >= self.broken_record.broken_runs.len() {
            Some(
                self.broken_record.springs[min(self.pos, self.broken_record.springs.len())..]
                    .iter()
                    .all(|s| *s != Spring::Broken),
            )
        } else if self.broken_record.springs.len().saturating_sub(self.pos)
            < self.min_needed_positions[self.pos_broken_run]
        {
            Some(false)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use rstest::rstest;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(7599, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(15454556629917, solution2());
    }

    #[rstest]
    #[case::ex("???.### 1,1,3", 1)]
    #[case::ex(".??..??...?##. 1,1,3", 4)]
    #[case::ex("?#?#?#?#?#?#?#? 1,3,1,6", 1)]
    #[case::ex("????.#...#... 4,1,1", 1)]
    #[case::ex("????.######..#####. 1,6,5", 4)]
    #[case::ex("?###???????? 3,2,1", 10)]
    fn examples_part1(#[case] line: &str, #[case] expected: usize) {
        let broken_record = BrokenRecord::parse(line).unwrap().1;
        assert_eq!(expected, broken_record.num_possibilities());
    }

    #[rstest]
    #[case::ex("???.### 1,1,3", 1)]
    #[case::ex(".??..??...?##. 1,1,3", 16384)]
    #[case::ex("?#?#?#?#?#?#?#? 1,3,1,6", 1)]
    #[case::ex("????.#...#... 4,1,1", 16)]
    #[case::ex("????.######..#####. 1,6,5", 2500)]
    #[case::ex("?###???????? 3,2,1", 506250)]
    fn examples_part2(#[case] line: &str, #[case] expected: usize) {
        let broken_record = BrokenRecord::parse(line).unwrap().1;
        assert_eq!(expected, broken_record.unfold().num_possibilities());
    }
}
