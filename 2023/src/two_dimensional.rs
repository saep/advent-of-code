use std::{
    fmt::Debug,
    ops::{AddAssign, Index, IndexMut, SubAssign},
};

use enumset::EnumSetType;

#[derive(Debug, PartialOrd, Ord, Hash, EnumSetType)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    pub fn all() -> [Direction; 4] {
        [
            Direction::North,
            Direction::East,
            Direction::South,
            Direction::West,
        ]
    }

    pub fn opposite(&self) -> Direction {
        match self {
            Direction::North => Direction::South,
            Direction::East => Self::West,
            Direction::South => Self::North,
            Direction::West => Self::East,
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub struct Point2D<I = i64> {
    pub x: I,
    pub y: I,
}

impl<I: Debug> Debug for Point2D<I> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{x: {:?}, y: {:?}}}", self.x, self.y)
    }
}

impl<I: Copy> Point2D<I> {
    pub fn map<J>(&self, f: impl Fn(I) -> J) -> Point2D<J> {
        Point2D {
            x: f(self.x),
            y: f(self.y),
        }
    }
}

impl<I> Point2D<I>
where
    I: SubAssign + AddAssign + Copy + From<i8>,
{
    pub fn move_in_direction(mut self, dir: Direction, n: I) -> Self {
        match dir {
            Direction::North => self.y -= n,
            Direction::East => self.x += n,
            Direction::South => self.y += n,
            Direction::West => self.x -= n,
        }
        self
    }

    pub fn moved(&self, dir: Direction) -> Self {
        (*self).move_in_direction(dir, 1.into())
    }
    pub fn moved_by(&self, dir: Direction, n: I) -> Self {
        (*self).move_in_direction(dir, n)
    }
}

impl<I> From<(I, I)> for Point2D<I> {
    fn from(value: (I, I)) -> Self {
        Point2D {
            x: value.0,
            y: value.1,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Array2D<V> {
    width: usize,
    height: usize,
    data: Vec<V>,
}

#[derive(Debug)]
pub enum TwoDError {
    EmptyArray,
    EmptyColumn,
    DifferentColumnLengths,
}

impl<V> Array2D<V> {
    /// The exclusive upper bound for the `y` field of [Point2D].
    pub fn height(&self) -> usize {
        self.height
    }

    /// The exclusive upper bound for the `x` field of [Point2D].
    pub fn width(&self) -> usize {
        self.width
    }

    fn pos(&self, point: Point2D) -> Option<usize> {
        if point.x < 0
            || point.y < 0
            || point.x as usize >= self.width
            || point.y as usize >= self.height
        {
            return None;
        }
        Some(point.y as usize * self.width + point.x as usize)
    }

    pub fn orthogonal_neighbours(
        &self,
        pos: Point2D,
    ) -> impl Iterator<Item = (Direction, Point2D, &V)> {
        Direction::all().into_iter().filter_map(move |dir| {
            let dpos = pos.moved(dir);
            let v = self.data.get(self.pos(dpos)?)?;
            Some((dir, dpos, v))
        })
    }
}

impl<V: Copy> Array2D<V> {
    pub fn new_filled_with(bottom_right: Point2D, v: V) -> Option<Array2D<V>> {
        if bottom_right.x <= 0 || bottom_right.y <= 0 {
            return None;
        }
        let height = 1 + bottom_right.y as usize;
        let width = 1 + bottom_right.x as usize;
        Some(Array2D {
            width,
            height,
            data: vec![v; height * width],
        })
    }

    pub fn new<I, J>(arr: I) -> Result<Array2D<V>, TwoDError>
    where
        I: IntoIterator<Item = J>,
        J: IntoIterator<Item = V>,
    {
        let mut width = Err(TwoDError::EmptyArray);
        let mut height = 0;
        let mut data = vec![];
        for row in arr {
            let mut row_width = 0;
            for v in row {
                row_width += 1;
                data.push(v);
            }
            if row_width == 0 {
                return Err(TwoDError::EmptyColumn);
            } else if let Ok(w) = width {
                if row_width != w {
                    return Err(TwoDError::DifferentColumnLengths);
                }
            } else {
                width = Ok(row_width);
            }
            height += 1;
        }
        Ok(Array2D {
            width: width?,
            height,
            data,
        })
    }

    pub fn get(&self, point: Point2D) -> Option<V> {
        let pos = self.pos(point)?;
        Some(self.data[pos])
    }

    pub fn set(&mut self, point: Point2D, v: V) {
        if let Some(pos) = self.pos(point) {
            self.data[pos] = v;
        }
    }

    pub fn all_positions(&self) -> impl Iterator<Item = Point2D> {
        let width = self.width;
        let height = self.height;
        (0..width).flat_map(move |x| {
            (0..height).map(move |y| Point2D {
                x: x as i64,
                y: y as i64,
            })
        })
    }
}

impl<V> Index<Point2D> for Array2D<V> {
    type Output = V;

    fn index(&self, index: Point2D) -> &Self::Output {
        let pos = self.pos(index).expect("index inside array");
        &self.data[pos]
    }
}

impl<V> IndexMut<Point2D> for Array2D<V> {
    fn index_mut(&mut self, index: Point2D) -> &mut Self::Output {
        let pos = self.pos(index).expect("index inside array");
        &mut self.data[pos]
    }
}

#[cfg(test)]
mod test {
    use rstest::rstest;

    use super::*;

    #[rstest]
    #[case::origin(Some(1), Point2D { x: 0, y: 0 })]
    #[case::last_column0(Some(3), Point2D { x: 2, y: 0 })]
    #[case::last_column1(Some(6), Point2D { x: 2, y: 1 })]
    #[case::last(Some(9), Point2D { x: 2, y: 2 })]
    #[case::just_outside_column(None, Point2D { x: 3, y: 0 })]
    #[case::just_outside_row(None, Point2D { x: 0, y: 3 })]
    fn array2d_get(#[case] expected: Option<u8>, #[case] point: Point2D) {
        let arr = Array2D::new([[1, 2, 3], [4, 5, 6], [7, 8, 9]]).expect("array to be constructed");
        assert_eq!(expected, arr.get(point));
    }

    #[test]
    fn opposite_twice_is_start() {
        for dir in Direction::all() {
            assert_eq!(dir, dir.opposite().opposite());
        }
    }
}
