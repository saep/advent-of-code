use std::collections::{HashMap, HashSet};

use itertools::Itertools;
use nalgebra::DMatrix;
use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, newline, space0, space1},
    combinator::eof,
    multi::{many0, separated_list1},
    sequence::{separated_pair, terminated, tuple},
    IResult,
};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day25_1.txt").expect("input file must exist");
    let graph = Graph::parse(&input);
    graph.disconnect()
}
pub fn solution2() -> usize {
    2
}

struct Graph {
    // spectal bisection
    // degree for i=i
    // -1 if edge from i to j (undirected) exists
    sp_matrix: DMatrix<f32>,
}

impl Graph {
    fn parse(input: &str) -> Graph {
        let parsed_edges: IResult<&str, Vec<(&str, Vec<&str>)>> = terminated(
            separated_list1(
                newline,
                separated_pair(
                    alpha1,
                    tuple((tag(":"), space0)),
                    separated_list1(space1, alpha1),
                ),
            ),
            tuple((many0(newline), eof)),
        )(input);
        let mut nodes = HashMap::new();
        let mut edges = Vec::new();
        for (from, tos) in parsed_edges.expect("input must be valid").1 {
            let from = match nodes.get(from) {
                Some(id) => *id,
                None => {
                    let id = nodes.len();
                    nodes.insert(from.to_owned(), id);
                    assert_eq!(edges.len(), id);
                    edges.push(HashSet::new());
                    id
                }
            };
            for to in tos {
                let to = match nodes.get(to) {
                    Some(to) => *to,
                    None => {
                        let id = nodes.len();
                        assert_eq!(edges.len(), id);
                        nodes.insert(to.to_owned(), id);
                        edges.push(HashSet::new());
                        id
                    }
                };
                edges[to].insert(from);
                edges[from].insert(to);
            }
        }
        let mut sp_matrix = DMatrix::<f32>::zeros(nodes.len(), nodes.len());
        for (from, tos) in edges.iter().enumerate() {
            sp_matrix[(from, from)] = tos.len() as f32;
            for to in tos {
                sp_matrix[(from, *to)] = -1.;
            }
        }
        Graph { sp_matrix }
    }

    fn disconnect(&self) -> usize {
        // Spectral bisection
        // Code has bee taken from:
        // https://github.com/alexcani/adventofcode2023/blob/master/src/bin/25.rs

        let symmetric_eigen = self.sp_matrix.clone().symmetric_eigen();
        let second_lowest_eigenvalue_index = symmetric_eigen
            .eigenvalues
            .iter()
            .enumerate()
            .filter(|(_, v)| !v.is_nan())
            .sorted_by(|v1, v2| v1.1.partial_cmp(v2.1).expect("No NaN"))
            .take(2)
            .last()
            .expect("2 eigen values")
            .0;

        symmetric_eigen
            .eigenvectors
            .column(second_lowest_eigenvalue_index)
            .into_iter()
            .into_grouping_map_by(|p| **p >= 0.)
            .fold(0, |acc, _key, _val| acc + 1)
            .values()
            .product()
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(solution1(), 568214);
    }

    const EXAMPLE: &str = indoc! {"
        jqt: rhn xhk nvd
        rsh: frs pzl lsr
        xhk: hfx
        cmg: qnr nvd lhk bvb
        rhn: xhk bvb hfx
        bvb: xhk hfx
        pzl: lsr hfx nvd
        qnr: nvd
        ntq: jqt hfx bvb xhk
        nvd: lhk
        lsr: lhk
        rzs: qnr cmg lsr rsh
        frs: qnr lhk lsr
    "};

    #[test]
    fn example_part_1() {
        let graph = Graph::parse(EXAMPLE);
        assert_eq!(54, graph.disconnect());
    }
}
