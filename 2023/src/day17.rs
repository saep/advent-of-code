use std::collections::{BinaryHeap, HashSet};

use crate::two_dimensional::{Array2D, Direction, Point2D, TwoDError};
pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day17_1.txt").expect("file to exist");
    let graph = parse_graph(&input).expect("valid test inupt");
    shortest_path_length(&graph)
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day17_1.txt").expect("file to exist");
    let graph = parse_graph(&input).expect("valid test inupt");
    shortest_path_length_ultra_crucible(&graph)
}

fn parse_graph(input: &str) -> Result<Array2D<u32>, TwoDError> {
    Array2D::new(
        input
            .lines()
            .map(|s| s.chars().flat_map(|c| c.to_digit(10))),
    )
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct HeatLossForPosition {
    cost: usize,
    pos: Point2D,
    dir_count: (Direction, u8),
}

impl Ord for HeatLossForPosition {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.pos.cmp(&other.pos))
            .then_with(|| self.dir_count.cmp(&other.dir_count))
    }
}

impl PartialOrd for HeatLossForPosition {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

fn shortest_path_length(heat_loss_positions: &Array2D<u32>) -> usize {
    let target = Point2D {
        x: heat_loss_positions.width() as i64 - 1,
        y: heat_loss_positions.height() as i64 - 1,
    };
    let mut queue = BinaryHeap::new();
    queue.push(HeatLossForPosition {
        cost: 0,
        pos: Point2D { x: 0, y: 0 },
        dir_count: (Direction::East, 0),
    });
    let mut seen = HashSet::new();
    while let Some(HeatLossForPosition {
        cost,
        pos,
        dir_count: (dir, count),
    }) = queue.pop()
    {
        if pos == target {
            return cost;
        }
        if !seen.insert((pos, dir, count)) {
            continue;
        }
        for next_dir in Direction::all() {
            if next_dir == dir.opposite() {
                continue;
            }
            let next_count = 1 + if next_dir == dir { count } else { 0 };
            if next_count > 3 {
                continue;
            }
            let next_pos = pos.moved(next_dir);
            if let Some(heat_loss) = heat_loss_positions.get(next_pos) {
                let next = HeatLossForPosition {
                    cost: cost + heat_loss as usize,
                    pos: next_pos,
                    dir_count: (next_dir, next_count),
                };
                queue.push(next);
            }
        }
    }
    usize::MAX
}

fn shortest_path_length_ultra_crucible(heat_loss_positions: &Array2D<u32>) -> usize {
    let target = Point2D {
        x: heat_loss_positions.width() as i64 - 1,
        y: heat_loss_positions.height() as i64 - 1,
    };
    let mut queue = BinaryHeap::new();
    queue.push(HeatLossForPosition {
        cost: 0,
        pos: Point2D { x: 0, y: 0 },
        dir_count: (Direction::East, 0),
    });
    queue.push(HeatLossForPosition {
        cost: 0,
        pos: Point2D { x: 0, y: 0 },
        dir_count: (Direction::South, 0),
    });
    let mut seen = HashSet::new();
    while let Some(HeatLossForPosition {
        cost,
        pos,
        dir_count: (dir, count),
    }) = queue.pop()
    {
        if pos == target && count > 4 {
            return cost;
        }
        if !seen.insert((pos, dir, count)) {
            continue;
        }
        if count < 4 {
            let next_pos = pos.moved(dir);
            if let Some(heat_loss) = heat_loss_positions.get(next_pos) {
                queue.push(HeatLossForPosition {
                    cost: cost + heat_loss as usize,
                    pos: next_pos,
                    dir_count: (dir, count + 1),
                })
            }
            continue;
        }
        for next_dir in Direction::all() {
            if next_dir == dir.opposite() {
                continue;
            }
            let next_count = 1 + if next_dir == dir { count } else { 0 };
            if next_count > 10 {
                continue;
            }
            let next_pos = pos.moved(next_dir);
            if let Some(heat_loss) = heat_loss_positions.get(next_pos) {
                let next = HeatLossForPosition {
                    cost: cost + heat_loss as usize,
                    pos: next_pos,
                    dir_count: (next_dir, next_count),
                };
                queue.push(next);
            }
        }
    }
    usize::MAX
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(1110, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(1294, solution2());
    }

    #[test]
    fn example_part1() {
        let input = indoc! {"
            2413432311323
            3215453535623
            3255245654254
            3446585845452
            4546657867536
            1438598798454
            4457876987766
            3637877979653
            4654967986887
            4564679986453
            1224686865563
            2546548887735
            4322674655533
            "};
        let graph = parse_graph(input).expect("valid test inupt");
        assert_eq!(102, shortest_path_length(&graph));
    }

    #[test]
    fn example_part2() {
        let input = indoc! {"
            2413432311323
            3215453535623
            3255245654254
            3446585845452
            4546657867536
            1438598798454
            4457876987766
            3637877979653
            4654967986887
            4564679986453
            1224686865563
            2546548887735
            4322674655533
            "};
        let graph = parse_graph(input).expect("valid test inupt");
        assert_eq!(94, shortest_path_length_ultra_crucible(&graph));
    }
}
