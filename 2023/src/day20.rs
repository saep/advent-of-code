use std::{
    collections::{HashMap, HashSet, VecDeque},
    ops::{Index, IndexMut},
};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, newline, space0},
    combinator::map,
    multi::separated_list0,
    sequence::{delimited, terminated, tuple},
    IResult,
};
use num::Integer;

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day20_1.txt").expect("input file must exist");
    Machines::parse(&input)
        .expect("input file to parse")
        .1
        .push_button(1000)
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day20_1.txt").expect("input file must exist");
    let machines = Machines::parse(&input).expect("input file to parse").1;

    // I cannot be bothered to write a general solution. I've inspected the input and made some
    // assumptions and tried the code.
    //
    // I'm assuming that the machines loop, i.e. the state of the machines as a collective will
    // be the same as the starting state at some point.
    //
    // From inspecting the input:
    //
    // rx receives signal from zh, which is an inverter
    //
    // zh sends low if ks kb and sx all send high
    //
    // With these assumptions, the number of button presses until rx receives a low is the least
    // common mulitple of the button presses, so that the 4 inverters send a high pulse.
    ["ks", "kb", "sx", "jt"]
        .into_iter()
        .map(|inverter| machines.clone().min_button_presses(inverter, Pulse::High))
        .reduce(|a, b| a.lcm(&b))
        .expect("result")
}

#[derive(Debug)]
struct Machines {
    modules: Vec<(String, Module)>,
    lookup: HashMap<String, usize>,
}

impl Clone for Machines {
    fn clone(&self) -> Self {
        let modules = self
            .modules
            .iter()
            .map(|(k, v)| (k.clone(), v.clone()))
            .collect();
        Machines {
            modules,
            lookup: self.lookup.clone(),
        }
    }
}

impl Machines {
    fn new(mut modules: Vec<(String, Module)>) -> Self {
        let mut lookup: HashMap<String, usize> = HashMap::from_iter(
            modules
                .iter()
                .map(|t| t.0.clone())
                .enumerate()
                .map(|t| (t.1, t.0)),
        );
        let edges: Vec<(String, Vec<String>)> = modules
            .iter()
            .map(|(id, module)| {
                let outputs = match module {
                    Module::Broadcaster { outputs } => outputs,
                    Module::FlipFlop { outputs, on: _ } => outputs,
                    Module::Conjunction {
                        outputs,
                        input_states: _,
                    } => outputs,
                    Module::Output => &vec![],
                };
                (id.clone(), outputs.clone())
            })
            .collect();
        let mut output_only: HashSet<String> = edges
            .iter()
            .flat_map(|(_, outputs)| outputs)
            .map(String::from)
            .collect();
        for edge in &edges {
            output_only.remove(&edge.0);
        }
        for output_module in output_only {
            lookup.insert(output_module.clone(), modules.len());
            modules.push((output_module.clone(), Module::Output));
        }
        let mut machines = Machines { modules, lookup };
        for (id, outputs) in edges {
            for output in outputs {
                if let Module::Conjunction {
                    outputs: _,
                    input_states,
                } = &mut machines[&output]
                {
                    input_states.insert(id.clone(), Pulse::Low);
                }
            }
        }
        machines
    }

    fn parse(input: &str) -> IResult<&str, Machines> {
        map(separated_list0(newline, Module::parse), Machines::new)(input)
    }

    fn push_button(&mut self, times: usize) -> usize {
        let mut num_low_pulses = 0;
        let mut num_high_pulses = 0;
        for _ in 0..times {
            let mut queue: VecDeque<(String, Vec<String>, Pulse)> = VecDeque::new();
            let recipients = vec![String::from("broadcaster")];
            queue.push_back((String::from("button"), recipients.clone(), Pulse::Low));
            while let Some((sender, recipients, pulse)) = queue.pop_front() {
                if pulse == Pulse::High {
                    num_high_pulses += recipients.len();
                } else {
                    num_low_pulses += recipients.len();
                }
                for recipient in recipients {
                    let (pulse, outputs) = self.send(&sender, &recipient, pulse);
                    if !outputs.is_empty() {
                        queue.push_back((recipient.clone(), outputs.clone(), pulse));
                    }
                }
            }
        }
        num_low_pulses * num_high_pulses
    }

    fn min_button_presses(&mut self, expected_sender: &str, expected_pulse: Pulse) -> usize {
        let mut button_presses = 0;
        loop {
            let mut queue: VecDeque<(String, Vec<String>, Pulse)> = VecDeque::new();
            let recipients = vec![String::from("broadcaster")];
            queue.push_back((String::from("button"), recipients.clone(), Pulse::Low));
            button_presses += 1;
            while let Some((sender, recipients, pulse)) = queue.pop_front() {
                if sender == expected_sender && pulse == expected_pulse {
                    return button_presses;
                }
                for recipient in recipients {
                    let (pulse, outputs) = self.send(&sender, &recipient, pulse);
                    if !outputs.is_empty() {
                        queue.push_back((recipient.clone(), outputs.clone(), pulse));
                    }
                }
            }
        }
    }

    fn send(&mut self, sender: &str, recipient: &str, pulse: Pulse) -> (Pulse, Vec<String>) {
        match &mut self[recipient] {
            Module::Broadcaster { outputs } => (pulse, outputs.clone()),
            Module::FlipFlop {
                outputs,
                ref mut on,
            } => {
                if pulse == Pulse::High {
                    return (pulse, vec![]);
                }
                *on = !*on;
                (if *on { Pulse::High } else { Pulse::Low }, outputs.clone())
            }
            Module::Conjunction {
                outputs,
                ref mut input_states,
            } => {
                input_states.insert(String::from(sender), pulse);
                if input_states.values().all(|p| *p == Pulse::High) {
                    (Pulse::Low, outputs.clone())
                } else {
                    (Pulse::High, outputs.clone())
                }
            }
            Module::Output => (pulse, vec![]),
        }
    }
}

impl Index<&str> for Machines {
    type Output = Module;

    fn index(&self, index: &str) -> &Self::Output {
        &self.modules[self.lookup[index]].1
    }
}

impl IndexMut<&str> for Machines {
    fn index_mut(&mut self, index: &str) -> &mut Self::Output {
        &mut self.modules[self.lookup[index]].1
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum Module {
    Broadcaster {
        outputs: Vec<String>,
    },
    FlipFlop {
        outputs: Vec<String>,
        on: bool,
    },
    Conjunction {
        outputs: Vec<String>,
        input_states: HashMap<String, Pulse>,
    },
    Output,
}

impl Module {
    fn parse(input: &str) -> IResult<&str, (String, Module)> {
        let parse_outputs =
            |input| separated_list0(delimited(space0, tag(","), space0), alpha1)(input);
        let parse_arrow = |input| delimited(space0, tag("->"), space0)(input);
        let parse_broadcaster = tuple((
            map(terminated(tag("broadcaster"), parse_arrow), String::from),
            map(parse_outputs, |outputs| Module::Broadcaster {
                outputs: outputs.into_iter().map(String::from).collect(),
            }),
        ));
        let parse_flip_flop = tuple((
            map(delimited(tag("%"), alpha1, parse_arrow), String::from),
            map(parse_outputs, |outputs| Module::FlipFlop {
                outputs: outputs.into_iter().map(String::from).collect(),
                on: false,
            }),
        ));
        let parse_conjunction = tuple((
            map(delimited(tag("&"), alpha1, parse_arrow), String::from),
            map(parse_outputs, |outputs| Module::Conjunction {
                outputs: outputs.into_iter().map(String::from).collect(),
                input_states: HashMap::new(),
            }),
        ));
        alt((parse_broadcaster, parse_flip_flop, parse_conjunction))(input)
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
enum Pulse {
    High,
    Low,
}

#[cfg(test)]
mod test {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(777666211, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(243081086866483, solution2());
    }

    fn str_vec(strs: &[&str]) -> Vec<String> {
        strs.iter().map(|s| String::from(*s)).collect()
    }

    #[rstest]
    #[case::broadcaster(
        "broadcaster -> a, b, c",
        (String::from("broadcaster"), Module::Broadcaster { outputs: str_vec(&["a", "b", "c"])})
    )]
    #[case::flip_flop(
        "%a -> b",
        (String::from("a"), Module::FlipFlop { on: false, outputs: str_vec(&["b"]) })
    )]
    #[case::conjunction(
        "&inv -> a",
        (String::from("inv"), Module::Conjunction { input_states: HashMap::new(), outputs: str_vec(&["a"]) })
    )]
    fn parse_module(#[case] input: &str, #[case] expected: (String, Module)) {
        let actual = Module::parse(input);
        assert_eq!(actual, Ok(("", expected)));
    }

    #[test]
    fn machines_example1() {
        let input = indoc! {"
            broadcaster -> a, b, c
            %a -> b
            %b -> c
            %c -> inv
            &inv -> a
        "};
        let actual = Machines::parse(input).expect("parsing to succeed");
        assert_eq!("\n", actual.0);
        let mut machines = actual.1;
        assert_eq!(32000000, machines.push_button(1000));
    }
}
