use std::cmp::min;

use bitvec::vec::BitVec;

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day13_1.txt").expect("file to exist");
    Pattern::parse_multiple(&input)
        .iter()
        .map(Pattern::reflection_indices)
        .map(|(h, v)| 100 * h.unwrap_or(0) + v.unwrap_or(0))
        .sum()
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day13_1.txt").expect("file to exist");
    Pattern::parse_multiple(&input)
        .iter()
        .map(Pattern::reflection_indices_smudged)
        .map(|(h, v)| 100 * h.unwrap_or(0) + v.unwrap_or(0))
        .sum()
}

#[derive(Debug)]
struct Pattern {
    rocks: BitVec,
    column_len: usize,
    rows_len: usize,
}

impl Pattern {
    fn parse(input: &str) -> Option<Pattern> {
        let mut rocks = BitVec::new();
        let mut line_len = 0;
        let mut rows_len = 0;
        for line in input.lines() {
            line_len = line.len();
            for c in line.chars() {
                let rock = match c {
                    '#' => true,
                    '.' => false,
                    _ => return None,
                };
                rocks.push(rock);
            }
            rows_len += 1;
        }
        Some(Pattern {
            rocks,
            column_len: line_len,
            rows_len,
        })
    }

    fn parse_multiple(input: &str) -> Vec<Pattern> {
        input.split("\n\n").filter_map(Pattern::parse).collect()
    }

    fn reflection_indices(&self) -> (Option<usize>, Option<usize>) {
        (
            self.horizontal_reflection_index(0),
            self.vertical_reflection_index(0),
        )
    }

    fn reflection_indices_smudged(&self) -> (Option<usize>, Option<usize>) {
        (
            self.horizontal_reflection_index(1),
            self.vertical_reflection_index(1),
        )
    }

    fn horizontal_reflection_index(&self, num_smudges: usize) -> Option<usize> {
        (1..self.rows_len).find(|row| {
            let min_border_distance = min(*row, self.rows_len - row);
            let mut differences = 0;
            for offset in 0..min_border_distance {
                let above = (row - 1 - offset) * self.column_len;
                let below = (row + offset) * self.column_len;
                for col in 0..self.column_len {
                    if self.rocks[above + col] != self.rocks[below + col] {
                        differences += 1;
                        if differences > num_smudges {
                            return false;
                        }
                    }
                }
            }
            num_smudges == differences
        })
    }

    fn vertical_reflection_index(&self, num_smudges: usize) -> Option<usize> {
        (1..self.column_len).find(|column| {
            let min_border_distance = min(*column, self.column_len - column);
            let mut differences = 0;
            for offset in 0..min_border_distance {
                let left = column - 1 - offset;
                let right = column + offset;
                for row in 0..self.rows_len {
                    if self.rocks[row * self.column_len + left]
                        != self.rocks[row * self.column_len + right]
                    {
                        differences += 1;
                        if differences > num_smudges {
                            return false;
                        }
                    }
                }
            }
            differences == num_smudges
        })
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(37381, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(28210, solution2());
    }

    #[test]
    fn example_part1() {
        let input = indoc! {"
            #.##..##.
            ..#.##.#.
            ##......#
            ##......#
            ..#.##.#.
            ..##..##.
            #.#.##.#.

            #...##..#
            #....#..#
            ..##..###
            #####.##.
            #####.##.
            ..##..###
            #....#..#
           "};
        let patterns = Pattern::parse_multiple(input);
        assert_eq!(2, patterns.len());
        let reflections: Vec<_> = patterns.iter().map(Pattern::reflection_indices).collect();
        assert_eq!(vec![(None, Some(5)), (Some(4), None)], reflections);
        assert_eq!(
            405,
            patterns
                .iter()
                .map(Pattern::reflection_indices)
                .map(|(h, v)| 100 * h.unwrap_or(0) + v.unwrap_or(0))
                .sum::<usize>()
        );
    }

    #[test]
    fn example_part2() {
        let input = indoc! {"
            #.##..##.
            ..#.##.#.
            ##......#
            ##......#
            ..#.##.#.
            ..##..##.
            #.#.##.#.

            #...##..#
            #....#..#
            ..##..###
            #####.##.
            #####.##.
            ..##..###
            #....#..#
           "};
        let patterns = Pattern::parse_multiple(input);
        assert_eq!(2, patterns.len());
        let reflections: Vec<_> = patterns
            .iter()
            .map(Pattern::reflection_indices_smudged)
            .collect();
        assert_eq!(vec![(Some(3), None), (Some(1), None)], reflections);
        assert_eq!(
            400,
            patterns
                .iter()
                .map(Pattern::reflection_indices_smudged)
                .map(|(h, v)| 100 * h.unwrap_or(0) + v.unwrap_or(0))
                .sum::<usize>()
        );
    }
}
