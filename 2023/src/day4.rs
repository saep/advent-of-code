use nom::{
    bytes::complete::tag,
    character::complete::{newline, space0, space1, u64, u8},
    combinator::{all_consuming, map},
    multi::{many0, separated_list1},
    sequence::{delimited, preceded, separated_pair, terminated, tuple},
    IResult,
};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("inputs/day4_1.txt").unwrap();
    sum_of_card_scores(&input)
}
pub fn solution2() -> usize {
    let input = std::fs::read_to_string("inputs/day4_1.txt").unwrap();
    count_of_copied_cards(&input)
}

#[derive(PartialEq, Debug)]
struct Card {
    winning_numbers: Vec<u8>,
    numbers: Vec<u8>,
}

impl Card {
    fn parse_many(input: &str) -> IResult<&str, Vec<Card>> {
        all_consuming(terminated(
            separated_list1(newline, Card::parse_line),
            many0(newline),
        ))(input)
    }

    fn parse_line(line: &str) -> IResult<&str, Card> {
        map(
            preceded(
                delimited(tuple((tag("Card"), space1)), u64, tag(":")),
                separated_pair(
                    delimited(space0, separated_list1(space1, u8), space0),
                    tag("|"),
                    delimited(space0, separated_list1(space1, u8), space0),
                ),
            ),
            |(winning_numbers, numbers)| Card {
                winning_numbers,
                numbers,
            },
        )(line)
    }

    fn count_of_winning_numbers(&self) -> usize {
        self.numbers
            .iter()
            .filter(|n| self.winning_numbers.contains(n))
            .count()
    }

    fn score(&self) -> usize {
        let overlap = self.count_of_winning_numbers() as u32;
        if overlap == 0 {
            0
        } else {
            let base: usize = 2;
            base.checked_pow(overlap - 1).expect("no overflow")
        }
    }
}

fn sum_of_card_scores(cards: &str) -> usize {
    Card::parse_many(cards)
        .unwrap()
        .1
        .iter()
        .map(Card::score)
        .sum()
}

fn count_of_copied_cards(cards: &str) -> usize {
    let cards = Card::parse_many(cards).unwrap().1;
    let mut num_cards: Vec<usize> = vec![1; cards.len()];
    for (i, card) in cards.iter().enumerate() {
        for j in (i + 1)..=(i + card.count_of_winning_numbers()) {
            num_cards[j] += num_cards[i];
        }
    }
    num_cards.iter().sum::<usize>()
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(21959, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(5132675, solution2());
    }

    #[test]
    fn parse_line() {
        let line = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53";
        let card = Card::parse_line(line);
        let expected = Card {
            winning_numbers: vec![41, 48, 83, 86, 17],
            numbers: vec![83, 86, 6, 31, 17, 9, 48, 53],
        };
        assert_eq!(Ok(("", expected)), card);
    }

    #[test]
    fn example_part1() {
        let input = indoc! {"
                Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
                Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
                Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
                Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
                Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
                Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
        "};
        let score = sum_of_card_scores(input);
        assert_eq!(13, score);
    }

    #[test]
    fn exmaple_part2() {
        let input = indoc! {"
                Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
                Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
                Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
                Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
                Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
                Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
        "};
        let count = count_of_copied_cards(input);
        assert_eq!(30, count);
    }
}
