use std::{
    cmp::{max, min},
    collections::HashMap,
    ops::Range,
};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, i64, newline, space0, space1},
    combinator::{eof, map},
    multi::{many1, separated_list0, separated_list1},
    sequence::tuple,
    IResult,
};

pub fn solution1() -> i64 {
    let input = std::fs::read_to_string("inputs/day5_1.txt").expect("input to exist");
    let (_, almanac) = Almanac::parse(&input).expect("parsing to succeed");
    almanac.lowest_location_number()
}

pub fn solution2() -> i64 {
    let input = std::fs::read_to_string("inputs/day5_1.txt").expect("input to exist");
    let (_, almanac) = Almanac::parse(&input).expect("parsing to succeed");
    almanac.lowest_location_number_starting_with_ranges()
}

#[derive(Debug, PartialEq)]
struct Almanac {
    seeds: Vec<i64>,
    maps: HashMap<String, Mapping>,
}

impl Almanac {
    fn parse(input: &str) -> IResult<&str, Almanac> {
        let (remaining, (_, _, seeds, _, mappings)) = tuple((
            tag("seeds:"),
            space0,
            separated_list1(space1, i64),
            tag("\n\n"),
            many1(Mapping::parse),
        ))(input)?;
        let maps = mappings
            .iter()
            .map(|mapping| (mapping.source_type.to_owned(), mapping.to_owned()))
            .collect();
        Ok((remaining, Almanac { seeds, maps }))
    }

    fn lowest_location_number(&self) -> i64 {
        self.seeds
            .iter()
            .map(|seed| self.location_number(*seed))
            .min()
            .expect("seeds to not be empty")
    }

    fn location_number(&self, seed: i64) -> i64 {
        let start = String::from("seed");
        let mut cursor = &start;
        let mut result = seed;
        while let Some(m) = self.maps.get(cursor) {
            result = m.map(result);
            cursor = &m.target_type;
        }
        result
    }

    fn lowest_location_number_starting_with_ranges(&self) -> i64 {
        (0..self.seeds.len())
            .step_by(2)
            .map(|i| Range {
                start: self.seeds[i],
                end: self.seeds[i] + self.seeds[i + 1],
            })
            .flat_map(|seed_range| self.location_number_of_range(seed_range))
            .map(|range| range.start)
            .min()
            .expect("seeds to not be empty")
    }

    fn location_number_of_range(&self, range: Range<i64>) -> Vec<Range<i64>> {
        let start = String::from("seed");
        let mut cursor = &start;
        let mut result = vec![range.clone()];
        while let Some(m) = self.maps.get(cursor) {
            result = result.iter().flat_map(|range| m.map_range(range)).collect();
            result.sort_by_key(|r| r.start);
            cursor = &m.target_type;
        }
        result.sort_by_key(|r| r.start);
        result
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Mapping {
    source_type: String,
    target_type: String,
    mapping_ranges: Vec<MappingRange>,
}

impl Mapping {
    fn parse(input: &str) -> IResult<&str, Mapping> {
        let (remaining, (source_type, _, target_type, _, mapping_ranges, _)) = tuple((
            alpha1,
            tag("-to-"),
            alpha1,
            tag(" map:\n"),
            separated_list0(newline, MappingRange::parse),
            alt((map(many1(newline), |_| ()), map(eof, |_| ()))),
        ))(input)?;
        Ok((
            remaining,
            Mapping {
                source_type: source_type.to_owned(),
                target_type: target_type.to_owned(),
                mapping_ranges,
            },
        ))
    }

    fn map(&self, n: i64) -> i64 {
        self.mapping_ranges
            .iter()
            .find_map(|r| r.map(n))
            .unwrap_or(n)
    }

    fn map_range(&self, range: &Range<i64>) -> Vec<Range<i64>> {
        let mut mapped_ranges = vec![];
        let mut unmapped: Vec<Range<i64>> = vec![range.clone()];
        for mapping_range in self.mapping_ranges.iter() {
            let mut tmp = vec![];
            for r in unmapped.iter() {
                let MappedRanges { mapped, unmapped } = mapping_range.map_range(r);
                unmapped.iter().for_each(|x| tmp.push(x.to_owned()));
                if let Some(mapped_range) = mapped {
                    mapped_ranges.push(mapped_range);
                }
            }
            unmapped = tmp;
        }
        for r in unmapped {
            mapped_ranges.push(r.clone());
        }
        mapped_ranges.sort_by_key(|r| r.start);
        mapped_ranges
    }
}

#[derive(Debug, PartialEq, Clone)]
struct MappingRange {
    from: Range<i64>,
    shift_by: i64,
}

#[derive(Debug, PartialEq, Clone)]
struct MappedRanges {
    mapped: Option<Range<i64>>,
    unmapped: Vec<Range<i64>>,
}

impl MappingRange {
    fn parse(input: &str) -> IResult<&str, MappingRange> {
        let (remaining, (_, to_start, _, start, _, length)) =
            tuple((space0, i64, space1, i64, space1, i64))(input)?;
        Ok((
            remaining,
            MappingRange {
                from: Range {
                    start,
                    end: start + length,
                },
                shift_by: to_start - start,
            },
        ))
    }

    fn map(&self, n: i64) -> Option<i64> {
        if self.from.contains(&n) {
            Some(n + self.shift_by)
        } else {
            None
        }
    }

    fn shifted(&self, range: &Range<i64>) -> Range<i64> {
        Range {
            start: range.start + self.shift_by,
            end: range.end + self.shift_by,
        }
    }

    fn map_range(&self, range: &Range<i64>) -> MappedRanges {
        if self.from.start <= range.start && self.from.end >= range.end {
            MappedRanges {
                mapped: Some(self.shifted(range)),
                unmapped: vec![],
            }
        } else if self.from.end < range.start || self.from.start >= range.end {
            MappedRanges {
                mapped: None,
                unmapped: vec![range.clone()],
            }
        } else {
            let mut unmapped = vec![];
            if range.start < self.from.start {
                unmapped.push(Range {
                    start: range.start,
                    end: self.from.start,
                });
            }
            let mapped = Some(self.shifted(&Range {
                start: max(range.start, self.from.start),
                end: min(range.end, self.from.end),
            }));
            if range.end > self.from.end {
                unmapped.push(Range {
                    start: self.from.end,
                    end: range.end,
                });
            }
            MappedRanges { unmapped, mapped }
        }
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(993500720, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(4917124, solution2());
    }

    #[test]
    fn example_part1() {
        let input = indoc! {"
            seeds: 79 14 55 13

            seed-to-soil map:
            50 98 2
            52 50 48

            soil-to-fertilizer map:
            0 15 37
            37 52 2
            39 0 15

            fertilizer-to-water map:
            49 53 8
            0 11 42
            42 0 7
            57 7 4

            water-to-light map:
            88 18 7
            18 25 70

            light-to-temperature map:
            45 77 23
            81 45 19
            68 64 13

            temperature-to-humidity map:
            0 69 1
            1 0 69

            humidity-to-location map:
            60 56 37
            56 93 4
           "};
        let (remaining, almanac) = Almanac::parse(input).expect("successful parse");
        assert_eq!(remaining, "");
        assert_eq!(35, almanac.lowest_location_number())
    }

    #[test]
    fn example_part2() {
        let input = indoc! {"
            seeds: 79 14 55 13

            seed-to-soil map:
            50 98 2
            52 50 48

            soil-to-fertilizer map:
            0 15 37
            37 52 2
            39 0 15

            fertilizer-to-water map:
            49 53 8
            0 11 42
            42 0 7
            57 7 4

            water-to-light map:
            88 18 7
            18 25 70

            light-to-temperature map:
            45 77 23
            81 45 19
            68 64 13

            temperature-to-humidity map:
            0 69 1
            1 0 69

            humidity-to-location map:
            60 56 37
            56 93 4
           "};
        let (remaining, almanac) = Almanac::parse(input).expect("successful parse");
        assert_eq!(remaining, "");
        assert_eq!(46, almanac.lowest_location_number_starting_with_ranges())
    }

    #[test]
    fn parse_mapping_range() {
        let input = "45 77 23";
        let (remaining, mapping_range) = MappingRange::parse(input).unwrap();
        assert_eq!(remaining, "");
        assert_eq!(
            mapping_range,
            MappingRange {
                from: Range {
                    start: 77,
                    end: 100
                },
                shift_by: -32,
            }
        );
    }

    #[test]
    fn parse_mapping() {
        let input = indoc! {"
            light-to-temperature map:
            45 77 23
            81 45 19
            68 64 13
        "};
        let (remaining, mapping) = Mapping::parse(input).unwrap();
        assert_eq!("", remaining);
        assert_eq!(
            mapping,
            Mapping {
                source_type: String::from("light"),
                target_type: String::from("temperature"),
                mapping_ranges: vec![
                    MappingRange {
                        from: Range {
                            start: 77,
                            end: 100
                        },
                        shift_by: -32,
                    },
                    MappingRange {
                        from: Range { start: 45, end: 64 },
                        shift_by: 36,
                    },
                    MappingRange {
                        from: Range { start: 64, end: 77 },
                        shift_by: 4,
                    },
                ],
            }
        )
    }

    #[test]
    fn map_range_completely_mapped() {
        let mr = MappingRange::parse(indoc! {"
            18 25 70
            "})
        .unwrap()
        .1;
        assert_eq!(
            MappedRanges {
                mapped: Some(74..88),
                unmapped: vec![],
            },
            mr.map_range(&(81..95))
        );
    }

    #[test]
    fn map_range_() {
        let input = indoc! {"
            fertilizer-to-water map:
            49 53 8
            0 11 42
            42 0 7
            57 7 4
        "};
        let (remaining, mapping) = Mapping::parse(input).unwrap();
        assert_eq!(remaining, "");
        assert_eq!(vec![(53..57), (61..70)], mapping.map_range(&(57..70)));
    }
}
