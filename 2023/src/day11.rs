use std::collections::{BTreeMap, BTreeSet};

use crate::two_dimensional::Point2D;

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day11_1.txt").expect("file to exist");
    Universe::parse(&input)
        .expand(1)
        .sum_of_distances_of_all_galaxy_pairs()
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day11_1.txt").expect("file to exist");
    Universe::parse(&input)
        .expand(999999)
        .sum_of_distances_of_all_galaxy_pairs()
}

impl Point2D {
    fn distance(&self, other: &Self) -> usize {
        ((self.x - other.x).abs() + (self.y - other.y).abs()) as usize
    }
}

#[derive(Debug)]
struct Universe {
    galaxies: BTreeMap<usize, Point2D>,
}

impl Universe {
    fn parse(input: &str) -> Universe {
        let galaxies = input
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .filter(|(_, c)| *c == '#')
                    .map(move |(x, _)| Point2D {
                        x: x as i64,
                        y: y as i64,
                    })
            })
            .enumerate()
            .collect();
        Universe { galaxies }
    }

    /// Expand the universe by adding `by` amount of rows or columns. So if you want your gaps to
    /// be twice as large, you pass `1` as the argument. If you want ten times the gap size, you
    /// pass `9`.
    fn expand(mut self, by: usize) -> Universe {
        let mut xs = BTreeSet::new();
        let mut ys = BTreeSet::new();
        self.galaxies.values().for_each(|galaxy| {
            xs.insert(galaxy.x);
            ys.insert(galaxy.y);
        });
        let xs_to_duplicate = (0..xs.last().copied().unwrap())
            .filter(|x| !xs.contains(x))
            .enumerate()
            .map(|(inc_by, x)| (x, by * (inc_by + 1)))
            .collect::<BTreeMap<i64, usize>>();
        let ys_to_duplicate = (0..ys.last().copied().unwrap())
            .filter(|y| !ys.contains(y))
            .enumerate()
            .map(|(inc_by, y)| (y, by * (inc_by + 1)))
            .collect::<BTreeMap<i64, usize>>();
        self.galaxies.iter_mut().for_each(|(_, galaxy)| {
            xs_to_duplicate
                .range(0..galaxy.x)
                .last()
                .iter()
                .for_each(|inc_by| {
                    galaxy.x += *inc_by.1 as i64;
                });
            ys_to_duplicate
                .range(0..galaxy.y)
                .last()
                .iter()
                .for_each(|inc_by| {
                    galaxy.y += *inc_by.1 as i64;
                });
        });
        self
    }

    fn sum_of_distances_of_all_galaxy_pairs(&self) -> usize {
        let max_index = match self.galaxies.last_key_value() {
            None => return 0,
            Some(e) => *e.0,
        };
        self.galaxies
            .iter()
            .map(|(i, pos1)| {
                self.galaxies
                    .range(i..=&max_index)
                    .skip(1) // skip (i,i)
                    .map(|(_, pos2)| pos1.distance(pos2))
                    .sum::<usize>()
            })
            .sum()
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(9329143, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(710674907809, solution2());
    }

    #[rstest]
    #[case::expand_by1(1, 374)]
    #[case::expand_by10(9, 1030)]
    #[case::expand_by100(99, 8410)]
    fn example1_part1(#[case] by: usize, #[case] expected: usize) {
        let input = indoc! {"
                ...#......
                .......#..
                #.........
                ..........
                ......#...
                .#........
                .........#
                ..........
                .......#..
                #...#.....
            "};
        let universe = Universe::parse(input).expand(by);
        assert_eq!(expected, universe.sum_of_distances_of_all_galaxy_pairs());
    }
}
