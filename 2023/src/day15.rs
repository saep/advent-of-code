pub fn solution1() -> usize {
    let input = std::fs::read_to_string("inputs/day15_1.txt").expect("file to exist");
    sum_of_hashes(&input)
}

pub fn solution2() -> usize {
    std::fs::read_to_string("inputs/day15_1.txt")
        .expect("file to exist")
        .trim_end()
        .split(',')
        .fold(Boxes::new(), |boxes: Boxes, step| {
            boxes.hashmap_algorithm_step(step)
        })
        .focusing_power()
}

fn sum_of_hashes(input: &str) -> usize {
    input
        .trim_end()
        .split(',')
        .map(|step| the_hash_algorithm(step) as usize)
        .sum()
}

fn the_hash_algorithm(step: &str) -> u8 {
    step.bytes()
        .fold(0, |acc, b| acc.overflowing_add(b).0.overflowing_mul(17).0)
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Default)]
struct LensBox {
    lenses: Vec<(String, u8)>,
}

#[derive(Debug)]
struct Boxes {
    boxes: [LensBox; 256],
}

impl Boxes {
    fn new() -> Boxes {
        Boxes {
            boxes: (0..256)
                .map(|_| LensBox::default())
                .collect::<Vec<_>>()
                .try_into()
                .expect("range to be correct"),
        }
    }

    fn hashmap_algorithm_step(mut self, step: &str) -> Self {
        if step.bytes().last() == Some(b'-') {
            let label = &step[..step.len() - 1];
            let box_position = the_hash_algorithm(label);
            let lenses = &mut self.boxes[box_position as usize].lenses;
            if let Some(pos) = lenses.iter().position(|(l, _)| l == label) {
                lenses.remove(pos);
            }
        } else if step.contains('=') {
            let (label, focal_strength) = step.split_once('=').unwrap();
            let focal_strength: u8 = focal_strength.parse().expect("only valid input");
            let box_position = the_hash_algorithm(label);
            let lenses = &mut self.boxes[box_position as usize].lenses;
            if let Some(pos) = lenses.iter().position(|(l, _)| l == label) {
                lenses[pos] = (String::from(label), focal_strength);
            } else {
                lenses.push((String::from(label), focal_strength))
            }
        }
        self
    }

    fn focusing_power(&self) -> usize {
        self.boxes
            .iter()
            .enumerate()
            .map(|(box_num, b)| {
                b.lenses
                    .iter()
                    .enumerate()
                    .map(|(i, (_, l))| (box_num + 1) * (i + 1) * *l as usize)
                    .sum::<usize>()
            })
            .sum()
    }
}

#[cfg(test)]
mod test {
    use rstest::rstest;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(504036, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(295719, solution2());
    }

    #[rstest]
    #[case::hash("HASH", 52)]
    #[case::rn("rn", 0)]
    #[case::rn_eq_1("rn=1", 30)]
    #[case::cm("cm", 0)]
    #[case::cm_dash("cm-", 253)]
    #[case::long("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7", 1320)]
    fn example_part1(#[case] input: &str, #[case] expected: usize) {
        assert_eq!(expected, sum_of_hashes(input));
    }

    #[test]
    fn example_part2() {
        let input = "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";
        let boxes = input.split(',').fold(Boxes::new(), |boxes, step| {
            boxes.hashmap_algorithm_step(step)
        });
        assert_eq!(145, boxes.focusing_power());
    }
}
