use std::ops::RangeInclusive;

use nalgebra::{RowVector2, SMatrix, Vector2};
use nom::{
    bytes::complete::tag,
    character::complete::{i64, newline, space0},
    combinator::map,
    multi::{separated_list0, separated_list1},
    sequence::{separated_pair, terminated, tuple},
    IResult,
};
use z3::{
    ast::{Ast, Int},
    Config, Context, Solver,
};

use crate::{three_dimensional::Point3D, two_dimensional::Point2D};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day24_1.txt").expect("input must exist");
    part_1(200000000000000.0..=400000000000000.0, &input)
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day24_1.txt").expect("input must exist");
    part_2(&input)
}

#[derive(Debug, PartialEq, Clone)]
struct HailStone<I = i64> {
    position: Point3D<I>,
    velocity: Point3D<I>,
}

#[derive(Debug)]
enum Cross {
    Future(Point2D<f64>),
    Past(Point2D<f64>),
    Never,
}

impl PartialEq for Cross {
    fn eq(&self, other: &Self) -> bool {
        fn round(p: &Point2D<f64>) -> Point2D<i64> {
            p.map(|x| (x * 1000.).round() as i64)
        }
        match self {
            Cross::Future(p) => {
                if let Cross::Future(other) = other {
                    round(p) == round(other)
                } else {
                    false
                }
            }
            Cross::Past(p) => {
                if let Cross::Past(other) = other {
                    round(p) == round(other)
                } else {
                    false
                }
            }
            Cross::Never => matches!(other, Cross::Never),
        }
    }
}

impl HailStone {
    fn parse(input: &str) -> IResult<&str, HailStone> {
        let v3 = |input| separated_list1(tuple((tag(","), space0)), i64)(input);
        map(
            separated_pair(v3, tuple((space0, tag("@"), space0)), v3),
            |(pos, vel)| HailStone {
                position: (pos[0], pos[1], pos[2]).into(),
                velocity: (vel[0], vel[1], vel[2]).into(),
            },
        )(input)
    }

    fn parse_many(input: &str) -> IResult<&str, Vec<HailStone>> {
        terminated(separated_list0(newline, HailStone::parse), newline)(input)
    }

    fn collision_xy(&self, other: &HailStone) -> Cross {
        let v1 = Point2D {
            x: self.velocity.x as f64,
            y: self.velocity.y as f64,
        };
        let v2 = Point2D {
            x: other.velocity.x as f64,
            y: other.velocity.y as f64,
        };
        let p1 = self.position;
        let p2 = other.position;
        // p1 + t1 * v1 = p2 + t2 * v2
        // <=> v1*t1 - v2*t2 = p2 - p1
        let ma: SMatrix<f64, 2, 2> =
            SMatrix::from_rows(&[RowVector2::new(v1.x, -v2.x), RowVector2::new(v1.y, -v2.y)]);
        let b = Vector2::new((p2.x - p1.x) as f64, (p2.y - p1.y) as f64);
        match ma.lu().solve(&b) {
            Some(t) => {
                let p = Point2D {
                    x: p1.x as f64 + t[0] * v1.x,
                    y: p1.y as f64 + t[0] * v1.y,
                };
                if t.x >= 0. && t.y >= 0. {
                    Cross::Future(p)
                } else {
                    Cross::Past(p)
                }
            }
            None => Cross::Never,
        }
    }
}

fn part_1(range: RangeInclusive<f64>, input: &str) -> usize {
    let hs = HailStone::parse_many(input).expect("input must be valid");
    assert_eq!(hs.0, "");
    let hs = hs.1;
    let mut result = 0;
    for i in 0..hs.len() {
        for j in (i + 1)..hs.len() {
            match hs[i].collision_xy(&hs[j]) {
                Cross::Past(_) | Cross::Never => {}
                Cross::Future(Point2D { x, y }) => {
                    if range.contains(&x) && range.contains(&y) {
                        result += 1;
                    }
                }
            }
        }
    }
    result
}

fn find_position_and_velocity_to_cross_everywhere(hs: Vec<HailStone>) -> Option<HailStone> {
    if hs.len() < 3 {
        // We need enough equations to determine the solution which is a hail stone with initial
        // position h and velocity v. We therefore have 6 unknowns: The starting position and
        // velocity each having x,y,z coordinates. A hail stone provides us with 3 equations, one
        // for each dimension. However, we alos need to introduce a single unknown time for those 3
        // equations because we have to solve the following: p1 + t*v1 = h + t*v
        // So for each hail stone, we can eliminate 2 unknowns and we therefore need 3 to solve the
        // equations.
        return None;
    }
    let cfg = Config::new();
    let ctx = Context::new(&cfg);
    let solver = Solver::new(&ctx);

    let hx = Int::new_const(&ctx, "hx");
    let hy = Int::new_const(&ctx, "hy");
    let hz = Int::new_const(&ctx, "hz");
    let vx = Int::new_const(&ctx, "vx");
    let vy = Int::new_const(&ctx, "vy");
    let vz = Int::new_const(&ctx, "vz");

    for h in hs.iter().take(3) {
        let pxn = Int::from_i64(&ctx, h.position.x);
        let pyn = Int::from_i64(&ctx, h.position.y);
        let pzn = Int::from_i64(&ctx, h.position.z);
        let vxn = Int::from_i64(&ctx, h.velocity.x);
        let vyn = Int::from_i64(&ctx, h.velocity.y);
        let vzn = Int::from_i64(&ctx, h.velocity.z);
        let tn = Int::fresh_const(&ctx, "t");

        solver.assert(&(&pxn + &vxn * &tn)._eq(&(&hx + &vx * &tn)));
        solver.assert(&(&pyn + &vyn * &tn)._eq(&(&hy + &vy * &tn)));
        solver.assert(&(&pzn + &vzn * &tn)._eq(&(&hz + &vz * &tn)));
    }
    solver.check();
    let model = solver.get_model()?;
    let hx = model.get_const_interp(&hx)?.as_i64()?;
    let hy = model.get_const_interp(&hy)?.as_i64()?;
    let hz = model.get_const_interp(&hz)?.as_i64()?;
    let vx = model.get_const_interp(&vx)?.as_i64()?;
    let vy = model.get_const_interp(&vy)?.as_i64()?;
    let vz = model.get_const_interp(&vz)?.as_i64()?;
    Some(HailStone {
        position: (hx, hy, hz).into(),
        velocity: (vx, vy, vz).into(),
    })
}

fn part_2(input: &str) -> usize {
    let hs = HailStone::parse_many(input).expect("input must be valid");
    assert_eq!(hs.0, "");
    let hs = hs.1;
    let hail_stone = find_position_and_velocity_to_cross_everywhere(hs).expect("solution");
    let p = hail_stone.position;
    (p.x + p.y + p.z) as usize
}

#[cfg(test)]
mod test {
    use indoc::indoc;
    use rstest::rstest;

    use crate::two_dimensional::Point2D;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(solution1(), 15889);
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(solution2(), 801386475216902);
    }

    #[rstest]
    #[case::paths_cross1(
        Cross::Future(Point2D {x: 14.333, y: 15.333}),
        indoc! {"
            19, 13, 30 @ -2, 1, -2
            18, 19, 22 @ -1, -1, -2
        "}
    )]
    #[case::paths_cross2(
        Cross::Future(Point2D {x: 11.667, y: 16.667}),
        indoc! {"
            19, 13, 30 @ -2, 1, -2
            20, 25, 34 @ -2, -2, -4
        "}
    )]
    #[case::paths_cross3(
        Cross::Future(Point2D {x: 6.2, y: 19.4}),
        indoc! {"
            19, 13, 30 @ -2, 1, -2
            12, 31, 28 @ -1, -2, -1
        "}
    )]
    #[case::path_cross4(
        Cross::Past(Point2D {x: 21.444, y: 11.778}),
        indoc! {"
            19, 13, 30 @ -2, 1, -2
            20, 19, 15 @ 1, -5, -3
        "}
    )]
    #[case::path_cross5(
        Cross::Future(Point2D {x: -6., y: -5.}),
        indoc! {"
            18, 19, 22 @ -1, -1, -2
            12, 31, 28 @ -1, -2, -1
        "}
    )]
    #[case::path_cross6(
        Cross::Past(Point2D {x: 19.667, y: 20.6667}),
        indoc! {"
            18, 19, 22 @ -1, -1, -2
            20, 19, 15 @ 1, -5, -3
        "}
    )]
    #[case::path_cross7(
        Cross::Future(Point2D {x: -2., y: 3.}),
        indoc! {"
            20, 25, 34 @ -2, -2, -4
            12, 31, 28 @ -1, -2, -1
        "}
    )]
    #[case::path_cross8(
        Cross::Past(Point2D {x: 19., y: 24.}),
        indoc! {"
            20, 25, 34 @ -2, -2, -4
            20, 19, 15 @ 1, -5, -3
        "}
    )]
    #[case::path_cross9(
        Cross::Past(Point2D {x: 16., y: 39.}),
        indoc! {"
            12, 31, 28 @ -1, -2, -1
            20, 19, 15 @ 1, -5, -3
        "}
    )]
    #[case::parallel(
        Cross::Never,
        indoc! {"
            18, 19, 22 @ -1, -1, -2
            20, 25, 34 @ -2, -2, -4
        "}
    )]
    fn examples_part1(#[case] expected: Cross, #[case] input: &str) {
        let hail_stones = HailStone::parse_many(input).expect("parsing must succeed");
        assert_eq!(hail_stones.0, "", "input is fully parsed");
        let hail_stones = hail_stones.1;
        assert_eq!(hail_stones.len(), 2);

        let actual = hail_stones[0].collision_xy(&hail_stones[1]);

        assert_eq!(actual, expected);
    }

    const EXAMPLE: &str = indoc! {"
            19, 13, 30 @ -2,  1, -2
            18, 19, 22 @ -1, -1, -2
            20, 25, 34 @ -2, -2, -4
            12, 31, 28 @ -1, -2, -1
            20, 19, 15 @  1, -5, -3
        "};

    #[test]
    fn solution_example_part1() {
        let actual = part_1(7.0..=27.0, EXAMPLE);
        assert_eq!(actual, 2);
    }

    #[test]
    fn solution_example_part2() {
        let actual = part_2(EXAMPLE);
        assert_eq!(actual, 47);
    }
}
