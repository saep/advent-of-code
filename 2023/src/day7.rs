use std::{cmp::Reverse, collections::BTreeMap};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{i64, newline, space1},
    combinator::value,
    multi::{count, separated_list1},
    sequence::tuple,
    IResult,
};

pub fn solution1() -> i64 {
    let input = std::fs::read_to_string("inputs/day7_1.txt").expect("file to exist");
    Game::parse(&input, RuleSet::Traditional)
        .expect("input to parse")
        .1
        .total_winnings()
}

pub fn solution2() -> i64 {
    let input = std::fs::read_to_string("inputs/day7_1.txt").expect("file to exist");
    Game::parse(&input, RuleSet::Joker)
        .expect("input to parse")
        .1
        .total_winnings()
}

#[derive(Debug, PartialEq, PartialOrd, Clone, Copy, Eq, Ord)]
enum Card {
    Joker,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    T,
    J,
    Q,
    K,
    A,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum RuleSet {
    Traditional,
    Joker,
}

impl Card {
    fn parse(input: &str, ruleset: RuleSet) -> IResult<&str, Card> {
        let j = if ruleset == RuleSet::Joker {
            Card::Joker
        } else {
            Card::J
        };
        let (remaining, card) = alt((
            value(Card::Two, tag("2")),
            value(Card::Three, tag("3")),
            value(Card::Four, tag("4")),
            value(Card::Five, tag("5")),
            value(Card::Six, tag("6")),
            value(Card::Seven, tag("7")),
            value(Card::Eight, tag("8")),
            value(Card::Nine, tag("9")),
            value(Card::T, tag("T")),
            value(j, tag("J")),
            value(Card::Q, tag("Q")),
            value(Card::K, tag("K")),
            value(Card::A, tag("A")),
        ))(input)?;
        Ok((remaining, card))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Hand {
    cards: [Card; 5],
    ruleset: RuleSet,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
enum HandResult {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

impl Hand {
    fn parse(input: &str, ruleset: RuleSet) -> IResult<&str, Hand> {
        let (remaining, cards) = count(|i| Card::parse(i, ruleset), 5)(input)?;
        let cards = cards.try_into().expect("vector to have 5 elements");
        Ok((remaining, Hand { cards, ruleset }))
    }

    fn result(&self) -> HandResult {
        let mut count: BTreeMap<Card, i64> = BTreeMap::new();
        for card in self.cards {
            count.entry(card).and_modify(|c| *c += 1).or_insert(1);
        }
        let num_jokers = count.remove(&Card::Joker).unwrap_or(0);
        if num_jokers == 5 {
            return HandResult::FiveOfAKind;
        }
        let mut sorted_values: Vec<i64> = count.values().map(|c| c.to_owned()).collect();
        sorted_values.sort_by_key(|w| Reverse(*w));
        // If the values are sorted descending, then the lexicographical order
        // matches the highest hand result. It is therefore sufficient to simply
        // add the number of jokers to the card with the most occurrences.
        sorted_values[0] += num_jokers;
        match sorted_values[..] {
            [1, 1, 1, 1, 1] => HandResult::HighCard,
            [2, 1, 1, 1] => HandResult::OnePair,
            [2, 2, 1] => HandResult::TwoPair,
            [3, 1, 1] => HandResult::ThreeOfAKind,
            [3, 2] => HandResult::FullHouse,
            [4, 1] => HandResult::FourOfAKind,
            [5] => HandResult::FiveOfAKind,
            _ => panic!("Impossible"),
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let r1 = self.result();
        let r2 = other.result();
        if r1 != r2 {
            r1.cmp(&r2)
        } else {
            self.cards.cmp(&other.cards)
        }
    }
}

struct Game {
    hands_and_bids: Vec<(Hand, i64)>,
}

impl Game {
    fn parse(input: &str, ruleset: RuleSet) -> IResult<&str, Game> {
        let (remaining, hands_and_bids) =
            separated_list1(newline, tuple((|i| Hand::parse(i, ruleset), space1, i64)))(input)?;
        let hands_and_bids = hands_and_bids
            .iter()
            .map(|(hand, _, bid)| (hand.to_owned(), bid.to_owned()))
            .collect();
        Ok((remaining, Game { hands_and_bids }))
    }

    fn total_winnings(&self) -> i64 {
        let mut sorted_hands_and_bids: Vec<&(Hand, i64)> = self.hands_and_bids.iter().collect();
        sorted_hands_and_bids.sort();
        let mut rank = 0;
        let mut result = 0;
        let mut previous_hand = None;
        for (hand, bid) in sorted_hands_and_bids {
            if previous_hand != Some(hand) {
                rank += 1;
            }
            result += rank * *bid;
            previous_hand = Some(hand);
        }
        result
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(253603890, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(253630098, solution2());
    }

    #[test]
    fn example_part1() {
        let input = indoc! {"
                32T3K 765
                T55J5 684
                KK677 28
                KTJJT 220
                QQQJA 483
            "};
        let (_, game) = Game::parse(input, RuleSet::Traditional).expect("no parse error");
        assert_eq!(6440, game.total_winnings());
    }

    #[test]
    fn example_part2() {
        let input = indoc! {"
                32T3K 765
                T55J5 684
                KK677 28
                KTJJT 220
                QQQJA 483
            "};
        let (_, game) = Game::parse(input, RuleSet::Joker).expect("no parse error");
        assert_eq!(5905, game.total_winnings());
    }
}
