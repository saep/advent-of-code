use std::{
    collections::HashMap,
    ops::{Index, IndexMut, Range},
};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, i32, newline},
    combinator::{map, value},
    multi::{many0, separated_list0, separated_list1},
    sequence::{delimited, separated_pair, terminated, tuple},
    IResult,
};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day19_1.txt").expect("day19_1.txt to exist");
    let workflows_and_objects = WorkflowsAndObjects::parse(&input).expect("parseable file");
    workflows_and_objects.1.sum_accepted_objects()
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day19_1.txt").expect("day19_1.txt to exist");
    let workflows_and_objects = WorkflowsAndObjects::parse(&input).expect("parseable file");
    workflows_and_objects.1.num_acceptable_objects()
}

struct WorkflowsAndObjects {
    workflows: HashMap<String, Workflow>,
    objects: Vec<Object<i32>>,
}

impl WorkflowsAndObjects {
    fn parse(input: &str) -> IResult<&str, WorkflowsAndObjects> {
        let workflows = terminated(
            separated_list1(newline, Workflow::parse),
            tuple((newline, newline)),
        );
        let objects = terminated(
            separated_list0(newline, Object::<i32>::parse),
            many0(newline),
        );
        map(tuple((workflows, objects)), |(workflows, objects)| {
            let workflows = HashMap::from_iter(
                workflows
                    .into_iter()
                    .map(|workflow| (workflow.name.clone(), workflow)),
            );
            WorkflowsAndObjects { workflows, objects }
        })(input)
    }

    fn sum_accepted_objects(&self) -> usize {
        self.objects
            .iter()
            .filter(|object| self.process(object) == Outcome::Accept)
            .map(Object::sum_ratings)
            .sum()
    }

    fn process(&self, object: &Object<i32>) -> Outcome {
        let mut next_workflow = String::from("in");
        loop {
            match self.workflows[&next_workflow].process(object) {
                Outcome::Accept => return Outcome::Accept,
                Outcome::Reject => return Outcome::Reject,
                Outcome::PassToWorkflow(workflow) => {
                    next_workflow = workflow;
                }
            }
        }
    }

    fn num_acceptable_objects(&self) -> usize {
        let start_object = Object {
            x: 1..4001,
            m: 1..4001,
            a: 1..4001,
            s: 1..4001,
        };
        self.num_acceptable_objects_rec(&self.workflows["in"], vec![start_object])
    }

    fn num_acceptable_objects_rec(
        &self,
        workflow: &Workflow,
        objects: Vec<Object<Range<i32>>>,
    ) -> usize {
        workflow
            .rules
            .iter()
            .fold(
                (objects, 0),
                |(objects, num_combinations), rule| match rule {
                    Rule::Next(Outcome::Reject) => (vec![], num_combinations),
                    Rule::Next(Outcome::Accept) => (
                        vec![],
                        num_combinations + objects.iter().map(Object::combinations).sum::<usize>(),
                    ),
                    Rule::Next(Outcome::PassToWorkflow(w)) => (
                        vec![],
                        num_combinations
                            + self.num_acceptable_objects_rec(&self.workflows[w], objects),
                    ),
                    Rule::Condition {
                        field,
                        operator,
                        num,
                        if_met,
                    } => {
                        let (keep, try_next): (Vec<_>, Vec<_>) = objects
                            .iter()
                            .map(|o| {
                                let r = operator.bisect(*num, &o[*field]);
                                (
                                    o.with_field_set_to(*field, r.keep),
                                    o.with_field_set_to(*field, r.try_next),
                                )
                            })
                            .collect();
                        match if_met {
                            Outcome::Reject => (try_next, num_combinations),
                            Outcome::Accept => (
                                try_next,
                                num_combinations
                                    + keep.into_iter().map(|o| o.combinations()).sum::<usize>(),
                            ),
                            Outcome::PassToWorkflow(next_workflow) => {
                                let next_workflow = &self.workflows[next_workflow];
                                (
                                    try_next,
                                    num_combinations
                                        + self.num_acceptable_objects_rec(next_workflow, keep),
                                )
                            }
                        }
                    }
                },
            )
            .1
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Workflow {
    name: String,
    rules: Vec<Rule>,
}

impl Workflow {
    fn parse(input: &str) -> IResult<&str, Workflow> {
        map(
            tuple((
                alpha1,
                delimited(tag("{"), separated_list1(tag(","), Rule::parse), tag("}")),
            )),
            |(name, rules)| Workflow {
                name: String::from(name),
                rules,
            },
        )(input)
    }

    fn process(&self, object: &Object<i32>) -> Outcome {
        self.rules
            .iter()
            .filter_map(|rule| object.outcome_if_rule_is_met(rule))
            .next()
            .expect("Workflow must have an outcome")
    }
}

#[derive(Debug, Clone, Copy)]
struct Object<T> {
    x: T,
    m: T,
    a: T,
    s: T,
}

impl<T> Index<Field> for Object<T> {
    type Output = T;

    fn index(&self, index: Field) -> &Self::Output {
        match index {
            Field::X => &self.x,
            Field::M => &self.m,
            Field::A => &self.a,
            Field::S => &self.s,
        }
    }
}

impl<T> IndexMut<Field> for Object<T> {
    fn index_mut(&mut self, index: Field) -> &mut Self::Output {
        match index {
            Field::X => &mut self.x,
            Field::M => &mut self.m,
            Field::A => &mut self.a,
            Field::S => &mut self.s,
        }
    }
}

impl<T> Object<T> {
    fn parse(input: &str) -> IResult<&str, Object<i32>> {
        let props = separated_list1(tag(","), separated_pair(alpha1, tag("="), i32));
        map(delimited(tag("{"), props, tag("}")), |props| {
            let map: HashMap<String, i32> =
                HashMap::from_iter(props.into_iter().map(|(k, v)| (String::from(k), v)));
            Object {
                x: map["x"],
                m: map["m"],
                a: map["a"],
                s: map["s"],
            }
        })(input)
    }
}

impl<T: Clone> Object<T> {
    fn with_field_set_to(&self, field: Field, value: T) -> Object<T> {
        let mut copy = self.clone();
        copy[field] = value;
        copy
    }
}

impl Object<i32> {
    fn sum_ratings(&self) -> usize {
        let sum: i64 = [self.x, self.m, self.a, self.s]
            .iter()
            .map(|n| i64::from(*n))
            .sum();
        usize::try_from(sum).expect("result to fit in usize")
    }

    fn outcome_if_rule_is_met(&self, rule: &Rule) -> Option<Outcome> {
        match rule {
            Rule::Condition {
                field,
                operator,
                num,
                if_met,
            } => match operator {
                Operator::LT if self[*field] < *num => Some(if_met.clone()),
                Operator::GT if self[*field] > *num => Some(if_met.clone()),
                _ => None,
            },
            Rule::Next(outcome) => Some(outcome.clone()),
        }
    }
}

impl Object<Range<i32>> {
    fn combinations(&self) -> usize {
        [Field::X, Field::M, Field::A, Field::S]
            .into_iter()
            .map(|f| (self[f].end - self[f].start) as usize)
            .product()
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum Outcome {
    Accept,
    Reject,
    PassToWorkflow(String),
}

impl Outcome {
    fn parse(input: &str) -> IResult<&str, Outcome> {
        let accept = value(Outcome::Accept, tag("A"));
        let reject = value(Outcome::Reject, tag("R"));
        let pass = map(alpha1, |workflow: &str| {
            Outcome::PassToWorkflow(workflow.to_owned())
        });
        alt((accept, reject, pass))(input)
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
enum Field {
    X,
    M,
    A,
    S,
}

impl Field {
    fn parse(input: &str) -> IResult<&str, Field> {
        alt((
            value(Field::X, tag("x")),
            value(Field::M, tag("m")),
            value(Field::A, tag("a")),
            value(Field::S, tag("s")),
        ))(input)
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
enum Operator {
    LT,
    GT,
}

struct OperatorResult {
    keep: Range<i32>,
    try_next: Range<i32>,
}

impl Operator {
    fn parse(input: &str) -> IResult<&str, Operator> {
        alt((value(Operator::LT, tag("<")), value(Operator::GT, tag(">"))))(input)
    }

    fn bisect(&self, at: i32, range: &Range<i32>) -> OperatorResult {
        match self {
            Operator::LT => OperatorResult {
                keep: range.start..at,
                try_next: at..range.end,
            },
            Operator::GT => OperatorResult {
                keep: (at + 1)..range.end,
                try_next: range.start..(at + 1),
            },
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
enum Rule {
    Condition {
        field: Field,
        operator: Operator,
        num: i32,
        if_met: Outcome,
    },
    Next(Outcome),
}

impl Rule {
    fn parse(input: &str) -> IResult<&str, Rule> {
        let next = map(Outcome::parse, Rule::Next);
        let op = map(
            tuple((
                Field::parse,
                Operator::parse,
                terminated(i32, tag(":")),
                Outcome::parse,
            )),
            |(field, operator, num, if_met)| Rule::Condition {
                field,
                operator,
                num,
                if_met,
            },
        );
        alt((op, next))(input)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use indoc::indoc;
    use rstest::rstest;

    #[test]
    fn expected_solution1() {
        assert_eq!(solution1(), 367602);
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(solution2(), 125317461667458);
    }

    #[rstest]
    #[case::lt(
        "a<2006:qkq",
        Rule::Condition {
            field: Field::A,
            operator: Operator::LT,
            num: 2006,
            if_met: Outcome::PassToWorkflow("qkq".to_owned()),
        }
    )]
    #[case::gt(
        "m>2090:A",
        Rule::Condition {
            field: Field::M,
            operator: Operator::GT,
            num: 2090,
            if_met: Outcome::Accept,
        }
    )]
    #[case::next(
        "rfg",
        Rule::Next(Outcome::PassToWorkflow("rfg".to_owned()))
    )]
    fn parse_rule(#[case] input: &str, #[case] expected: Rule) {
        let actual = Rule::parse(input);
        assert_eq!(actual, Ok(("", expected)));
    }

    #[test]
    fn parse_workflow() {
        let actual = Workflow::parse("crn{x>2662:A,R}");
        let expected = Workflow {
            name: String::from("crn"),
            rules: vec![
                Rule::Condition {
                    field: Field::X,
                    operator: Operator::GT,
                    num: 2662,
                    if_met: Outcome::Accept,
                },
                Rule::Next(Outcome::Reject),
            ],
        };
        assert_eq!(actual, Ok(("", expected)));
    }

    #[test]
    fn parse_workflow_and_objects() {
        let actual = WorkflowsAndObjects::parse(indoc! {"
            hdj{m>838:A,pv}

            {x=787,m=2655,a=1222,s=2876}"
        })
        .expect("valid parse");
        assert_eq!("", actual.0);
        assert_eq!(actual.1.workflows.len(), 1);
        assert_eq!(
            actual.1.workflows["hdj"],
            Workflow {
                name: String::from("hdj"),
                rules: vec![
                    Rule::Condition {
                        field: Field::M,
                        operator: Operator::GT,
                        num: 838,
                        if_met: Outcome::Accept
                    },
                    Rule::Next(Outcome::PassToWorkflow(String::from("pv")))
                ]
            }
        );
    }

    #[rstest]
    #[case::reject(
        indoc! {"
            in{R}

            "},
            0
    )]
    #[case::accept(
        indoc! {"
            in{A}

            "},
            4000*4000*4000*4000
   )]
    #[case::pass_to_next_workflow(
        indoc! {"
            in{other}
            other{A}

            "},
            4000*4000*4000*4000
   )]
    #[case::accept_and_pass_to_next(
        indoc! {"
            in{a>2000:other,A}
            other{A}

            "},
            4000*4000*4000*4000
   )]
    #[case::split_and_reject(
        indoc! {"
            in{a>2000:A,x<2001:A,R}

            "},
            2000*4000*4000*4000 + 2000*2000*4000*4000
   )]
    #[case::example(
        indoc! {"
            px{a<2006:qkq,m>2090:A,rfg}
            pv{a>1716:R,A}
            lnx{m>1548:A,A}
            rfg{s<537:gd,x>2440:R,A}
            qs{s>3448:A,lnx}
            qkq{x<1416:A,crn}
            crn{x>2662:A,R}
            in{s<1351:px,qqz}
            qqz{s>2770:qs,m<1801:hdj,R}
            gd{a>3333:R,R}
            hdj{m>838:A,pv}

        "},
        167409079868000
    )]
    fn num_combinations(#[case] input: &str, #[case] expected: usize) {
        let sut = WorkflowsAndObjects::parse(input).expect("must parse");
        assert_eq!(sut.0, "");
        let sut = sut.1;

        assert_eq!(sut.num_acceptable_objects(), expected);
    }
}
