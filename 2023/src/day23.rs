use std::collections::{HashMap, HashSet};

use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::newline,
    combinator::value,
    multi::{many0, many1, separated_list1},
    sequence::terminated,
    IResult,
};

use crate::two_dimensional::{Array2D, Direction, Point2D};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day23_1.txt").expect("File to exist");
    HikingTrails::parse(&input)
        .with_slippery_slopes()
        .longest_trail()
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day23_1.txt").expect("File to exist");
    HikingTrails::parse(&input)
        .without_slippery_slopes()
        .longest_trail()
}

#[derive(Debug, PartialEq, Eq)]
struct HikingTrails {
    start: Point2D,
    end: Point2D,
    trails: Array2D<Tile>,
}

impl HikingTrails {
    fn parse(input: &str) -> Self {
        let row = |input| many1(Tile::parse)(input);
        let (rem, trails) =
            terminated(separated_list1(newline, row), many0(newline))(input).expect("valid input");
        assert_eq!("", rem);
        let trails = Array2D::new(trails).expect("valid input");
        let start = (0..(trails.width()))
            .map(|x| (x as i64, 0).into())
            .find(|pos| trails[*pos] == Tile::Path)
            .expect("The starting position must be in the first row");
        let end = (0..(trails.width()))
            .rev()
            .map(|x| (x as i64, trails.height() as i64 - 1).into())
            .find(|pos| trails[*pos] == Tile::Path)
            .expect("The ending position must be in the last row");
        Self { start, end, trails }
    }

    fn with_slippery_slopes(self) -> Graph {
        let neighbours = |pos, edge_filter: EdgeFilter| match self.trails[pos] {
            Tile::Forest => Vec::new(),
            Tile::Path => self
                .trails
                .orthogonal_neighbours(pos)
                .filter(|(dir, _, tile)| match tile {
                    Tile::Forest => false,
                    Tile::Slope(slope_dir) => edge_filter.is_valid_direction(*slope_dir, *dir),
                    Tile::Path => true,
                })
                .map(|t| t.1)
                .collect(),
            Tile::Slope(slope_dir) => self
                .trails
                .orthogonal_neighbours(pos)
                .filter(|n| edge_filter.is_valid_direction(slope_dir, n.0))
                .filter(|(_, _, tile)| match tile {
                    Tile::Forest => false,
                    Tile::Slope(_) | Tile::Path => true,
                })
                .map(|t| t.1)
                .collect(),
        };
        let nodes: HashSet<Point2D> = self
            .trails
            .all_positions()
            .filter(|pos| neighbours(*pos, EdgeFilter::All).len() != 2)
            .collect();
        let distances = nodes
            .iter()
            .map(|node| {
                let distances: Vec<(Point2D, usize)> = neighbours(*node, EdgeFilter::OutBoundOnly)
                    .into_iter()
                    .filter_map(|next| {
                        let mut prev = *node;
                        let mut next = next;
                        let mut len = 1;
                        loop {
                            if nodes.contains(&next) {
                                return Some((next, len));
                            }
                            let tmp = neighbours(next, EdgeFilter::OutBoundOnly)
                                .into_iter()
                                .filter(|neighbour| neighbour != &prev)
                                .exactly_one()
                                .ok()?;
                            prev = next;
                            next = tmp;
                            len += 1;
                        }
                    })
                    .collect();
                (*node, distances)
            })
            .filter(|(_, distances)| !distances.is_empty())
            .collect();
        Graph {
            start: self.start,
            end: self.end,
            distances,
        }
    }

    fn without_slippery_slopes(mut self) -> Graph {
        for pos in self.trails.all_positions() {
            if let Tile::Slope(_) = self.trails[pos] {
                self.trails[pos] = Tile::Path
            }
        }
        self.with_slippery_slopes()
    }
}

#[derive(Debug, Clone, Copy)]
enum EdgeFilter {
    OutBoundOnly,
    All,
}

impl EdgeFilter {
    fn is_valid_direction(self, slope_dir: Direction, dir: Direction) -> bool {
        match self {
            EdgeFilter::OutBoundOnly => slope_dir == dir,
            EdgeFilter::All => slope_dir == dir || slope_dir == dir.opposite(),
        }
    }
}

#[derive(Debug)]
struct Graph {
    start: Point2D,
    end: Point2D,
    distances: HashMap<Point2D, Vec<(Point2D, usize)>>,
}

impl Graph {
    fn longest_trail(&self) -> usize {
        let mut dfs = Dfs::new(self);
        dfs.longest_path(self.start).unwrap_or(0)
    }
}

struct Dfs<'a> {
    seen: HashSet<Point2D>,
    graph: &'a Graph,
}

impl<'a> Dfs<'a> {
    fn new(graph: &'a Graph) -> Self {
        Dfs {
            seen: HashSet::new(),
            graph,
        }
    }
    fn longest_path(&mut self, pos: Point2D) -> Option<usize> {
        if pos == self.graph.end {
            return Some(0);
        }
        let paths = match self.graph.distances.get(&pos) {
            None => return None,
            Some(paths) => paths,
        };
        self.seen.insert(pos);
        let mut result = 0;
        for (to, len) in paths {
            if !self.seen.contains(to) {
                if let Some(path_len) = self.longest_path(*to) {
                    result = result.max(len + path_len);
                }
            }
        }
        self.seen.remove(&pos);
        if result == 0 {
            None
        } else {
            Some(result)
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
enum Tile {
    Forest,
    Path,
    Slope(Direction),
}

impl Tile {
    fn parse(input: &str) -> IResult<&str, Tile> {
        alt((
            value(Tile::Forest, tag("#")),
            value(Tile::Path, tag(".")),
            value(Tile::Slope(Direction::North), tag("^")),
            value(Tile::Slope(Direction::East), tag(">")),
            value(Tile::Slope(Direction::South), tag("v")),
            value(Tile::Slope(Direction::West), tag("<")),
        ))(input)
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(solution1(), 2086);
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(solution2(), 6526);
    }

    const EXAMPLE: &str = indoc! {"
        #.#####################
        #.......#########...###
        #######.#########.#.###
        ###.....#.>.>.###.#.###
        ###v#####.#v#.###.#.###
        ###.>...#.#.#.....#...#
        ###v###.#.#.#########.#
        ###...#.#.#.......#...#
        #####.#.#.#######.#.###
        #.....#.#.#.......#...#
        #.#####.#.#.#########v#
        #.#...#...#...###...>.#
        #.#.#v#######v###.###v#
        #...#.>.#...>.>.#.###.#
        #####v#.#.###v#.#.###.#
        #.....#...#...#.#.#...#
        #.#########.###.#.#.###
        #...###...#...#...#.###
        ###.###.#.###v#####v###
        #...#...#.#.>.>.#.>.###
        #.###.###.#.###.#.#v###
        #.....###...###...#...#
        #####################.#
    "};

    #[test]
    fn example_part1() {
        let actual = HikingTrails::parse(EXAMPLE);
        assert_eq!(actual.with_slippery_slopes().longest_trail(), 94);
    }

    #[rstest]
    #[case::single_step(1, indoc!{"
        #.#
        #.#
    "})]
    #[case::only_junctions(4, indoc!{"
        #.##
        #..#
        #..#
        ##.#
    "})]
    #[case::two_paths(12, indoc!{"
        #.######
        #.....##
        #.###..#
        #.####.#
        #......#
        ####.###
    "})]
    #[case::example(154, EXAMPLE)]
    fn part2_examples(#[case] expected: usize, #[case] input: &str) {
        let actual = HikingTrails::parse(input);
        assert_eq!(actual.without_slippery_slopes().longest_trail(), expected);
    }
}
