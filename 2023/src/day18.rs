use std::num::ParseIntError;

use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::{
        complete::{newline, space1, u8},
        streaming::satisfy,
    },
    combinator::{map, map_res, value},
    multi::{count, separated_list1},
    sequence::{delimited, terminated, tuple},
    AsChar, IResult,
};

use crate::two_dimensional::{Direction, Point2D};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day18_1.txt").expect("Input file must exist");
    TrenchPlan::parse(&input)
        .expect("Input must be parseable")
        .1
        .volume()
        .try_into()
        .expect("Volume to not overflow")
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day18_1.txt").expect("Input file must exist");
    TrenchPlan::parse(&input)
        .expect("Input must be parseable")
        .1
        .with_corrected_instructions()
        .volume()
        .try_into()
        .expect("Volume to not overflow")
}

#[derive(Debug, PartialEq, Eq)]
struct TrenchPlan {
    instructions: Vec<Instruction>,
}

#[derive(Debug, PartialEq, Eq)]
struct Instruction {
    direction: Direction,
    steps: u32,
    color_steps: u32,
    color_direction: Direction,
}

impl Instruction {
    fn parse(input: &str) -> IResult<&str, Instruction> {
        let parse_direction = |input| {
            terminated(
                alt((
                    value(Direction::North, tag("U")),
                    value(Direction::East, tag("R")),
                    value(Direction::South, tag("D")),
                    value(Direction::West, tag("L")),
                )),
                space1,
            )(input)
        };
        let parse_direction_hex = |input| {
            alt((
                value(Direction::North, tag("3")),
                value(Direction::East, tag("0")),
                value(Direction::South, tag("1")),
                value(Direction::West, tag("2")),
            ))(input)
        };
        let parse_num_steps = |input| terminated(u8, space1)(input);
        let parse_color = |input| {
            map_res(
                delimited(
                    tag("(#"),
                    tuple((count(satisfy(|c| c.is_hex_digit()), 5), parse_direction_hex)),
                    tag(")"),
                ),
                |(steps, direction): (Vec<char>, Direction)| {
                    let steps = String::from_iter(steps);
                    let steps: u32 = u32::from_str_radix(&steps, 16)?;
                    let result: Result<_, ParseIntError> = Ok((steps, direction));
                    result
                },
            )(input)
        };
        let parse_instruction = |input| {
            map(
                tuple((parse_direction, parse_num_steps, parse_color)),
                |(direction, steps, (color_steps, color_direction))| Self {
                    direction,
                    steps: steps.into(),
                    color_steps,
                    color_direction,
                },
            )(input)
        };
        parse_instruction(input)
    }

    fn to_correct_instructions(&self) -> Self {
        Self {
            steps: self.color_steps,
            direction: self.color_direction,
            ..*self
        }
    }
}

impl TrenchPlan {
    fn parse(input: &str) -> IResult<&str, TrenchPlan> {
        map(
            separated_list1(newline, Instruction::parse),
            |instructions| TrenchPlan { instructions },
        )(input)
    }

    fn volume(&self) -> u64 {
        let start = Point2D { x: 0, y: 0 };
        let mut cycle = Vec::with_capacity(self.instructions.len() + 2);
        cycle.push(start);
        let mut p = start;
        for instruction in self.instructions.iter() {
            p = p.moved_by(instruction.direction, instruction.steps.into());
            cycle.push(p);
        }
        let trench_len: u64 = self
            .instructions
            .iter()
            .map(|i| Into::<u64>::into(i.steps))
            .sum();
        cycle.push(start);
        // shoelace formula
        let volume_twice = cycle
            .iter()
            .tuple_windows()
            .map(|(p1, p2)| p1.x * p2.y - p2.x * p1.y)
            .sum::<i64>();
        let volume_2: u64 = volume_twice.abs().try_into().unwrap();
        (volume_2 + trench_len) / 2 + 1
    }

    fn with_corrected_instructions(&self) -> Self {
        let instructions = self
            .instructions
            .iter()
            .map(|x| x.to_correct_instructions())
            .collect();
        TrenchPlan { instructions }
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn parse_instruction() {
        let actual = Instruction::parse("R 6 (#70c710)").unwrap().1;
        let expected = Instruction {
            direction: Direction::East,
            steps: 6,
            color_direction: Direction::East,
            color_steps: 461937,
        };
        assert_eq!(actual, expected);
    }

    #[test]
    fn parse_trench_plan() {
        let actual = TrenchPlan::parse("D 5 (#0dc571)\nL 2 (#5713f0)").unwrap().1;
        let expected = TrenchPlan {
            instructions: vec![
                Instruction {
                    direction: Direction::South,
                    steps: 5,
                    color_direction: Direction::South,
                    color_steps: 56407,
                },
                Instruction {
                    direction: Direction::West,
                    steps: 2,
                    color_direction: Direction::East,
                    color_steps: 356671,
                },
            ],
        };
        assert_eq!(actual, expected);
    }

    #[test]
    fn example_part1() {
        let input = indoc! {"
            R 6 (#70c710)
            D 5 (#0dc571)
            L 2 (#5713f0)
            D 2 (#d2c081)
            R 2 (#59c680)
            D 2 (#411b91)
            L 5 (#8ceee2)
            U 2 (#caa173)
            L 1 (#1b58a2)
            U 2 (#caa171)
            R 2 (#7807d2)
            U 3 (#a77fa3)
            L 2 (#015232)
            U 2 (#7a21e3)
            "};
        let volume = TrenchPlan::parse(input).unwrap().1.volume();

        assert_eq!(volume, 62);
    }

    #[test]
    fn expected_solution1() {
        assert_eq!(solution1(), 38188);
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(solution2(), 93325849869340);
    }
}
