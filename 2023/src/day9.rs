use std::iter::zip;

use nom::{
    character::complete::{i64, space1},
    multi::separated_list1,
    IResult,
};

pub fn solution1() -> i64 {
    let input = std::fs::read_to_string("inputs/day9_1.txt").expect("file to exist");
    input
        .lines()
        .map(NumberSequence::parse)
        .map(|seq| seq.expect("line to parse").1)
        .map(|seq| seq.guess_next_number())
        .sum()
}

pub fn solution2() -> i64 {
    let input = std::fs::read_to_string("inputs/day9_1.txt").expect("file to exist");
    input
        .lines()
        .map(NumberSequence::parse)
        .map(|seq| seq.expect("line to parse").1)
        .map(|seq| seq.guess_previous_number())
        .sum()
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct NumberSequence {
    numbers: Vec<i64>,
}

impl NumberSequence {
    fn parse(input: &str) -> IResult<&str, NumberSequence> {
        let (remeining, numbers) = separated_list1(space1, i64)(input)?;
        Ok((remeining, NumberSequence { numbers }))
    }

    fn diffs_from_all_zeroes_to_numbers(&self) -> Vec<Vec<i64>> {
        let mut diffs = vec![];
        diffs.push(self.numbers.clone());
        let mut last = diffs.last().unwrap();
        while last.iter().any(|n| *n != 0) {
            let diff: Vec<i64> = zip(last.iter().skip(1), last.iter())
                .map(|(a, b)| a - b)
                .collect();
            diffs.push(diff);
            last = diffs.last().unwrap();
        }
        diffs.reverse();
        diffs
    }

    fn guess_next_number(&self) -> i64 {
        self.diffs_from_all_zeroes_to_numbers()
            .into_iter()
            .filter_map(|vec| vec.last().copied())
            .fold(0, |next_number, last| last + next_number)
    }

    fn guess_previous_number(&self) -> i64 {
        self.diffs_from_all_zeroes_to_numbers()
            .into_iter()
            .filter_map(|vec| vec.first().copied())
            .fold(0, |next_number, first| first - next_number)
    }
}

#[cfg(test)]
mod test {
    use rstest::rstest;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(2174807968, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(1208, solution2());
    }

    #[rstest]
    #[case("0 3 6 9 12 15", 18)]
    #[case("1 3 6 10 15 21", 28)]
    #[case("10 13 16 21 30 45", 68)]
    fn examples_part1(#[case] input: &str, #[case] expected: i64) {
        let next_number = NumberSequence::parse(input).unwrap().1.guess_next_number();
        assert_eq!(expected, next_number);
    }

    #[rstest]
    #[case("10 13 16 21 30 45", 5)]
    fn examples_part2(#[case] input: &str, #[case] expected: i64) {
        let previous_number = NumberSequence::parse(input)
            .unwrap()
            .1
            .guess_previous_number();
        assert_eq!(expected, previous_number);
    }
}
