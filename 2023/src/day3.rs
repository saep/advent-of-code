use std::{iter::zip, ops::Range};

use itertools::Itertools;
use num::traits::clamp_max;

use crate::two_dimensional::Point2D;

pub fn solution1() -> u64 {
    let input = std::fs::read_to_string("inputs/day3_1.txt").unwrap();
    let schematic = Schematic::new(&input);
    schematic.sum_of_partnumbers()
}

pub fn solution2() -> u64 {
    let input = std::fs::read_to_string("inputs/day3_1.txt").unwrap();
    let schematic = Schematic::new(&input);
    schematic.sum_of_gear_ratios()
}

#[derive(Debug, PartialEq, Clone)]
struct Schematic<'a> {
    lines: Vec<&'a str>,
}

#[derive(Debug, PartialEq, Clone)]
struct PartNumber {
    line_nr: usize,
    range: Range<usize>,
    number: u64,
}

type Position = Point2D<usize>;

impl PartNumber {
    fn from_line(line_nr: usize, line: &str) -> impl Iterator<Item = PartNumber> + '_ {
        let mut part_numbers: Vec<Range<usize>> = vec![];
        let mut start: Option<usize> = None;
        line.bytes().enumerate().for_each(|(i, chr)| {
            if chr.is_ascii_digit() {
                if start.is_none() {
                    start = Some(i);
                }
            } else {
                if let Some(start) = start {
                    part_numbers.push(start..i);
                }
                start = None;
            }
        });
        if let Some(start) = start {
            part_numbers.push(start..line.len());
        }
        part_numbers
            .into_iter()
            .map(move |range: Range<usize>| -> PartNumber {
                let number = line[range.clone()].parse().unwrap();
                PartNumber {
                    line_nr,
                    range,
                    number,
                }
            })
    }

    fn is_adjacent_to_symbol(&self, schematic: &Schematic) -> bool {
        self.adjacent_cell_positions()
            .into_iter()
            .any(|position| schematic.is_symbol(&position))
    }

    fn is_adjacent_to(&self, position: &Position) -> bool {
        self.adjacent_cell_positions()
            .into_iter()
            .any(|p| &p == position)
    }

    fn adjacent_cell_positions(&self) -> impl IntoIterator<Item = Position> {
        let mut positions: Vec<Position> = vec![];
        let above_or_below = move |line_nr: usize| {
            let start = self.range.start.saturating_sub(1);
            (start..=self.range.end).map(move |column_nr| Position {
                y: line_nr,
                x: column_nr,
            })
        };
        if self.line_nr > 0 {
            for position in above_or_below(self.line_nr - 1) {
                positions.push(position)
            }
        };
        for position in above_or_below(self.line_nr + 1) {
            positions.push(position);
        }
        if self.range.start > 0 {
            positions.push(Position {
                y: self.line_nr,
                x: self.range.start - 1,
            });
        }
        positions.push(Position {
            y: self.line_nr,
            x: self.range.end,
        });
        positions
    }
}

impl Schematic<'_> {
    fn new(input: &str) -> Schematic {
        Schematic {
            lines: input.lines().collect(),
        }
    }

    fn sum_of_partnumbers(&self) -> u64 {
        self.lines
            .iter()
            .enumerate()
            .flat_map(|(line_nr, line)| PartNumber::from_line(line_nr, line))
            .filter(|p: &PartNumber| p.is_adjacent_to_symbol(self))
            .map(|p: PartNumber| p.number)
            .sum()
    }

    fn is_symbol(&self, position: &Position) -> bool {
        let chr = self
            .lines
            .get(position.y)
            .and_then(|line| line.as_bytes().get(position.x))
            .copied()
            .unwrap_or(b'.');
        !chr.is_ascii_digit() && chr != b'.'
    }

    fn gear_positions(&self) -> impl Iterator<Item = Position> + '_ {
        self.lines.iter().enumerate().flat_map(|(line_nr, line)| {
            line.bytes()
                .enumerate()
                .filter(|(_, chr)| *chr == b'*')
                .map(move |(column_nr, _)| Position {
                    y: line_nr,
                    x: column_nr,
                })
        })
    }

    fn adjacent_part_numbers<'a>(
        &'a self,
        position: &'a Position,
    ) -> impl Iterator<Item = PartNumber> + 'a {
        let start = clamp_max(position.y.saturating_sub(1), self.lines.len());
        zip(start.., self.lines[start..].iter().take(3))
            .flat_map(|(line_nr, line)| PartNumber::from_line(line_nr, line))
            .filter(|part_number| part_number.is_adjacent_to(position))
    }

    fn sum_of_gear_ratios(&self) -> u64 {
        self.gear_positions()
            .map(|gear_position| {
                let part_numbers = self
                    .adjacent_part_numbers(&gear_position)
                    .take(3)
                    .collect_vec();
                if part_numbers.len() != 2 {
                    0
                } else {
                    part_numbers
                        .into_iter()
                        .map(|part_number| part_number.number)
                        .product::<u64>()
                }
            })
            .sum()
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(537732, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(84883664, solution2());
    }

    #[test]
    fn example_from_text() {
        let schematic = Schematic::new(indoc! {"
                467..114..
                ...*......
                ..35..633.
                ......#...
                617*......
                .....+.58.
                ..592.....
                ......755.
                ...$.*....
                .664.598..
            "});
        assert_eq!(4361, schematic.sum_of_partnumbers());
    }

    #[test]
    fn example_from_reddit() {
        let schematic = Schematic::new(indoc! {"
                12.......*..
                +.........34
                .......-12..
                ..78........
                ..*....60...
                78.........9
                .5.....23..$
                8...90*12...
                ............
                2.2......12.
                .*.........*
                1.1..503+.56
            "});
        assert_eq!(925, schematic.sum_of_partnumbers());
    }

    #[test]
    fn example_from_text_part2() {
        let schematic = Schematic::new(indoc! {"
                467..114..
                ...*......
                ..35..633.
                ......#...
                617*......
                .....+.58.
                ..592.....
                ......755.
                ...$.*....
                .664.598..
            "});
        assert_eq!(467835, schematic.sum_of_gear_ratios());
    }
}
