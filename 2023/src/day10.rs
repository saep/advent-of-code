use std::iter::zip;
use crate::two_dimensional::{Point2D, Direction};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::newline,
    combinator::{map, value},
    multi::{many1, separated_list1},
    IResult,
};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("inputs/day10_1.txt").expect("file to exist");
    let maze = PipeMaze::parse(&input).expect("parsing to succeed").1;
    maze.farthest_distance().expect("input to have a result")
}

pub fn solution2() -> i64 {
    let input = std::fs::read_to_string("inputs/day10_1.txt").expect("file to exist");
    let maze = PipeMaze::parse(&input).expect("parsing to succeed").1;
    maze.enclosed_tiles().unwrap()
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Tile {
    Ground,
    Start,
    Pipe { directions: [Direction; 2] },
}

#[derive(Debug)]
struct PipeMaze {
    rows: Vec<Vec<Tile>>,
}

impl PipeMaze {
    fn parse_row(input: &str) -> IResult<&str, Vec<Tile>> {
        use Direction::*;
        many1(alt((
            value(Tile::Ground, tag(".")),
            value(Tile::Start, tag("S")),
            value(
                Tile::Pipe {
                    directions: [North, South],
                },
                tag("|"),
            ),
            value(
                Tile::Pipe {
                    directions: [North, East],
                },
                tag("L"),
            ),
            value(
                Tile::Pipe {
                    directions: [North, West],
                },
                tag("J"),
            ),
            value(
                Tile::Pipe {
                    directions: [East, South],
                },
                tag("F"),
            ),
            value(
                Tile::Pipe {
                    directions: [East, West],
                },
                tag("-"),
            ),
            value(
                Tile::Pipe {
                    directions: [South, West],
                },
                tag("7"),
            ),
        )))(input)
    }

    fn parse(input: &str) -> IResult<&str, PipeMaze> {
        map(separated_list1(newline, PipeMaze::parse_row), |rows| {
            PipeMaze { rows }
        })(input)
    }

    fn all_points(&self) -> impl Iterator<Item = Point2D> + '_ {
        self.rows.iter().enumerate().flat_map(|(y, row)| {
            row.iter().enumerate().map(move |(x, _)| Point2D {
                x: x as i64,
                y: y as i64,
            })
        })
    }

    fn cycle(&self) -> Option<Vec<Point2D>> {
        let start: Point2D = self
            .all_points()
            .find(|p| self.tile_at(p) == Some(Tile::Start))?;
        let mut prev = start;
        let mut next = self.connecting_pipes(&start).first().copied()?;
        let mut cycle = vec![start];
        while next != start {
            cycle.push(next);
            let next_tmp = self
                .connecting_pipes(&next)
                .into_iter()
                .find(|p| *p != prev)?;
            prev = next;
            next = next_tmp;
        }
        Some(cycle)
    }

    fn farthest_distance(&self) -> Option<usize> {
        self.cycle().map(|cycle: Vec<Point2D>| (cycle.len() + 1) / 2)
    }

    fn connecting_pipes(&self, p: &Point2D) -> Vec<Point2D> {
        match self.tile_at(p).unwrap_or(Tile::Ground) {
            Tile::Ground => vec![],
            Tile::Start => Direction::all()
                .into_iter()
                .map(|d| p.moved(d))
                .filter(|point| Tile::Ground != self.tile_at(point).unwrap_or(Tile::Ground))
                .collect::<Vec<Point2D>>(),
            Tile::Pipe { directions } => directions.iter().map(|d| p.moved(*d)).collect(),
        }
    }

    fn tile_at(&self, p: &Point2D) -> Option<Tile> {
        if p.x < 0 || p.y < 0 {
            None
        } else {
            self.rows.get(p.y as usize)?.get(p.x as usize).copied()
        }
    }

    fn enclosed_tiles(&self) -> Option<i64> {
        let mut cycle = self.cycle()?;
        cycle.push(cycle.first().unwrap().to_owned());
        Some(shoelace(&cycle).abs() / 2 - (cycle.len() as i64 / 2 - 1))
    }
}

fn shoelace(path: &[Point2D]) -> i64 {
    zip(path.iter(), path.iter().skip(1))
        .map(|(p1, p2)| p1.x * p2.y - p2.x * p1.y)
        .sum()
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(6846, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(325, solution2());
    }

    #[test]
    fn example1_part1() {
        let input = indoc! {"
                .....
                .S-7.
                .|.|.
                .L-J.
                .....
        "};
        let maze = PipeMaze::parse(input).unwrap().1;
        assert_eq!(Some(4), maze.farthest_distance());
    }

    #[test]
    fn example2_part1() {
        let input = indoc! {"
                ..F7.
                .FJ|.
                SJ.L7
                |F--J
                LJ...
        "};
        let maze = PipeMaze::parse(input).unwrap().1;
        assert_eq!(Some(8), maze.farthest_distance());
    }

    #[test]
    fn example1_part2() {
        let input = indoc! {"
                ...........
                .S-------7.
                .|F-----7|.
                .||.....||.
                .||.....||.
                .|L-7.F-J|.
                .|..|.|..|.
                .L--J.L--J.
                ...........
        "};
        let maze = PipeMaze::parse(input).unwrap().1;
        assert_eq!(Some(4), maze.enclosed_tiles());
    }
}
