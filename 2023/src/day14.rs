use std::{collections::HashMap, fmt::Display, iter::successors, ops::Rem};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("inputs/day14_1.txt").expect("file to exist");
    let platform = Platform::parse(&input);
    platform.tilt_north().load()
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("inputs/day14_1.txt").expect("file to exist");
    let platform = Platform::parse(&input);
    platform.cycle(1000000000).load()
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
struct Platform {
    rows: Vec<Vec<u8>>,
}

impl Platform {
    fn parse(input: &str) -> Platform {
        Platform {
            rows: input.lines().map(|line| line.as_bytes().into()).collect(),
        }
    }

    fn tilt_north(mut self) -> Self {
        let num_rows = self.rows.len();
        for i_row in 0..self.rows.len() {
            for i_col in 0..self.rows[i_row].len() {
                if b'.' == self.rows[i_row][i_col] {
                    (i_row + 1..num_rows)
                        .find(|i| self.rows[*i][i_col] != b'.')
                        .filter(|i| self.rows[*i][i_col] == b'O')
                        .into_iter()
                        .for_each(|i| {
                            self.rows[i_row][i_col] = b'O';
                            self.rows[i][i_col] = b'.';
                        })
                }
            }
        }
        self
    }

    fn load(&self) -> usize {
        self.rows
            .iter()
            .enumerate()
            .map(|(i, row)| (self.rows.len() - i) * row.iter().filter(|c| **c == b'O').count())
            .sum()
    }

    fn cycle(self, times: usize) -> Self {
        if times == 0 {
            return self;
        }
        let mut results = HashMap::new();
        let cycles = successors(Some((0, self)), |(i, platform)| {
            Some((i + 1, platform.cycle_once()))
        })
        .take(times + 1);
        for (i, p) in cycles {
            let cycle_begins = results.entry(p).or_insert(i);
            if *cycle_begins != i {
                let cycle_length = i - *cycle_begins;
                let pos_equivalent_to_times =
                    (times - *cycle_begins).rem(cycle_length) + *cycle_begins;
                return results
                    .into_iter()
                    .find(|(_, pos)| *pos == pos_equivalent_to_times)
                    .map(|(p, _)| p)
                    .expect("matching element");
            }
        }
        results
            .into_iter()
            .find(|(_, pos)| *pos == times)
            .map(|(p, _)| p)
            .unwrap()
    }

    fn cycle_once(&self) -> Self {
        let mut rotated_4_times = self.clone();
        for _ in 0..4 {
            rotated_4_times = rotated_4_times.tilt_north().rotate();
        }
        rotated_4_times
    }

    fn rotate(self) -> Self {
        let len = self.rows[0].len();
        let mut row_iterators: Vec<_> = self.rows.into_iter().map(|row| row.into_iter()).collect();
        let rows = (0..len)
            .map(|_| {
                let mut transposed_row: Vec<u8> = row_iterators
                    .iter_mut()
                    .map(|it| it.next().expect("rows have same length"))
                    .collect();
                transposed_row.reverse();
                transposed_row
            })
            .collect();
        Self { rows }
    }
}

impl Display for Platform {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in self.rows.iter() {
            let _ = f.write_str(core::str::from_utf8(row).expect("utf8"));
            let _ = f.write_str("\n");
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(105784, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(91286, solution2());
    }

    #[test]
    fn example_part1() {
        let input = indoc! {"
                O....#....
                O.OO#....#
                .....##...
                OO.#O....O
                .O.....O#.
                O.#..O.#.#
                ..O..#O..O
                .......O..
                #....###..
                #OO..#....
            "};
        let platform = Platform::parse(input).tilt_north();
        assert_eq!(136, platform.load())
    }

    #[test]
    fn example_part2() {
        let input = indoc! {"
                O....#....
                O.OO#....#
                .....##...
                OO.#O....O
                .O.....O#.
                O.#..O.#.#
                ..O..#O..O
                .......O..
                #....###..
                #OO..#....
            "};
        let platform = Platform::parse(input).cycle(1000000000);
        assert_eq!(64, platform.load())
    }
}
