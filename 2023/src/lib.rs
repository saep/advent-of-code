pub mod three_dimensional;
pub mod two_dimensional;

pub mod day1;
pub mod day10;
pub mod day11;
pub mod day12;
pub mod day13;
pub mod day14;
pub mod day15;
pub mod day16;
pub mod day17;
pub mod day18;
pub mod day19;
pub mod day2;
pub mod day20;
pub mod day21;
pub mod day22;
pub mod day23;
pub mod day24;
pub mod day25;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
pub mod day7;
pub mod day8;
pub mod day9;

pub fn all_solutions() {
    println!("Day1: 1st {} 2nd {}", day1::solution1(), day1::solution2());
    println!("Day2: 1st {} 2nd {}", day2::solution1(), day2::solution2());
    println!("Day3: 1st {} 2nd {}", day3::solution1(), day3::solution2());
    println!("Day4: 1st {} 2nd {}", day4::solution1(), day4::solution2());
    println!("Day5: 1st {} 2nd {}", day5::solution1(), day5::solution2());
    println!("Day6: 1st {} 2nd {}", day6::solution1(), day6::solution2());
    println!("Day7: 1st {} 2nd {}", day7::solution1(), day7::solution2());
    println!("Day8: 1st {} 2nd {}", day8::solution1(), day8::solution2());
    println!("Day9: 1st {} 2nd {}", day9::solution1(), day9::solution2());
    println!(
        "Day10: 1st {} 2nd {}",
        day10::solution1(),
        day10::solution2()
    );
    println!(
        "Day11: 1st {} 2nd {}",
        day11::solution1(),
        day11::solution2()
    );
    println!(
        "Day12: 1st {} 2nd {}",
        day12::solution1(),
        day12::solution2()
    );
    println!(
        "Day13: 1st {} 2nd {}",
        day13::solution1(),
        day13::solution2()
    );
    println!(
        "Day14: 1st {} 2nd {}",
        day14::solution1(),
        day14::solution2()
    );
    println!(
        "Day15: 1st {} 2nd {}",
        day15::solution1(),
        day15::solution2()
    );
    println!(
        "Day16: 1st {} 2nd {}",
        day16::solution1(),
        day16::solution2()
    );
    println!(
        "Day17: 1st {} 2nd {}",
        day17::solution1(),
        day17::solution2()
    );
    println!(
        "Day18: 1st {} 2nd {}",
        day18::solution1(),
        day18::solution2()
    );
    println!(
        "Day19: 1st {} 2nd {}",
        day19::solution1(),
        day19::solution2()
    );
    println!(
        "Day20: 1st {} 2nd {}",
        day20::solution1(),
        day20::solution2()
    );
    println!(
        "Day21: 1st {} 2nd {}",
        day21::solution1(),
        day21::solution2()
    );
    println!(
        "Day22: 1st {} 2nd {}",
        day22::solution1(),
        day22::solution2()
    );
    println!(
        "Day23: 1st {} 2nd {}",
        day23::solution1(),
        day23::solution2()
    );
    println!(
        "Day24: 1st {} 2nd {}",
        day24::solution1(),
        day24::solution2()
    );
    println!(
        "Day25: 1st {} 2nd {}",
        day25::solution1(),
        day25::solution2()
    );
}
