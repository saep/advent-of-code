use std::{borrow::Borrow, collections::HashMap};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alphanumeric1, newline, space0},
    combinator::value,
    multi::{many1, separated_list1},
    sequence::{separated_pair, tuple},
    IResult,
};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("inputs/day8_1.txt").expect("file to exist");
    Network::parse(&input)
        .expect("parsing to succeed")
        .1
        .steps("AAA", |link| link == "ZZZ")
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("inputs/day8_1.txt").expect("file to exist");
    Network::parse(&input)
        .expect("parsing to succeed")
        .1
        .ghost_steps()
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
enum Direction {
    Left,
    Right,
}

impl Direction {
    fn select<'a, T>(&self, pair: &'a (T, T)) -> &'a T {
        match self {
            Direction::Left => &pair.0,
            Direction::Right => &pair.1,
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
struct Instructions {
    repeatedly: Vec<Direction>,
}

impl Instructions {
    fn iter(&self) -> InstructionsIterator {
        InstructionsIterator {
            instructions: self,
            pos: 0,
        }
    }
}
struct InstructionsIterator<'a> {
    instructions: &'a Instructions,
    pos: usize,
}

impl Iterator for InstructionsIterator<'_> {
    type Item = Direction;

    fn next(&mut self) -> Option<Self::Item> {
        let next = self.instructions.repeatedly.get(self.pos);
        self.pos += 1;
        if self.pos >= self.instructions.repeatedly.len() {
            self.pos = 0;
        }
        next.copied()
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Network {
    instructions: Instructions,
    links: HashMap<String, (String, String)>,
}

fn parse_link(input: &str) -> IResult<&str, (String, (String, String))> {
    let (remaining, (from, _, (l, r), _)) = tuple((
        alphanumeric1,
        tuple((space0, tag("="), space0, tag("("))),
        separated_pair(
            alphanumeric1,
            tuple((space0, tag(","), space0)),
            alphanumeric1,
        ),
        tuple((space0, tag(")"), space0)),
    ))(input)?;
    Ok((
        remaining,
        (String::from(from), (String::from(l), String::from(r))),
    ))
}

impl Network {
    fn parse(input: &str) -> IResult<&str, Network> {
        let (remaining, (repeatedly, _)) = tuple((
            many1(alt((
                value(Direction::Left, tag("L")),
                value(Direction::Right, tag("R")),
            ))),
            many1(newline),
        ))(input)?;
        let (remaining, links) = separated_list1(newline, parse_link)(remaining)?;
        let links = links.into_iter().collect();
        Ok((
            remaining,
            Network {
                instructions: Instructions { repeatedly },
                links,
            },
        ))
    }

    fn steps(&self, from: &str, to: impl Fn(&str) -> bool) -> usize {
        let mut left_or_right_iterator = self.instructions.iter();
        let mut pos = from;
        let mut steps = 0;
        while !to(pos) {
            steps += 1;
            pos = left_or_right_iterator
                .next()
                .expect("unlimited directions")
                .select(self.links.get(pos).expect("path to exist"))
                .borrow();
        }
        steps
    }

    fn ghost_steps(&self) -> usize {
        self.links
            .keys()
            .filter(|link| link.ends_with('A'))
            .map(|link| self.steps(link, |l| l.ends_with('Z')))
            .reduce(num::integer::lcm)
            .expect("links ending with A to exist")
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(20659, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(15690466351717, solution2());
    }

    #[test]
    fn example_part1() {
        let input = indoc! {"
                LLR

                AAA = (BBB, BBB)
                BBB = (AAA, ZZZ)
                ZZZ = (ZZZ, ZZZ)
            " };
        let network = Network::parse(input).unwrap().1;
        assert_eq!(6, network.steps("AAA", |link| link == "ZZZ"));
    }

    #[test]
    fn example_part2() {
        let input = indoc! {"
                LR

                11A = (11B, XXX)
                11B = (XXX, 11Z)
                11Z = (11B, XXX)
                22A = (22B, XXX)
                22B = (22C, 22C)
                22C = (22Z, 22Z)
                22Z = (22B, 22B)
                XXX = (XXX, XXX)
            " };
        let network = Network::parse(input).unwrap().1;
        assert_eq!(6, network.ghost_steps());
    }
}
