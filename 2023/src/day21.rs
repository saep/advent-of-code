use std::collections::{HashMap, VecDeque};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::newline,
    combinator::{map, value},
    multi::{many1, separated_list1},
    sequence::terminated,
    IResult,
};

use crate::two_dimensional::{Array2D, Direction, Point2D};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("./inputs/day21_1.txt").expect("file to exist");
    Garden::parse(&input)
        .expect("parsing to succeed")
        .1
        .reachable_garden_spots(64)
}

pub fn solution2() -> usize {
    let input = std::fs::read_to_string("./inputs/day21_1.txt").expect("file to exist");
    Garden::parse(&input)
        .expect("parsing to succeed")
        .1
        .reachable_garden_spots_repeatable(26501365)
}

#[derive(Debug, Clone)]
struct Garden {
    spots: Array2D<Spot>,
}

impl Garden {
    fn parse(input: &str) -> IResult<&str, Garden> {
        map(
            terminated(separated_list1(newline, many1(Spot::parse)), newline),
            |rows| {
                let spots = Array2D::new(rows).expect("input with rows of same length");
                Garden { spots }
            },
        )(input)
    }

    fn start_position(&self) -> Point2D {
        self.spots
            .all_positions()
            .find(|pos| self.spots[*pos] == Spot::Start)
            .expect("Start position")
    }

    fn shortest_distance_in_bounds(&self) -> HashMap<Point2D, usize> {
        let start = self.start_position();
        let mut shortest_distance = HashMap::new();
        let mut queue = VecDeque::new();
        queue.push_back((0, start));
        while let Some((dist, pos)) = queue.pop_front() {
            if shortest_distance.contains_key(&pos) {
                continue;
            }
            shortest_distance.insert(pos, dist);

            for pos in Direction::all().map(|d| pos.moved(d)) {
                if let Some(spot) = self.spots.get(pos) {
                    if spot.is_garden_spot() && !shortest_distance.contains_key(&pos) {
                        queue.push_back((dist + 1, pos));
                    }
                }
            }
        }
        shortest_distance
    }

    fn reachable_garden_spots(&self, steps: usize) -> usize {
        self.shortest_distance_in_bounds()
            .values()
            .filter(|s| **s & 1 == steps & 1 && **s <= steps)
            .count()
    }

    fn reachable_garden_spots_repeatable(&self, steps: usize) -> usize {
        // with help from https://github.com/villuna/aoc23/wiki/A-Geometric-solution-to-advent-of-code-2023,-day-21
        let start = self.start_position();
        assert_eq!(
            Ok(start.x),
            (self.spots.width() / 2).try_into(),
            "starts in the middle"
        );
        assert_eq!(
            Ok(start.y),
            (self.spots.height() / 2).try_into(),
            "starts in the middle"
        );
        let half_length = self.spots.height() / 2;
        assert_eq!(
            self.spots.width() / 2,
            half_length,
            "only works with square garden"
        );
        let shortest_distances = self.shortest_distance_in_bounds();
        assert!(steps & 1 == 1, "odd number of steps");
        let n = (steps - half_length) / self.spots.height();
        assert_eq!((steps - half_length) % n, 0);
        let odd = (n + 1) * (n + 1);
        let odd_full = odd * shortest_distances.values().filter(|v| **v & 1 == 1).count();
        let odd_corners = (n + 1)
            * shortest_distances
                .values()
                .filter(|v| **v % 2 == 1 && **v > half_length)
                .count();
        let even = n * n;
        let even_full = even * shortest_distances.values().filter(|v| **v & 1 == 0).count();
        let even_corners = n * shortest_distances
            .values()
            .filter(|v| **v % 2 == 0 && **v > half_length)
            .count();
        odd_full + even_full - odd_corners + even_corners
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
enum Spot {
    Start,
    GardenPlot,
    Rock,
}

impl Spot {
    fn parse(input: &str) -> IResult<&str, Spot> {
        alt((
            value(Spot::Start, tag("S")),
            value(Spot::GardenPlot, tag(".")),
            value(Spot::Rock, tag("#")),
        ))(input)
    }

    fn is_garden_spot(&self) -> bool {
        match self {
            Self::Rock => false,
            Self::GardenPlot | Self::Start => true,
        }
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(3743, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(618261433219147, solution2());
    }

    #[test]
    fn example1() {
        let input = indoc! {"
            ...........
            .....###.#.
            .###.##..#.
            ..#.#...#..
            ....#.#....
            .##..S####.
            .##..#...#.
            .......##..
            .##.#.####.
            .##..##.##.
            ...........
            "};
        let actual = Garden::parse(input).expect("parseable input");
        assert_eq!("", actual.0);
        assert_eq!(16, actual.1.reachable_garden_spots(6));
    }
}
