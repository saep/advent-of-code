use std::iter::zip;

use nom::{
    bytes::complete::tag,
    character::complete::{i64, newline, space0, space1},
    multi::{many0, separated_list1},
    sequence::tuple,
    IResult,
};

pub fn solution1() -> i64 {
    let input = std::fs::read_to_string("inputs/day6_1.txt").expect("file to exist");
    Record::parse_many(&input)
        .expect("parsing to succeed")
        .1
        .iter()
        .map(Record::num_ways_to_beat)
        .product()
}

pub fn solution2() -> i64 {
    let input = std::fs::read_to_string("inputs/day6_1.txt").expect("file to exist");
    Record::parse_many(&input.replace(' ', ""))
        .expect("parsing to succeed")
        .1
        .iter()
        .map(Record::num_ways_to_beat)
        .product()
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Record {
    time: i64,
    distance: i64,
}

impl Record {
    fn parse_many(input: &str) -> IResult<&str, Vec<Record>> {
        let (remaining, ((_, _, times, _), (_, _, distances), _)) = tuple((
            tuple((tag("Time:"), space0, separated_list1(space1, i64), newline)),
            tuple((tag("Distance:"), space0, separated_list1(space1, i64))),
            many0(newline),
        ))(input)?;
        let races = zip(times, distances)
            .map(|(time, distance)| Record { time, distance })
            .collect();
        Ok((remaining, races))
    }

    /// d(x)  = x * (t - x) = t*x - x*x
    /// d'(x) = t - 2x = 0
    ///      <=> x = t/2
    fn num_ways_to_beat(&self) -> i64 {
        let mut start = (self.time + 1) / 2;
        let mut wins = 0;
        while start * self.time - start * start > self.distance {
            wins += 1;
            start += 1;
        }
        if self.time % 2 == 0 && wins > 0 {
            wins * 2 - 1
        } else {
            wins * 2
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(5133600, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(40651271, solution2());
    }

    #[test]
    fn example_part1() {
        let record = Record {
            time: 7,
            distance: 9,
        };
        assert_eq!(4, record.num_ways_to_beat());
    }

    #[test]
    fn example_part2() {
        let record = Record {
            time: 71530,
            distance: 940200,
        };
        assert_eq!(71503, record.num_ways_to_beat());
    }
}
