pub fn solution1() -> u64 {
    let file_content = std::fs::read_to_string("inputs/day1_1.txt").unwrap();
    let lines = file_content.lines();
    sum_of_first_and_last_digit_in_lines(lines)
}

fn sum_of_first_and_last_digit_in_lines<'a, I>(lines: I) -> u64
where
    I: IntoIterator<Item = &'a str>,
{
    lines
        .into_iter()
        .filter_map(first_and_last_digit)
        .sum()
}

fn first_and_last_digit(line: &str) -> Option<u64> {
    let mut digits = line.chars().filter(|c| c.is_ascii_digit());
    let first = digits.next()?.to_digit(10)?;
    let last = digits
        .last()
        .iter()
        .find_map(|c| c.to_digit(10))
        .unwrap_or(first);
    Some((10 * first + last).into())
}

use nom::bytes::complete::tag;
use nom::{
    branch::alt,
    character::complete::anychar,
    combinator::{map, value},
    multi::many0,
    IResult,
};

pub fn solution2() -> u64 {
    let file_content = std::fs::read_to_string("inputs/day1_1.txt").unwrap();
    let lines = file_content.lines();
    sum_of_first_and_last_digit_spelled_or_not(lines)
}

fn sum_of_first_and_last_digit_spelled_or_not<'a, I>(lines: I) -> u64
where
    I: IntoIterator<Item = &'a str>,
{
    lines
        .into_iter()
        .filter_map(|line| parse_digits(line).ok())
        .filter_map(|digits| first_and_last_digit_of_vector(digits.1))
        .sum()
}

fn first_and_last_digit_of_vector(digits: Vec<u64>) -> Option<u64> {
    let first = digits.first()?;
    let last = digits.last()?;
    Some(10 * first + last)
}

fn parse_digits(input: &str) -> IResult<&str, Vec<u64>> {
    let maybe_digits = many0(alt((
        map(|i| parse_spelled_digit(i), Some),
        map(parse_digit(), Some),
        map(anychar, |_| None),
    )));
    map(maybe_digits, |ms| ms.iter().filter_map(|d| *d).collect())(input)
}

fn parse_digit<'a>() -> impl Fn(&'a str) -> IResult<&'a str, u64> {
    |input: &'a str| match input
        .chars()
        .next()
        .into_iter()
        .find_map(|c| c.to_digit(10))
    {
        Some(i) => Ok((&input[1..], i.into())),
        None => nom::combinator::fail(input),
    }
}

fn parse_spelled_digit(input: &str) -> IResult<&str, u64> {
    let (_, num) = alt((
        value(1, tag("one")),
        value(2, tag("two")),
        value(3, tag("three")),
        value(4, tag("four")),
        value(5, tag("five")),
        value(6, tag("six")),
        value(7, tag("seven")),
        value(8, tag("eight")),
        value(9, tag("nine")),
    ))(input)?;
    Ok((&input[1..], num))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn first_and_last_digit_with_only_one_digit() {
        let only_one_digit = first_and_last_digit("treb7uchet");
        assert_eq!(only_one_digit, Some(77))
    }

    #[test]
    fn example() {
        let lines = vec!["1abc2", "pqr3stu8vwx", "a1b2c3d4e5f", "treb7uchet"];
        let sum = sum_of_first_and_last_digit_in_lines(lines);
        assert_eq!(sum, 142)
    }

    #[test]
    fn part_1() {
        assert_eq!(solution1(), 54644);
    }

    #[test]
    fn parse_spelled_number_success() {
        let (rem, one) = parse_spelled_digit("one").unwrap();
        assert_eq!(one, 1);
        assert_eq!(rem, "ne");
    }

    #[test]
    fn parse_line_with_spelled_and_unspelled_digits() {
        let (rem, digits) = parse_digits("one2three").unwrap();
        assert_eq!(digits, vec![1, 2, 3]);
        assert_eq!(rem, "");
    }

    #[test]
    fn parsers_should_only_consume_1_character() {
        let (rem, digits) = parse_digits("1eightwom").unwrap();
        assert_eq!(rem, "");
        assert_eq!(digits, vec![1, 8, 2]);
    }

    #[test]
    fn parse_line_with_spelled_and_unspelled_digits_and_a_little_junk() {
        let (rem, digits) = parse_digits("oneisnot2isnotthree").unwrap();
        assert_eq!(rem, "");
        assert_eq!(digits, vec![1, 2, 3]);
    }

    #[test]
    fn sum_of_first_and_last_digit_spelled_or_not_in_lines() {
        let lines = vec![
            "two1nine",
            "eightwothree",
            "abcone2threexyz",
            "xtwone3four",
            "4nineeightseven2",
            "zoneight234",
            "7pqrstsixteen",
        ];
        let sum = sum_of_first_and_last_digit_spelled_or_not(lines);
        assert_eq!(sum, 281);
    }

    #[test]
    fn part_2() {
        assert_eq!(solution2(), 53348);
    }
}
