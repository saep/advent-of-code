use std::collections::HashMap;

use enumset::EnumSet;

use crate::two_dimensional::{Point2D, Direction};

pub fn solution1() -> usize {
    let input = std::fs::read_to_string("inputs/day16_1.txt").expect("file to exist");
    Contraption::parse(&input).energized_tiles((Point2D { x: 0, y: 0 }, Direction::East))
}
pub fn solution2() -> usize {
    let input = std::fs::read_to_string("inputs/day16_1.txt").expect("file to exist");
    Contraption::parse(&input).max_energized_tiles_for_all_starting_positions()
}

#[derive(Debug)]
struct Contraption {
    rows: Vec<Vec<u8>>,
}

impl Contraption {
    fn parse(input: &str) -> Contraption {
        let rows = input.lines().map(|line| line.bytes().collect()).collect();
        Contraption { rows }
    }

    fn get(&self, point: Point2D) -> Option<u8> {
        if point.x < 0 || point.y < 0 {
            None
        } else {
            self.rows
                .get(point.y as usize)?
                .get(point.x as usize)
                .copied()
        }
    }

    fn energized_tiles(&self, start: (Point2D, Direction)) -> usize {
        let mut visited: HashMap<Point2D, EnumSet<Direction>> = HashMap::new();
        let mut paths_not_taken = vec![start];
        while let Some((p, dir)) = paths_not_taken.pop() {
            let mut current_position = p;
            let mut current_direction = dir;
            while let Some(tile) = self.get(current_position) {
                let visited_directions = visited.entry(current_position).or_default();
                if !visited_directions.insert(current_direction) {
                    break;
                }
                match tile {
                    b'.' => {}
                    b'|' => match current_direction {
                        Direction::North | Direction::South => {}
                        Direction::East | Direction::West => {
                            current_direction = Direction::North;
                            paths_not_taken.push((current_position, Direction::South));
                        }
                    },
                    b'-' => match current_direction {
                        Direction::East | Direction::West => {}
                        Direction::North | Direction::South => {
                            current_direction = Direction::East;
                            paths_not_taken.push((current_position, Direction::West));
                        }
                    },
                    b'/' => match current_direction {
                        Direction::North => current_direction = Direction::East,
                        Direction::East => current_direction = Direction::North,
                        Direction::South => current_direction = Direction::West,
                        Direction::West => current_direction = Direction::South,
                    },
                    b'\\' => match current_direction {
                        Direction::North => current_direction = Direction::West,
                        Direction::East => current_direction = Direction::South,
                        Direction::South => current_direction = Direction::East,
                        Direction::West => current_direction = Direction::North,
                    },
                    _ => panic!("invalid input"),
                }
                current_position = current_position.moved(current_direction);
            }
        }
        visited.len()
    }

    fn max_energized_tiles_for_all_starting_positions(&self) -> usize {
        self.starting_positions()
            .into_iter()
            .map(|start| self.energized_tiles(start))
            .max()
            .unwrap_or(0)
    }

    fn starting_positions(&self) -> Vec<(Point2D, Direction)> {
        let mut starting_positions = vec![];
        let last_col_index = self.rows[0].len() - 1;
        let last_row_index = self.rows.len() - 1;
        for x in 0..=last_col_index {
            starting_positions.push((Point2D { x: x as i64, y: 0 }, Direction::South));
            starting_positions.push((
                Point2D {
                    x: x as i64,
                    y: last_row_index as i64,
                },
                Direction::North,
            ));
        }
        for y in 0..=last_row_index {
            starting_positions.push((Point2D { x: 0, y: y as i64 }, Direction::East));
            starting_positions.push((
                Point2D {
                    x: last_col_index as i64,
                    y: y as i64,
                },
                Direction::West,
            ));
        }
        starting_positions
    }
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use crate::two_dimensional::{Point2D, Direction};

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(6978, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(7315, solution2());
    }

    #[test]
    fn example_part1() {
        let input = indoc! {r"
            .|...\....
            |.-.\.....
            .....|-...
            ........|.
            ..........
            .........\
            ..../.\\..
            .-.-/..|..
            .|....-|.\
            ..//.|....
            "};
        let contraption = Contraption::parse(input);
        assert_eq!(
            46,
            contraption.energized_tiles((Point2D { x: 0, y: 0 }, Direction::East))
        );
    }

    #[test]
    fn example_part2() {
        let input = indoc! {r"
            .|...\....
            |.-.\.....
            .....|-...
            ........|.
            ..........
            .........\
            ..../.\\..
            .-.-/..|..
            .|....-|.\
            ..//.|....
            "};
        let contraption = Contraption::parse(input);
        assert_eq!(
            51,
            contraption.max_energized_tiles_for_all_starting_positions()
        );
    }
}
