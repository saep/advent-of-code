use enumset::{EnumSet, EnumSetType};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{newline, space0, space1, u32},
    combinator::{all_consuming, map, value},
    multi::separated_list1,
    sequence::{delimited, pair, separated_pair, terminated, tuple},
    IResult,
};
use std::{
    cmp::max,
    ops::{Index, IndexMut},
};

pub fn solution1() -> u32 {
    let input = std::fs::read_to_string("inputs/day2_1.txt").unwrap();
    sum_of_ids_of_possible_games(&input)
}

fn sum_of_ids_of_possible_games(input: &str) -> u32 {
    let max_cubes = CubeReveal {
        red: 12,
        green: 13,
        blue: 14,
    };
    Game::parse_many(input)
        .expect("valid input")
        .1
        .into_iter()
        .filter(|game| game.is_possible_with_max_cubes(&max_cubes))
        .map(|game| game.id)
        .sum()
}

#[derive(PartialOrd, Ord, EnumSetType)]
enum Color {
    Red,
    Green,
    Blue,
}

#[derive(Debug, PartialEq, Hash, Default, Copy, Clone)]
struct CubeReveal {
    red: u32,
    green: u32,
    blue: u32,
}

impl CubeReveal {
    fn reduce(&self, other: &CubeReveal, f: impl Fn(u32, u32) -> u32) -> CubeReveal {
        CubeReveal {
            red: f(self.red, other.red),
            green: f(self.green, other.green),
            blue: f(self.blue, other.blue),
        }
    }

    fn is_possible_with_max_cubes(&self, max_cubes: &CubeReveal) -> bool {
        EnumSet::all()
            .iter()
            .all(|color| self[color] <= max_cubes[color])
    }

    fn power(self) -> u64 {
        EnumSet::all()
            .iter()
            .map(|color| Into::<u64>::into(self[color]))
            .product()
    }
}

impl Index<Color> for CubeReveal {
    type Output = u32;

    fn index(&self, index: Color) -> &Self::Output {
        match index {
            Color::Red => &self.red,
            Color::Green => &self.green,
            Color::Blue => &self.blue,
        }
    }
}

impl IndexMut<Color> for CubeReveal {
    fn index_mut(&mut self, index: Color) -> &mut Self::Output {
        match index {
            Color::Red => &mut self.red,
            Color::Green => &mut self.green,
            Color::Blue => &mut self.blue,
        }
    }
}

#[derive(Debug, PartialEq)]
struct Game {
    id: u32,
    reveals: Vec<CubeReveal>,
}

impl Game {
    fn is_possible_with_max_cubes(&self, max_cubes: &CubeReveal) -> bool {
        self.reveals
            .iter()
            .all(|reveal| reveal.is_possible_with_max_cubes(max_cubes))
    }

    fn fewest_number_of_cubes_to_make_possible(&self) -> CubeReveal {
        self.reveals
            .iter()
            .fold(CubeReveal::default(), |acc, cr| acc.reduce(cr, max))
    }

    fn parse(input: &str) -> IResult<&str, Game> {
        let id = delimited(tag("Game "), u32, tuple((tag(":"), space0)));
        let reveals = separated_list1(tuple((tag(";"), space0)), parse_cube_reveal);
        map(pair(id, reveals), |(id, reveals)| Game { id, reveals })(input)
    }

    fn parse_many(input: &str) -> IResult<&str, Vec<Game>> {
        all_consuming(separated_list1(newline, Game::parse))(input.trim_end())
    }
}

fn parse_cube_reveal(input: &str) -> IResult<&str, CubeReveal> {
    let parse_n_color = separated_pair(
        u32,
        space1,
        alt((
            value(Color::Red, tag("red")),
            value(Color::Green, tag("green")),
            value(Color::Blue, tag("blue")),
        )),
    );
    map(
        separated_list1(terminated(tag(","), space0), parse_n_color),
        |reveals| {
            reveals
                .into_iter()
                .fold(CubeReveal::default(), |mut acc, (n, c)| {
                    acc[c] += n;
                    acc
                })
        },
    )(input)
}

pub fn solution2() -> u64 {
    let input = std::fs::read_to_string("inputs/day2_1.txt").unwrap();
    sum_of_power_of_minum_cubes(&input)
}

fn sum_of_power_of_minum_cubes(input: &str) -> u64 {
    Game::parse_many(input)
        .expect("valid input")
        .1
        .into_iter()
        .map(|game| game.fewest_number_of_cubes_to_make_possible().power())
        .sum()
}

#[cfg(test)]
mod test {
    use indoc::indoc;

    use super::*;

    #[test]
    fn expected_solution1() {
        assert_eq!(2176, solution1());
    }

    #[test]
    fn expected_solution2() {
        assert_eq!(63700, solution2());
    }

    #[test]
    fn parse_game_1() {
        let game_input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green";
        let (remaining, parsed) = Game::parse(game_input).unwrap();
        assert_eq!(remaining, "");
        assert_eq!(
            parsed,
            Game {
                id: 1,
                reveals: vec![
                    CubeReveal {
                        red: 4,
                        blue: 3,
                        green: 0
                    },
                    CubeReveal {
                        red: 1,
                        blue: 6,
                        green: 2
                    },
                    CubeReveal {
                        red: 0,
                        blue: 0,
                        green: 2
                    }
                ]
            }
        )
    }

    #[test]
    fn possible_games_example_part1() {
        let games = indoc![
            "
            Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
            Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
            Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
            Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
            Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
        "
        ];
        assert_eq!(8, sum_of_ids_of_possible_games(games));
    }

    #[test]
    fn possible_games_example_part2() {
        let games = indoc![
            "
            Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
            Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
            Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
            Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
            Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
        "
        ];
        assert_eq!(2286, sum_of_power_of_minum_cubes(games));
    }
}
