#!/usr/bin/env bash

set -euo pipefail

identity="${HOME}/.ssh/id_ed25519"
if [ ! -f "${identity}" ]; then
  echo "${identity} doesn't exist"
  exit 1
fi

INPUT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

for f in "${INPUT_DIR}"/*.txt.age ; do
  target="${f%.age}"
  if [[ -f "${f}" ]] && [[ ! -f "${target}" ]]; then
    age --decrypt -i "${identity}" "${f}" > "${target}"
  fi
done
