use aoc_2023::day4::{solution1, solution2};
use criterion::{criterion_group, criterion_main, Criterion};

fn solution1_benchmark(c: &mut Criterion) {
    c.bench_function("day 4 solution1", |b| b.iter(solution1));
}

fn solution2_benchmark(c: &mut Criterion) {
    c.bench_function("day 4 solution2", |b| b.iter(solution2));
}

criterion_group!(benches, solution1_benchmark, solution2_benchmark);
criterion_main!(benches);
